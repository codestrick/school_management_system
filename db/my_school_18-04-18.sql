/*
SQLyog Ultimate v12.4.1 (64 bit)
MySQL - 10.1.21-MariaDB : Database - my_school
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`my_school` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `my_school`;

/*Table structure for table `address` */

DROP TABLE IF EXISTS `address`;

CREATE TABLE `address` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(255) NOT NULL,
  `pincode` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `address` */

insert  into `address`(`id`,`address`,`pincode`,`updated_at`) values (1,'chandrapur,bagnan','711303','2018-04-16 07:00:38');

/*Table structure for table `bank` */

DROP TABLE IF EXISTS `bank`;

CREATE TABLE `bank` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bank_name` varchar(255) NOT NULL,
  `branch_name` varchar(255) NOT NULL,
  `ifsc` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bank` */

/*Table structure for table `cast` */

DROP TABLE IF EXISTS `cast`;

CREATE TABLE `cast` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cast_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `cast` */

insert  into `cast`(`id`,`cast_name`,`created_at`,`updated_at`) values (1,'General','2018-04-02 06:44:29','2018-04-02 06:49:42');
insert  into `cast`(`id`,`cast_name`,`created_at`,`updated_at`) values (2,'S.C','2018-04-02 06:49:23','2018-04-02 06:49:55');

/*Table structure for table `class` */

DROP TABLE IF EXISTS `class`;

CREATE TABLE `class` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) NOT NULL,
  `class_type` enum('lower','upper') NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `class` */

insert  into `class`(`id`,`class_name`,`class_type`,`updated_at`) values (1,'5','upper','2018-04-08 06:43:27');

/*Table structure for table `guardians` */

DROP TABLE IF EXISTS `guardians`;

CREATE TABLE `guardians` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `std_reg_id` int(11) NOT NULL,
  `fathers_name` varchar(255) NOT NULL,
  `fathers_occupation` varchar(255) NOT NULL,
  `mothers_name` varchar(255) DEFAULT NULL,
  `mothers_occupation` varchar(255) DEFAULT NULL,
  `guardians_name` varchar(255) NOT NULL,
  `guardians_occupation` varchar(255) NOT NULL,
  `guardians_relation` varchar(255) NOT NULL,
  `guardians_address` varchar(255) NOT NULL,
  `guardians_pincode` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `guardians` */

/*Table structure for table `letter_grade` */

DROP TABLE IF EXISTS `letter_grade`;

CREATE TABLE `letter_grade` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `grade_json` text NOT NULL,
  `all_data` text NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `letter_grade` */

insert  into `letter_grade`(`id`,`grade_json`,`all_data`,`updated_at`) values (1,'{\"start_number_1\":\"0\",\"start_number_2\":\"20\",\"start_number_3\":\"40\",\"start_number_4\":\"60\",\"start_number_5\":\"90\",\"start_number_6\":\"\",\"start_number_7\":\"\",\"start_number_8\":\"\",\"start_number_9\":\"\",\"start_number_10\":\"\",\"end_number_1\":\"19\",\"end_number_2\":\"39\",\"end_number_3\":\"59\",\"end_number_4\":\"89\",\"end_number_5\":\"100\",\"end_number_6\":\"\",\"end_number_7\":\"\",\"end_number_8\":\"\",\"end_number_9\":\"\",\"end_number_10\":\"\",\"grade_1\":\"E\",\"grade_2\":\"D\",\"grade_3\":\"C\",\"grade_4\":\"B\",\"grade_5\":\"A\",\"grade_6\":\"\",\"grade_7\":\"\",\"grade_8\":\"\",\"grade_9\":\"\",\"grade_10\":\"\",\"grade_name_1\":\"E\",\"grade_name_2\":\"D\",\"grade_name_3\":\"C\",\"grade_name_4\":\"B\",\"grade_name_5\":\"AA\",\"grade_name_6\":\"\",\"grade_name_7\":\"\",\"grade_name_8\":\"\",\"grade_name_9\":\"\",\"grade_name_10\":\"\"}','[{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"E\",\"grade_name\":\"E\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"D\",\"grade_name\":\"D\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"C\",\"grade_name\":\"C\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"B\",\"grade_name\":\"B\"},{\"grade\":\"A\",\"grade_name\":\"AA\"},{\"grade\":\"A\",\"grade_name\":\"AA\"},{\"grade\":\"A\",\"grade_name\":\"AA\"},{\"grade\":\"A\",\"grade_name\":\"AA\"},{\"grade\":\"A\",\"grade_name\":\"AA\"},{\"grade\":\"A\",\"grade_name\":\"AA\"},{\"grade\":\"A\",\"grade_name\":\"AA\"},{\"grade\":\"A\",\"grade_name\":\"AA\"},{\"grade\":\"A\",\"grade_name\":\"AA\"},{\"grade\":\"A\",\"grade_name\":\"AA\"},{\"grade\":\"A\",\"grade_name\":\"AA\"}]','2018-04-14 14:17:41');

/*Table structure for table `occupation` */

DROP TABLE IF EXISTS `occupation`;

CREATE TABLE `occupation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `occupation_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `occupation` */

insert  into `occupation`(`id`,`occupation_name`,`created_at`,`updated_at`) values (1,'Abc','2018-04-03 06:36:26','2018-04-03 06:36:26');
insert  into `occupation`(`id`,`occupation_name`,`created_at`,`updated_at`) values (2,'Abcde','2018-04-03 06:36:39','2018-04-03 06:37:34');

/*Table structure for table `old_school` */

DROP TABLE IF EXISTS `old_school`;

CREATE TABLE `old_school` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `old_school_name` varchar(255) NOT NULL,
  `address` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `old_school` */

insert  into `old_school`(`id`,`old_school_name`,`address`,`created_at`,`updated_at`) values (1,'N.M.K.G School',NULL,'2018-04-02 06:42:49','2018-04-02 06:43:58');

/*Table structure for table `religion` */

DROP TABLE IF EXISTS `religion`;

CREATE TABLE `religion` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `religion_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `religion` */

insert  into `religion`(`id`,`religion_name`,`created_at`,`updated_at`) values (1,'Hindu','2018-04-02 06:46:30','2018-04-02 06:46:30');

/*Table structure for table `section` */

DROP TABLE IF EXISTS `section`;

CREATE TABLE `section` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `section_name` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `section` */

insert  into `section`(`id`,`class_id`,`section_name`,`updated_at`) values (1,1,'5-A','2018-04-08 08:25:40');
insert  into `section`(`id`,`class_id`,`section_name`,`updated_at`) values (2,1,'5-B','2018-04-08 08:26:04');

/*Table structure for table `session` */

DROP TABLE IF EXISTS `session`;

CREATE TABLE `session` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ses_start_yr` int(11) NOT NULL,
  `ses_end_yr` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `session` */

insert  into `session`(`id`,`ses_start_yr`,`ses_end_yr`,`created_at`,`updated_at`) values (1,2018,2019,'2018-04-02 06:34:52','2018-04-02 06:34:52');

/*Table structure for table `std_registration` */

DROP TABLE IF EXISTS `std_registration`;

CREATE TABLE `std_registration` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reg_id` varchar(255) NOT NULL,
  `guardians_id` bigint(20) NOT NULL,
  `std_name` varchar(255) NOT NULL,
  `state_id` int(11) NOT NULL,
  `std_district_id` int(11) NOT NULL,
  `pincode` int(11) NOT NULL,
  `location` varchar(255) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `cast_id` int(11) NOT NULL,
  `sub_cast_id` int(11) DEFAULT NULL,
  `admission_class_id` int(11) NOT NULL,
  `admission_section_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `std_registration` */

/*Table structure for table `sub_cast` */

DROP TABLE IF EXISTS `sub_cast`;

CREATE TABLE `sub_cast` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cast_id` int(11) NOT NULL,
  `sub_cast_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sub_cast` */

/*Table structure for table `subject` */

DROP TABLE IF EXISTS `subject`;

CREATE TABLE `subject` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(255) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `subject` */

insert  into `subject`(`id`,`subject_name`,`updated_at`) values (1,'English','2018-04-04 19:53:15');

/*Table structure for table `subject_exam_lay_out_x` */

DROP TABLE IF EXISTS `subject_exam_lay_out_x`;

CREATE TABLE `subject_exam_lay_out_x` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `subject_lay_out` text NOT NULL,
  `exam_lay_out` text NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `subject_exam_lay_out_x` */

/*Table structure for table `subject_lay_out_xii` */

DROP TABLE IF EXISTS `subject_lay_out_xii`;

CREATE TABLE `subject_lay_out_xii` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(11) NOT NULL,
  `strim_id` int(11) NOT NULL,
  `lay_out` text NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `subject_lay_out_xii` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
