<?php

    if ( ! defined('BASEPATH'))
    {
        exit('No direct script access allowed');
    }

    require_once APPPATH . "/third_party/PHPExcel/PHPExcel.php";

    class Excel extends PHPExcel
    {

        public function __construct()
        {
            parent::__construct();
            $this->excel = new PHPExcel();
        }

        //example
        public function report_month_duration($dataArray,$name)
        {


            $objRichText = new PHPExcel_RichText();

            $this->excel->getActiveSheet()
                ->setCellValue('A1', 'No.')
                ->setCellValue('B1', 'Name')
                ->setCellValue('C1', 'Selling Date')
                ->setCellValue('D1', 'Net Total')
                ->setCellValue('E1', 'GST Charged Amount')
                ->setCellValue('F1', 'Net Total With GST')
                ->setCellValue('G1', 'Created')
                ->setCellValue('H1', 'Updated');

            $rowNumber = 2;
            $num_col   = 8;

            foreach ($dataArray as $k => $row)
            {
                for ($i = 1; $i <= $num_col; $i++)
                {
                    if ($i == 1)
                    {
                        $col = 'A';
                        $this->excel->getActiveSheet()->setCellValue($col . $rowNumber, ($k+1));
                    }
                    else if ($i == 2)
                    {
                        $col = 'B';
                        $this->excel->getActiveSheet()->setCellValue($col . $rowNumber, $row['name']);
                    }
                    else if ($i == 3)
                    {
                        $col = 'C';
                        $this->excel->getActiveSheet()->setCellValue($col . $rowNumber, $row['selling_date']);
                    }
                    else if ($i == 4)
                    {
                        $col = 'D';
                        $this->excel->getActiveSheet()->setCellValue($col . $rowNumber, $row['net_total']);
                    }
                    else if ($i == 5)
                    {
                        $col = 'E';
                        $this->excel->getActiveSheet()->setCellValue($col . $rowNumber, $row['total_gst_charged_amount']);
                    }
                    else if ($i == 6)
                    {
                        $col = 'F';
                        $this->excel->getActiveSheet()->setCellValue($col . $rowNumber, $row['net_total_with_gst']);
                    }
                    else if ($i == 7)
                    {
                        $col = 'G';
                        $this->excel->getActiveSheet()->setCellValue($col . $rowNumber, $row['created_at']);
                    }
                    else if ($i == 8)
                    {
                        $col = 'H';
                        $this->excel->getActiveSheet()->setCellValue($col . $rowNumber, $row['updated_at']);
                    }
                }
                $rowNumber++;
            }

            $filename = $name;

            header('Content-type: application/ms-excel');
            header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
            header("Cache-control: private");
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            $objWriter->save("reports/details/" . $filename);
//            $objWriter->save('php://output');
            header("location: " . base_url() . "reports/details/" . $filename);
            unlink(base_url() . "reports/details/" . $filename);
        }

        public function roll_number_download()
        {
            p('cggf');
        }

    }
    