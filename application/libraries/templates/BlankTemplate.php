<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}
/**
 * Blank template
 */
require_once 'Template.php';

/**
 * Blank template for rendering the partial views
 * that do not require the other stuff like header and footer etc.
 * Normally this will be used in AJAX calls for partial rendering of pages
 */
class BlankTemplate extends Template
{

    /**
     * BlankTemplate constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param       $view
     * @param array $data
     *
     * @return mixed
     */
    public function render($view, array $data = array())
    {
        return $this->CI->load->viewPartial($view, $data);
    }
}