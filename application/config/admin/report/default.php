<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

if ( ! isset($config))
{
    $config = array();
}


$config = array_merge($config, array(
    'session_listing_headers' => array(
        'checkbox'     => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'ses_start_yr' => array(
            'jsonField' => 'ses_start_yr',
            'width'     => '25%'
        ),
        'ses_end_yr'   => array(
            'jsonField' => 'ses_end_yr',
            'width'     => '20%'
        ),
        'created_at'   => array(
            'jsonField' => 'created_at',
            'width'     => '15%'
        ),
        'updated_at'   => array(
            'jsonField' => 'updated_at',
            'width'     => '15%'
        )
    )
));
$config = array_merge($config, array(
    'student_listing_headers' => array(
        'checkbox' => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'std_name' => array(
            'jsonField' => 'std_name',
            'width'     => '25%'
        ),
        'dob'      => array(
            'jsonField' => 'dob',
            'width'     => '20%'
        ),
        'age'      => array(
            'jsonField' => 'age',
            'width'     => '15%'
        )
    )
));

$config = array_merge($config, array(
    'caste_listing_headers' => array(
        'checkbox'   => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'caste_name' => array(
            'jsonField' => 'caste_name',
            'width'     => '20%'
        ),
        'created_at' => array(
            'jsonField' => 'created_at',
            'width'     => '15%'
        ),
        'updated_at' => array(
            'jsonField' => 'updated_at',
            'width'     => '15%'
        ),
        'action'     => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                //                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON' => 'id',
                //                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON' => 'admin/caste/edit/',
                //                'DELETE_ICON' => 'admin/session/delete/'
            ),
        )
    )
));


$config = array_merge($config, array(
    'sub_caste_listing_headers' => array(
        'checkbox'       => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'caste_name'     => array(
            'jsonField' => 'caste_name',
            'width'     => '20%'
        ),
        'sub_caste_name' => array(
            'jsonField' => 'sub_caste_name',
            'width'     => '20%'
        ),
        'created_at'     => array(
            'jsonField' => 'created_at',
            'width'     => '15%'
        ),
        'updated_at'     => array(
            'jsonField' => 'updated_at',
            'width'     => '15%'
        ),
        'action'         => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                //                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON' => 'id',
                //                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON' => 'admin/sub-caste/edit/',
                //                'DELETE_ICON' => 'admin/session/delete/'
            ),
        )
    )
));

$config = array_merge($config, array(
    'subject_listing_headers' => array(
        'checkbox'     => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'subject_name' => array(
            'jsonField' => 'subject_name',
            'width'     => '20%'
        ),
        'updated_at'   => array(
            'jsonField' => 'updated_at',
            'width'     => '15%'
        ),
        'action'       => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                //                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON' => 'id',
                //                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON' => 'admin/subject/edit/',
                //                'DELETE_ICON' => 'admin/session/delete/'
            ),
        )
    )
));

$config = array_merge($config, array(
    'old_school_listing_headers' => array(
        'checkbox'        => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'old_school_name' => array(
            'jsonField' => 'old_school_name',
            'width'     => '20%'
        ),
        'address'         => array(
            'jsonField' => 'address',
            'width'     => '20%'
        ),
        'created_at'      => array(
            'jsonField' => 'created_at',
            'width'     => '15%'
        ),
        'updated_at'      => array(
            'jsonField' => 'updated_at',
            'width'     => '15%'
        ),
        'action'          => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                //                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON' => 'id',
                //                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON' => 'admin/old-school/edit/',
                //                'DELETE_ICON' => 'admin/session/delete/'
            ),
        )
    )
));

$config = array_merge($config, array(
    'religion_listing_headers' => array(
        'checkbox'      => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'religion_name' => array(
            'jsonField' => 'religion_name',
            'width'     => '20%'
        ),
        'created_at'    => array(
            'jsonField' => 'created_at',
            'width'     => '15%'
        ),
        'updated_at'    => array(
            'jsonField' => 'updated_at',
            'width'     => '15%'
        ),
        'action'        => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                //                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON' => 'id',
                //                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON' => 'admin/religion/edit/',
                //                'DELETE_ICON' => 'admin/session/delete/'
            ),
        )
    )
));

$config = array_merge($config, array(
    'address_listing_headers' => array(
        'checkbox'   => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'address'    => array(
            'jsonField' => 'address',
            'width'     => '25%'
        ),
        'pincode'    => array(
            'jsonField' => 'pincode',
            'width'     => '25%'
        ),
        'updated_at' => array(
            'jsonField' => 'updated_at',
            'width'     => '15%'
        ),
        'action'     => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                //                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON' => 'id',
                //                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON' => 'admin/address/edit/',
                //                'DELETE_ICON' => 'admin/session/delete/'
            ),
        )
    )
));
$config = array_merge($config, array(
    'occupation_listing_headers' => array(
        'checkbox'        => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'occupation_name' => array(
            'jsonField' => 'occupation_name',
            'width'     => '25%'
        ),
        'created_at'      => array(
            'jsonField' => 'created_at',
            'width'     => '15%'
        ),
        'updated_at'      => array(
            'jsonField' => 'updated_at',
            'width'     => '15%'
        ),
        'action'          => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                //                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON' => 'id',
                //                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON' => 'admin/occupation/edit/',
                //                'DELETE_ICON' => 'admin/session/delete/'
            ),
        )
    )
));

$config = array_merge($config, array(
    'section_listing_headers' => array(
        'checkbox'     => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'section_name' => array(
            'jsonField' => 'section_name',
            'width'     => '25%'
        ),
        'class_name'   => array(
            'jsonField' => 'class_name',
            'width'     => '25%'
        ),
        'updated_at'   => array(
            'jsonField' => 'updated_at',
            'width'     => '15%'
        ),
        'action'       => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                //                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON' => 'id',
                //                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON' => 'admin/section/edit/',
                //                'DELETE_ICON' => 'admin/session/delete/'
            ),
        )
    )
));

$config = array_merge($config, array(
    'class_listing_headers' => array(
        'checkbox'   => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'class_name' => array(
            'jsonField' => 'class_name',
            'width'     => '25%'
        ),
        'class_type' => array(
            'jsonField' => 'class_type',
            'width'     => '25%'
        ),
        'updated_at' => array(
            'jsonField' => 'updated_at',
            'width'     => '15%'
        ),
        'action'     => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                //                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON' => 'id',
                //                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON' => 'admin/class/edit/',
                //                'DELETE_ICON' => 'admin/session/delete/'
            ),
        )
    )
));

$config = array_merge($config, array(
    'faculty_listing_headers' => array(
        'checkbox'     => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'name'         => array(
            'jsonField' => 'name',
            'width'     => '10%'
        ),
        'email'        => array(
            'jsonField' => 'email',
            'width'     => '10%'
        ),
        'mobile'       => array(
            'jsonField' => 'mobile',
            'width'     => '10%'
        ),
//        'address'      => array(
//            'jsonField' => 'address',
//            'width'     => '10%'
//        ),
        'subject_name' => array(
            'jsonField' => 'subject_name',
            'width'     => '10%'
        ),
        'action'       => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '10%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON'   => 'id',
                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON'   => 'admin/faculty/edit/',
                'DELETE_ICON' => 'admin/faculty/delete/'
            ),
        )
    )
));