<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

$config['css_default'] = array(
    'styles.css'           => array('name' => 'assets/admin/css/icons/icomoon/styles.css'),
    'bootstrap.css'        => array('name' => 'assets/admin/css/bootstrap.css'),
    'core.min.css'         => array('name' => 'assets/admin/css/core.css'),
    'components.css'       => array('name' => 'assets/admin/css/components.css'),
    'colors.css'           => array('name' => 'assets/admin/css/colors.css'),
    'font-awesome.min.css' => array('name' => 'assets/css/plugins/font-awesome/css/font-awesome.min.css'),

    'custom.css' => array('name' => 'assets/css/admin/custom.css'),
);

$config['js_default'] = array(
    'pace.min.js'      => array('name' => 'assets/admin/js/plugins/loaders/pace.min.js'),
    'jquery.min.js'    => array('name' => 'assets/admin/js/core/libraries/jquery.min.js'),
    'bootstrap.min.js' => array('name' => 'assets/admin/js/core/libraries/bootstrap.min.js'),
    'blockui.min.js'   => array('name' => 'assets/admin/js/plugins/loaders/blockui.min.js'),
    'bootbox.min.js'   => array('name' => 'assets/admin/js/plugins/notifications/bootbox.min.js'),
    'app.js'           => array('name' => 'assets/admin/js/core/app.js'),
    'select2'          => array('name' => 'assets/admin/js/plugins/forms/selects/select2.min.js'),
    'jquery.mask'      => array('name' => 'assets/admin/js/plugins/forms/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js'),

    'custom.js' => array('name' => 'assets/js/admin/custom.js'),

    'blockUi' => array('name' => 'assets/admin/js/blockUi.min.js'),

    'jquery.validate.js'             => array('name' => 'assets/js/plugins/jquery-validate/jquery.validate.js'),
    'validate-additional-methods.js' => array('name' => 'assets/js/plugins/jquery-validate/additional-methods.js'),
    'alphanumeric.js'                => array('name' => 'assets/js/plugins/alphanumeric/alphanum.js'),
    'bootstrap_multiselect.js'       => array('name' => 'assets/admin/js/plugins/forms/selects/bootstrap_multiselect.js'),
);

$config['css_arr'] = array(
    'dataTables.bootstrap4' => array('name' => 'assets/js/plugins/data-tables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css'),
    'buttons.dataTables'    => array('name' => 'assets/js/plugins/data-tables/Buttons-1.5.1/css/buttons.dataTables.min.css'),
    'select.bootstrap4'     => array('name' => 'assets/js/plugins/data-tables/Select-1.2.5/css/select.bootstrap4.min.css'),

    'bootstrap-datepicker' => array('name' => 'assets/js/plugins/boostrap-datepicker/css/bootstrap-datepicker.min.css'),
);

$config['js_arr'] = array(
    'jquery.dataTables'     => array('name' => 'assets/js/plugins/data-tables/DataTables-1.10.16/js/jquery.dataTables.min.js'),
    'dataTables.bootstrap4' => array('name' => 'assets/js/plugins/data-tables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js'),
    'dataTables.buttons'    => array('name' => 'assets/js/plugins/data-tables/Buttons-1.5.1/js/dataTables.buttons.min.js'),
    'jszip'                 => array('name' => 'assets/js/plugins/data-tables/JSZip-2.5.0/jszip.min.js'),
    'pdfmake'               => array('name' => 'assets/js/plugins/data-tables/pdfmake-0.1.32/pdfmake.min.js'),
    'vfs_fonts'             => array('name' => 'assets/js/plugins/data-tables/pdfmake-0.1.32/vfs_fonts.js'),
    'buttons.html5'         => array('name' => 'assets/js/plugins/data-tables/Buttons-1.5.1/js/buttons.html5.min.js'),
    'buttons.print'         => array('name' => 'assets/js/plugins/data-tables/Buttons-1.5.1/js/buttons.print.min.js'),
    'dataTables.select'     => array('name' => 'assets/js/plugins/data-tables/Select-1.2.5/js/dataTables.select.min.js'),
    'dataTables-main'       => array('name' => 'assets/js/plugins/data-tables/dataTables-main.js'),

    'bootstrap-datepicker' => array('name' => 'assets/js/plugins/boostrap-datepicker/js/bootstrap-datepicker.min.js'),
    'moment'               => array('name' => 'assets/admin/js/plugins/ui/moment/moment.min.js'),

    'steps'                => array('name' => 'assets/admin/js/plugins/forms/wizards/steps.min.js'),
    'validate'             => array('name' => 'assets/admin/js/plugins/forms/validation/validate.min.js'),

    'editable-table'             => array('name' => 'assets/js/plugins/table-edits.min.js'),

    //pages
    'edit-student'         => array('name' => 'assets/js/admin/edit-student.js'),
    'student-upgrade'      => array('name' => 'assets/js/admin/student-upgrade.js'),
    'student-readmission'  => array('name' => 'assets/js/admin/student-readmission.js'),
    'student-registration' => array('name' => 'assets/js/admin/student-registration.js'),
    'student-list'         => array('name' => 'assets/js/admin/student-list.js'),

    'subject-and-exam-settings' => array('name' => 'assets/js/admin/subject-and-exam-settings.js'),
    'request-result'            => array('name' => 'assets/js/admin/request-result.js'),
    'result'            => array('name' => 'assets/js/admin/result.js'),
);
