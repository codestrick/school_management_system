<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

if ( ! isset($config))
{
    $config = array();
}


$config = array_merge($config, array(
    'publisher_listing_headers' => array(
        'checkbox'     => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'publisher_name' => array(
            'jsonField' => 'publisher_name',
            'width'     => '25%'
        ),
        'created_at'   => array(
            'jsonField' => 'created_at',
            'width'     => '15%'
        ),
        'updated_at'   => array(
            'jsonField' => 'updated_at',
            'width'     => '15%'
        ),
        'action'     => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                //                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON' => 'id',
                //                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON' => 'lib/publisher/edit/',
                //                'DELETE_ICON' => 'admin/session/delete/'
            ),
        )
    )
));

$config = array_merge($config, array(
    'author_listing_headers' => array(
        'checkbox'     => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'author_name' => array(
            'jsonField' => 'author_name',
            'width'     => '25%'
        ),
        'created_at'   => array(
            'jsonField' => 'created_at',
            'width'     => '15%'
        ),
        'updated_at'   => array(
            'jsonField' => 'updated_at',
            'width'     => '15%'
        ),
        'action'     => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                //                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON' => 'id',
                //                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON' => 'lib/author/edit/',
                //                'DELETE_ICON' => 'admin/session/delete/'
            ),
        )
    )
));

$config = array_merge($config, array(
    'book_listing_headers' => array(
        'checkbox'     => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
        'book_name' => array(
            'jsonField' => 'book_name',
            'width'     => '25%'
        ),
        'author_name' => array(
            'jsonField' => 'author_name',
            'width'     => '25%'
        ),
        'publisher_name' => array(
            'jsonField' => 'publisher_name',
            'width'     => '25%'
        ),
        'created_at'   => array(
            'jsonField' => 'created_at',
            'width'     => '15%'
        ),
        'updated_at'   => array(
            'jsonField' => 'updated_at',
            'width'     => '15%'
        ),
        'action'     => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                //                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON' => 'id',
                //                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON' => 'lib/book/edit/',
                //                'DELETE_ICON' => 'admin/session/delete/'
            ),
        )
    )
));




$config = array_merge($config, array(
    'book_access_id_listing_headers' => array(
        'checkbox'     => array(
            'isSortable'     => FALSE,
            'isParseAble'    => TRUE,
            'systemDefaults' => TRUE,
            'width'          => '5%',
            'align'          => 'center'
        ),
		'id' => array(
            'jsonField' => 'id',
            'width'     => '25%'
        ),
        'book_name' => array(
            'jsonField' => 'book_name',
            'width'     => '25%'
        ),
		
        
        'created_at'   => array(
            'jsonField' => 'created_at',
            'width'     => '15%'
        ),
        'updated_at'   => array(
            'jsonField' => 'updated_at',
            'width'     => '15%'
        ),
        'action'     => array(
            'isSortable'     => FALSE,
            'systemDefaults' => TRUE,
            'isLink'         => TRUE,
            'width'          => '5%',
            'align'          => 'center',
            'type'           => array(
                'EDIT_ICON',
                //                'DELETE_ICON'
            ),
            'linkParams'     => array(
                'EDIT_ICON' => 'id',
                //                'DELETE_ICON' => 'id'
            ),
            'linkTarget'     => array(
                'EDIT_ICON' => 'lib/book-access-id/editt/',
                //                'DELETE_ICON' => 'admin/session/delete/'
            ),
        )
    )
));