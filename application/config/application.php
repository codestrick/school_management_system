<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


$config['app_config'] = array(
    'admin_user'                 => 'admin',
    'admin_pass'                 => '21232f297a57a5a743894a0e4a801fc3',
    'admin_pass_encryption_type' => 'MD5',
    'admin_name'                 => 'Koushik Samanta',
    'admin_email'                => 'koushiksamanta034@gmail.com',
    'admin_mobile'               => '8768755493',
);

$config['app_config_other'] = array(
    'admin_user'                 => 'admin',
    'admin_pass'                 => '$2a$10$3lRLZR432mLdYQBuVTNy2OiUVi5qpdiU9XvG9oJLcgSvnplIEcTiu',
    'admin_pass_encryption_type' => 'password_encrypt',
    'admin_name'                 => 'Koushik Samanta',
    'admin_email'                => 'koushiksamanta034@gmail.com',
    'admin_mobile'               => '8768755493',
);