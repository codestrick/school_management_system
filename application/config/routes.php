<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
/**
 * Default Routes
 */
$route['default_controller']   = 'public/index/index';
$route['404_override']         = '';
$route['translate_uri_dashes'] = FALSE;


/**
 * Public Routes
 */
$route['home']    = 'public/index/home';
$route['about']   = 'public/index/about';
$route['gallery'] = 'public/index/gallery';
$route['contact'] = 'public/index/contact';

/**
 * Auth
 */
$route['admin']                 = 'admin/index';
$route['admin/login']           = 'auth/index/login';
$route['admin/forgot-password'] = 'auth/index/forgot_password';
$route['admin/reset-password']  = 'auth/index/reset_password';
$route['admin/logout']          = 'auth/index/logout';

/**
 * Admin Routes
 */
$route['admin/profile']   = 'admin/profile/index';
$route['admin/dashboard'] = 'admin/index/dashboard';

$route['admin/letter-grade']        = 'admin/index/letter_grade';
$route['admin/letter-grade/(:any)'] = 'admin/index/letter_grade/$1';

/******************************* Session *****************************/
$route['admin/session/change'] = 'admin/session/change_session';

/****************************** Student *****************************/
$route['admin/student/add']                 = 'admin/student/add';
$route['admin/student/list']                = 'admin/student/index';
$route['admin/student/edit/(:num)']         = 'admin/student/student_edit/$1';
$route['admin/student/upgrade/(:num)']      = 'admin/student/student_upgradation/$1';
$route['admin/student/re-admission/(:num)'] = 'admin/student/student_readmission/$1';
$route['admin/student/upgradation']         = 'admin/student/search_student';
$route['admin/student/readmission']         = 'admin/student/search_student';
$route['admin/student/delete/(:num)']       = 'admin/student/delete/$1';
$route['admin/student/(:num)/details']      = 'admin/student/view/$1';

$route['admin/student/get-student-list-ajax']        = 'admin/student/get_student_list_ajax';
$route['admin/student/ajax-get-subject-combination'] = 'admin/student/get_subject_combination_ajax';
$route['admin/student/ajax-save-student']            = 'admin/student/save_student_ajax';
$route['admin/student/ajax-get-sub-caste-by-caste']  = 'admin/student/get_sub_caste_by_caste_ajax';
$route['admin/student/ajax-get-section-by-class']    = 'admin/student/get_sub_section_by_class_ajax';
$route['admin/student/ajax-update-student']          = 'admin/student/update_student';
$route['admin/student/ajax-upgrade-student']         = 'admin/student/save_student_upgradation';
$route['admin/student/ajax-readmission-student']     = 'admin/student/save_student_readmission';

/****************************** Session *****************************/
$route['admin/session/add']           = 'admin/session/add';
$route['admin/session/list']          = 'admin/session/index';
$route['admin/session/edit/(:num)']   = 'admin/session/add/$1';
$route['admin/session/delete/(:num)'] = 'admin/session/delete/$1';

/****************************** Address *****************************/
$route['admin/address/add']              = 'admin/address/add';
$route['admin/address/list']             = 'admin/address/index';
$route['admin/address/edit/(:num)']      = 'admin/address/add/$1';
$route['admin/address/delete/(:num)']    = 'admin/address/delete/$1';
$route['admin/address/ajax-get-address'] = 'admin/address/ajax_get_address';

/****************************** Old School *****************************/
$route['admin/old-school/add']           = 'admin/old_school/add';
$route['admin/old-school/list']          = 'admin/old_school/index';
$route['admin/old-school/edit/(:num)']   = 'admin/old_school/add/$1';
$route['admin/old-school/delete/(:num)'] = 'admin/old_school/delete/$1';

/****************************** Religion *****************************/
$route['admin/religion/add']           = 'admin/religion/add';
$route['admin/religion/list']          = 'admin/religion/index';
$route['admin/religion/edit/(:num)']   = 'admin/religion/add/$1';
$route['admin/religion/delete/(:num)'] = 'admin/religion/delete/$1';

/****************************** Cast *****************************/
$route['admin/caste/add']           = 'admin/caste/add';
$route['admin/caste/list']          = 'admin/caste/index';
$route['admin/caste/edit/(:num)']   = 'admin/caste/add/$1';
$route['admin/caste/delete/(:num)'] = 'admin/caste/delete/$1';


/****************************** Sub Cast *****************************/
$route['admin/sub-caste/add']           = 'admin/sub_caste/add';
$route['admin/sub-caste/list']          = 'admin/sub_caste/index';
$route['admin/sub-caste/edit/(:num)']   = 'admin/sub_caste/add/$1';
$route['admin/sub-caste/delete/(:num)'] = 'admin/sub_caste/delete/$1';

/****************************** Occupation *****************************/
$route['admin/occupation/add']           = 'admin/occupation/add';
$route['admin/occupation/list']          = 'admin/occupation/index';
$route['admin/occupation/edit/(:num)']   = 'admin/occupation/add/$1';
$route['admin/occupation/delete/(:num)'] = 'admin/occupation/delete/$1';

/****************************** Subject *****************************/
$route['admin/subject/add']           = 'admin/subject/add';
$route['admin/subject/list']          = 'admin/subject/index';
$route['admin/subject/edit/(:num)']   = 'admin/subject/add/$1';
$route['admin/subject/delete/(:num)'] = 'admin/subject/delete/$1';

/****************************** Section *****************************/
$route['admin/section/add']           = 'admin/section/add';
$route['admin/section/list']          = 'admin/section/index';
$route['admin/section/edit/(:num)']   = 'admin/section/add/$1';
$route['admin/section/delete/(:num)'] = 'admin/section/delete/$1';

/****************************** Class *****************************/
$route['admin/class/add']           = 'admin/classes/add';
$route['admin/class/list']          = 'admin/classes/index';
$route['admin/class/edit/(:num)']   = 'admin/classes/add/$1';
$route['admin/class/delete/(:num)'] = 'admin/classes/delete/$1';

/****************************** Class & Subject Settings *****************************/
$route['admin/settings/add']         = 'admin/subject_exam_settings/settings_add';
$route['admin/settings/edit/(:num)'] = 'admin/subject_exam_settings/settings_add/$1';

$route['admin/settings/exam-settings-add/(:num)']  = 'admin/subject_exam_settings/exam_settings_add/$1';
$route['admin/settings/view-exam-settings/(:num)'] = 'admin/subject_exam_settings/view_exam_settings/$1';


/***************************** Result **********************************************/
$route['admin/results']                           = 'admin/resultsxx/index';
$route['admin/results/empty-excel-file-download'] = 'admin/resultsxx/empty_excel_file_download';
$route['admin/result/student-search']             = 'admin/result/result/search_student';
$route['admin/result/result-form']                = 'admin/result/result/result_form';
$route['admin/result/result-form/(:num)/(:num)']  = 'admin/result/result/result_form/$1/$2';
$route['admin/generate/result']                   = 'admin/result/result/generate_result';
$route['admin/result/ajax-result-save']           = 'admin/result/result/ajax_submit_result';


/***************************** Faculty **********************************************/
$route['admin/faculty/add']           = 'admin/faculty/add';
$route['admin/faculty/list']          = 'admin/faculty/index';
$route['admin/faculty/edit/(:num)']   = 'admin/faculty/add/$1';
$route['admin/faculty/delete/(:num)'] = 'admin/faculty/delete/$1';

///////////////////////////////
// routes for lib management
//////////////////////////////

$route['lib']           = 'library-sys/index';
$route['lib/dashboard'] = 'library-sys/index';

/************************************ library-sys publisher ********************************/
$route['lib/publisher/add']         = 'library-sys/publisher/add';
$route['lib/publisher/edit/(:num)'] = 'library-sys/publisher/add/$1';
$route['lib/publisher/list']        = 'library-sys/publisher/index';

/**********************************library-sys author********************************************/
$route['lib/author/add']         = 'library-sys/author/add';
$route['lib/author/edit/(:num)'] = 'library-sys/author/add/$1';
$route['lib/author/list']        = 'library-sys/author/index';

/*****************************library-sys book **************************************************/
$route['lib/book/add']         = 'library-sys/book/add';
$route['lib/book/edit/(:num)'] = 'library-sys/book/add/$1';
$route['lib/book/list']        = 'library-sys/book/index';

/*****************************lybrary-sys book_access_id**********************************************************/

$route['lib/book-access-id/add'] = 'library-sys/book_access_id/add';
//$route['lib/book-access-id/edit/(:num)'] = 'library-sys/book_access_id/add/$1';
$route['lib/book-access-id/list'] = 'library-sys/book_access_id/index';