<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

$config['css_default'] = array(
    'bootstrap'    => array('name' => 'assets/public/css/bootstrap.css'),
    'font-awesome' => array('name' => 'assets/public/css/font-awesome.css'),
    'style'        => array('name' => 'assets/public/css/style.css'),
);

$config['js_default'] = array(
    'jquery'    => array('name' => 'assets/public/js/jquery-2.1.4.min.js'),
    'move-top'  => array('name' => 'assets/public/js/move-top.js'),
    'easing'    => array('name' => 'assets/public/js/easing.js'),
    'bootstrap' => array('name' => 'assets/public/js/bootstrap.js'),

    'common' => array('name' => 'assets/public/js/common.js'),
);

$config['css_arr'] = array(
    'flexslider'   => array('name' => 'assets/public/css/flexslider.css'),
    'owl.carousel' => array('name' => 'assets/public/css/owl.carousel.css'),
    'lsb'          => array('name' => 'assets/public/css/lsb.css'),
);

$config['js_arr'] = array(
    'jquery.flexslider' => array('name' => 'assets/public/js/jquery.flexslider.js'),
    'owl.carousel'      => array('name' => 'assets/public/js/owl.carousel.js'),
    'slider'            => array('name' => 'assets/public/js/pages/slider.js'),
    'lsb.min'           => array('name' => 'assets/public/js/lsb.min.js'),
    'gallery'           => array('name' => 'assets/public/js/pages/gallery.js'),
);
