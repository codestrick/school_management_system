<?php $admin_session = get_current_logged_in_session(); ?>

<!-- Main sidebar -->
<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a class="media-left"><img src="<?php echo base_url(); ?>assets/images/user_icon.png" class="img-circle img-sm" alt=""></a>
                    <div class="media-body">
                        <span class="media-heading text-semibold"><?php echo ! empty($admin_session) ? $admin_session['name'] : 'Guest'; ?></span>
                        <div class="text-size-mini text-muted">
                            <i class="fa fa-user-secret"></i> &nbsp; Library
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->

        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <?php $initial_info_menu = ['session', 'old-school', 'cast', 'religion', 'occupation', 'address', 'subject', 'section', 'class', 'sub-cast']; ?>

                    <!-- Main -->
                    <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                    <li class="<?php echo($this->uri->segment(2) == 'dashboard' ? 'active' : ''); ?>">
                        <a href="<?php echo base_url('lib/dashboard'); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a>
                    </li>
                    <li class="<?php echo ($this->uri->segment(2) == 'author') ? 'active' : ''; ?>">
                        <a href="#"><i class="fa fa-users"></i> <span>Author</span></a>
                        <ul>
                            <li class="<?php echo($this->uri->segment(2) == 'author' && $this->uri->segment(3) == 'add' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('lib/author/add'); ?>"><i class="fa fa-plus"></i> Add Author</a>
                            </li>
                            <li class="<?php echo($this->uri->segment(2) == 'author' && $this->uri->segment(3) == 'list' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('lib/author/list'); ?>"><i class="fa fa-list"></i> List Author</a>
                            </li>
                        </ul>
                    </li>


                    <li class="<?php echo ($this->uri->segment(2) == 'book') ? 'active' : ''; ?>">
                        <a href="#"><i class="fa fa-users"></i> <span>Book</span></a>
                        <ul>
                            <li class="<?php echo($this->uri->segment(2) == 'book' && $this->uri->segment(3) == 'add' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('lib/book/add'); ?>"><i class="fa fa-plus"></i> Add Book</a>
                            </li>
                            <li class="<?php echo($this->uri->segment(2) == 'book' && $this->uri->segment(3) == 'list' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('lib/book/list'); ?>"><i class="fa fa-list"></i> List Book</a>
                            </li>
                        </ul>
                    </li>

                    <li class="<?php echo ($this->uri->segment(2) == 'publisher') ? 'active' : ''; ?>">
                        <a href="#"><i class="fa fa-users"></i> <span>Publisher</span></a>
                        <ul>
                            <li class="<?php echo($this->uri->segment(2) == 'publisher' && $this->uri->segment(3) == 'add' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('lib/publisher/add'); ?>"><i class="fa fa-plus"></i> Add Publisher</a>
                            </li>
                            <li class="<?php echo($this->uri->segment(2) == 'publisher' && $this->uri->segment(3) == 'list' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('lib/publisher/list'); ?>"><i class="fa fa-list"></i> List Publisher</a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
        <!-- /main navigation -->
    </div>
</div>
<!-- /main sidebar -->