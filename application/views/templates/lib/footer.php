<!-- Footer -->
<div class="footer text-muted">
    &copy; <?php echo date('Y'); ?>. <a>Admin</a> | Developed & Maintained by <a>CodeScript</a>
</div>
<!-- /footer -->