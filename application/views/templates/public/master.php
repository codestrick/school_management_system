<!DOCTYPE html>
<html lang="zxx">
<head>
    <title><?php echo ! empty($page_title) ? $page_title : ''; ?></title>

    <!-- Meta-Tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content=""/>

    <link rel="icon" href="<?php echo base_url('assets/images/167707.png'); ?>" type="image/png">

    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar()
        {
            window.scrollTo(0, 1);
        } </script>
    <!-- //Meta-Tags -->

    <!-- Custom-Stylesheet-Links -->
    <?php echo $template_css; ?>

    <!-- Headings-font -->
    <link href="//fonts.googleapis.com/css?family=Hind+Vadodara:300,400,500,600,700" rel="stylesheet">
    <!-- Body-font -->
    <link href="//fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
    <!--//web-fonts-->

    <script type="text/javascript">
        var base_url = '<?php echo base_url(); ?>';
    </script>

</head>
<body>

<!-- header -->
<?php echo $template_header; ?>
<!-- //header -->

<!-- content -->
<?php echo $template_content; ?>
<!-- //content -->

<!--footer-->
<?php echo $template_footer; ?>
<!--//footer-->

<?php echo $template_js; ?>

</body>
</html>