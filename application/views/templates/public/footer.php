<div class="footer w3layouts">
    <div class="container">
        <div class="footer-row w3layouts-agile">
            <div class="col-md-4 footer-grids w3l-agileits">
                <h6><a href="<?php echo base_url(); ?>">Scholar Vision</a></h6>
                <p class="footer-one-w3ls">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas pulvinar tellus sed mauvehicula tempor. </p>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-2 footer-grids w3l-agileits">
                <h3>Footer Menu</h3>
                <ul class="b-nav">
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <li><a href="<?php echo base_url('about'); ?>">About Us</a></li>
                    <li><a href="<?php echo base_url('gallery'); ?>">Gallery</a></li>
                    <li><a href="<?php echo base_url('contact'); ?>">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-md-3 footer-grids w3l-agileits">
                <h3>Contact Info</h3>
                <p>Virginia, USA</p>
                <p>El Montee RV, Sterling USA</p>
                <p><a>+0 097 338 004</a></p>
                <p><a>mail@example.com</a></p>
            </div>
            <div class="col-md-3 footer-grids w3l-agileits">
                <h3>Social Links</h3>
                <div class="top-header-agile-right">
                    <ul>
                        <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="copyright-wthree">
    <p>&copy; <?php echo date('Y'); ?> Scholar Vision . All Rights Reserved | Design by <a> W3layouts </a></p>
</div>

<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>