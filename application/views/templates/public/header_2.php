<div class="banner inner-banner-w3-agileits" id="home">
    <div class="banner-overlay-agileinfo">
        <div class="top-header-agile">
            <h1><a class="col-md-4 navbar-brand" href="<?php echo base_url('home'); ?>">Scholar Vision<span>Education for everyone</span></a></h1>
            <div class="col-md-4 top-header-agile-right">
                <ul>
                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    <li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="col-md-4 top-header-agile-left">
                <ul class="num-w3ls">
                    <li><i class="fa fa-envelope" aria-hidden="true"></i></li>
                    <li>info@scool.com</li>
                    <li><i class="fa fa-phone" aria-hidden="true"></i></li>
                    <li>+0 097 338 004</li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-header navbar-left">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                    <nav class="link-effect-3" id="link-effect-3">
                        <ul class="nav navbar-nav">
                            <li class="<?php echo ($this->uri->segment(1) == 'home') ? 'active' : ''; ?>"><a href="<?php echo base_url('home'); ?>" data-hover="Home">Home</a></li>
                            <li class="<?php echo ($this->uri->segment(1) == 'about') ? 'active' : ''; ?>"><a href="<?php echo base_url('about'); ?>" data-hover="About Us">About Us</a></li>
                            <li class="<?php echo ($this->uri->segment(1) == 'gallery') ? 'active' : ''; ?>"><a href="<?php echo base_url('gallery'); ?>" data-hover="Gallery">Gallery</a></li>
                            <li class="<?php echo ($this->uri->segment(1) == 'contact') ? 'active' : ''; ?>"><a href="<?php echo base_url('contact'); ?>" data-hover="Mail Us">Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </nav>
            <h2 class="inner-tittle-w3layouts"><?php echo ! empty($page_title) ? $page_title : ''; ?></h2>
        </div>
    </div>
</div>