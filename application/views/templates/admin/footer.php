<!-- Footer -->
<div class="footer text-muted">
    &copy; <?php echo date('Y'); ?>. <a href="<?php echo base_url(); ?>" target="_blank">SCHOOL MANAGEMENT SYSTEM</a> | Developed & Maintained by <a>CodeScript</a>
</div>
<!-- /footer -->