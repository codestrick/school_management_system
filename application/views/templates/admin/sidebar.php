<?php $admin_session = get_current_logged_in_session(); ?>

<!-- Main sidebar -->
<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user" style="">
            <div class="category-content">
                <div class="media">
                    <a class="media-left" href="<?php echo base_url('admin/profile'); ?>">
                        <img src="<?php echo base_url('assets/images/user_icon.png'); ?>" class="img-circle img-sm" alt="">
                    </a>
                    <div class="media-body">
                        <span class="media-heading text-semibold"><?php echo ! empty($admin_session) ? $admin_session['name'] : 'Guest'; ?></span>
                        <div class="text-size-mini text-muted">
                            <i class="fa fa-user-secret"></i> &nbsp; Admin
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->
        <hr>
        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <?php $initial_info_menu = ['session', 'old-school', 'cast', 'religion', 'occupation', 'address', 'subject', 'section', 'class', 'sub-cast']; ?>

                    <!-- Main -->
                    <li class="<?php echo($this->uri->segment(2) == 'dashboard' ? 'active' : ''); ?>">
                        <a href="<?php echo base_url('admin/dashboard'); ?>"><i class="icon-home4"></i> <span>Dashboard</span></a>
                    </li>
                    <li class="<?php echo(in_array($this->uri->segment(2), $initial_info_menu) ? 'active' : ''); ?>">
                        <a href="#"><i class="icon-grid2"></i> <span>Initial Information</span></a>
                        <ul>
                            <li class="<?php echo($this->uri->segment(2) == 'session' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('admin/session/list'); ?>"><i class="fa fa-calendar"></i> Session</a>
                            </li>
                            <li class="<?php echo($this->uri->segment(2) == 'class' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('admin/class/list'); ?>"><i class="fa fa-copyright"></i> Class</a>
                            </li>
                            <li class="<?php echo($this->uri->segment(2) == 'section' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('admin/section/list'); ?>"><i class="fa fa-cubes"></i> Section</a>
                            </li>
                            <li class="<?php echo($this->uri->segment(2) == 'subject' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('admin/subject/list'); ?>"><i class="fa fa-language"></i> Subject</a>
                            </li>
                            <li class="<?php echo($this->uri->segment(2) == 'letter-grade' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('admin/letter-grade/list'); ?>"><i class="fa fa-bookmark-o"></i> Letter Grade</a>
                            </li>
                            <li class="<?php echo($this->uri->segment(2) == 'religion' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('admin/religion/list'); ?>"><i class="fa fa-registered"></i> Religion</a>
                            </li>
                            <li class="<?php echo($this->uri->segment(2) == 'caste' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('admin/caste/list'); ?>"><i class="fa fa-mars"></i> Caste</a>
                            </li>
                            <li class="<?php echo($this->uri->segment(2) == 'sub-caste' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('admin/sub-caste/list'); ?>"><i class="fa fa-mars-double"></i> Sub Caste</a>
                            </li>
                            <li class="<?php echo($this->uri->segment(2) == 'occupation' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('admin/occupation/list'); ?>"><i class="fa fa-briefcase"></i> Occupation</a>
                            </li>
                        </ul>
                    </li>
                    <li class="<?php echo ($this->uri->segment(2) == 'student') ? 'active' : ''; ?>">
                        <a href="#"><i class="fa fa-users"></i> <span>Student</span></a>
                        <ul>
                            <li class="<?php echo($this->uri->segment(2) == 'student' && $this->uri->segment(3) == 'add' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('admin/student/add'); ?>"><i class="fa fa-plus"></i> Add Student</a>
                            </li>
                            <li class="<?php echo($this->uri->segment(2) == 'student' && $this->uri->segment(3) == 'upgradation' || $this->uri->segment(3) == 'upgrade' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('admin/student/upgradation'); ?>"><i class="icon-user-check"></i> Upgradation</a>
                            </li>
                            <li class="<?php echo($this->uri->segment(2) == 'student' && $this->uri->segment(3) == 'readmission' || $this->uri->segment(3) == 're-admission' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('admin/student/readmission'); ?>"><i class="fa fa-user-plus"></i> Readmission</a>
                            </li>
                            <li class="<?php echo($this->uri->segment(2) == 'student' && $this->uri->segment(3) == 'list' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('admin/student/list'); ?>"><i class="fa fa-list"></i> List Student</a>
                            </li>
                        </ul>
                    </li>
                    <li class="<?php echo ($this->uri->segment(2) == 'results') ? 'active' : ''; ?>">
                        <a href="#"><i class="icon-address-book2"></i> <span>Result</span></a>
                        <ul>
<!--                            <li class="--><?php //echo($this->uri->segment(2) == 'student' && $this->uri->segment(3) == 'empty-excel-file-download' ? 'active' : ''); ?><!--">-->
<!--                                <a href="--><?php //echo base_url('admin/results/empty-excel-file-download'); ?><!--"><i class="fa fa-plus"></i> Request For Excel Report</a>-->
<!--                            </li>-->
                            <li class="<?php echo($this->uri->segment(2) == 'result' && $this->uri->segment(3) == 'result-list' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('admin/result/list'); ?>"><i class="fa fa-plus"></i>All Result</a>
                            </li>
                        </ul>
                    </li>
                    <li class="<?php echo ($this->uri->segment(2) == 'settings') ? 'active' : ''; ?>">
                        <a href="<?php echo base_url('admin/settings/add'); ?>"><i class="icon-address-book2"></i> <span>Exam Setting</span></a>
                    </li>

                    <li class="navigation-divider"></li>

                    <li class="<?php echo ($this->uri->segment(2) == 'faculty') ? 'active' : ''; ?>">
                        <a href="#"><i class="fa fa-user-secret"></i> <span>Faculty</span></a>
                        <ul>
                            <li class="<?php echo($this->uri->segment(2) == 'faculty' && $this->uri->segment(3) == 'add' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('admin/faculty/add'); ?>"><i class="fa fa-plus"></i> Add Faculty</a>
                            </li>
                            <li class="<?php echo($this->uri->segment(2) == 'faculty' && $this->uri->segment(3) == 'list' ? 'active' : ''); ?>">
                                <a href="<?php echo base_url('admin/faculty/list'); ?>"><i class="fa fa-list"></i> List Faculty</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navigation -->
    </div>
</div>
<!-- /main sidebar -->