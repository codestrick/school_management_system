<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo ! empty($page_title) ? $page_title . ' | Admin' : 'Admin'; ?></title>
    <link rel="icon" href="<?php echo base_url('assets/images/167707.png'); ?>" type="image/png">

    <!-- Global stylesheets -->
    <link href="//fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">

    <?php echo $template_css; ?>
    <?php echo $template_js; ?>

    <script type="text/javascript">
        var base_url = '<?php echo base_url(); ?>';
    </script>

</head>

<body class="navbar-top">

<?php echo $template_header; ?>

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <?php echo $template_sidebar; ?>

        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
            <div class="page-header page-header-default">
                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="<?php echo base_url('admin/dashboard'); ?>"><i class="icon-home2 position-left"></i> Home</a></li>
                        <li class="active"><?php echo ! empty($breadcrumb) ? $breadcrumb : ''; ?></li>
                    </ul>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">

                <?php echo $template_content; ?>

                <?php echo $template_footer; ?>

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->
</body>
</html>
