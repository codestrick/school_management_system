<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

///////////////////////////////
// table headers for student management
//////////////////////////////

$lang['session_listing_headers'] = array(
    'checkbox'     => '',
    'ses_start_yr' => 'Session Start Year',
    'ses_end_yr'   => 'Session End Year',
    'created_at'   => 'Created',
    'updated_at'   => 'Modified',
);

$lang['student_listing_headers'] = array(
    'checkbox' => '',
    'std_name' => 'Name',
    'dob'      => 'DOB',
    'age'      => 'Age',
);

$lang['old_school_listing_headers'] = array(
    'checkbox'        => '',
    'old_school_name' => 'Old School Name',
    'address'         => 'Address',
    'created_at'      => 'Created',
    'updated_at'      => 'Modified',
    'edit'            => 'Action',
);

$lang['religion_listing_headers']   = array(
    'checkbox'      => '',
    'religion_name' => 'Religion Name',
    'created_at'    => 'Created',
    'updated_at'    => 'Modified',
    'edit'          => 'Action',
);
$lang['occupation_listing_headers'] = array(
    'checkbox'        => '',
    'occupation_name' => 'Occupation Name',
    'created_at'      => 'Created',
    'updated_at'      => 'Modified',
    'edit'            => 'Action',
);

$lang['section_listing_headers'] = array(
    'checkbox'     => '',
    'section_name' => 'Section Name',
    'class_name'   => 'Class Name',
    'updated_at'   => 'Modified',
    'edit'         => 'Action',
);

$lang['caste_listing_headers'] = array(
    'checkbox'   => '',
    'caste_name' => 'Caste Name',
    'created_at' => 'Created',
    'updated_at' => 'Modified',
    'edit'       => 'Action',
);

$lang['sub_caste_listing_headers'] = array(
    'checkbox'       => '',
    'caste_name'     => 'Caste Name',
    'sub_caste_name' => 'Sub Caste Name',
    'created_at'     => 'Created',
    'updated_at'     => 'Modified',
    'edit'           => 'Action',
);

$lang['subject_listing_headers'] = array(
    'checkbox'     => '',
    'subject_name' => 'Subject Name',
    'updated_at'   => 'Modified',
    'edit'         => 'Action',
);

$lang['address_listing_headers'] = array(
    'checkbox'   => '',
    'address'    => 'Address',
    'pincode'    => 'Pincode',
    'updated_at' => 'Modified',
    'edit'       => 'Action',
);

$lang['class_listing_headers'] = array(
    'checkbox'   => '',
    'class_name' => 'Class Name',
    'class_type' => 'Class Type',
    'updated_at' => 'Modified',
    'edit'       => 'Action',
);

$lang['faculty_listing_headers'] = array(
    'checkbox'     => '',
    'name'         => 'Name',
    'email'        => 'E-mail',
    'mobile'       => 'Mobile',
//    'address'      => 'Address',
    'subject_name' => 'Subject',
    'edit'         => 'Action',
);

///////////////////////////////
// table headers for lib management
//////////////////////////////

$lang['publisher_listing_headers'] = array(
    'checkbox'       => '',
    'publisher_name' => 'Publisher Name',
    'created_at'     => 'Created',
    'updated_at'     => 'Modified',
    'edit'           => 'Action'
);


$lang['author_listing_headers'] = array(
    'checkbox'    => '',
    'author_name' => 'Author Name',
    'created_at'  => 'Created',
    'updated_at'  => 'Modified',
    'edit'        => 'Action'
);

$lang['book_listing_headers'] = array(
    'checkbox'       => '',
    'book_name'      => 'Book Name',
    'author_name'    => 'Author Name',
    'publisher_name' => 'Publisher Name',
    'created_at'     => 'Created',
    'updated_at'     => 'Modified',
    'edit'           => 'Action'
);

$lang['book_access_id_listing_headers'] = array(
    'checkbox'   => '',
    'id'         => 'ID',
    'book_name'  => 'Book ',
    'created_at' => 'Created',
    'updated_at' => 'Modified',
    'edit'       => 'Action'
);
