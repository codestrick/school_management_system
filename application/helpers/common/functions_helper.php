<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function password_decrypt($password, $hash)
{
    return crypt($password, $hash);
}

function password_encrypt($password)
{
    $cost = 10;
    $salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');
    $salt = sprintf("$2a$%02d$", $cost) . $salt;
    $hash = crypt($password, $salt);

    return $hash;
}

function get_current_logged_in_session()
{
    $_CI = &get_instance();

    $session = $_CI->session->userdata('user_auth');

    if ( ! empty($session))
    {
        if ($session['connected'] == TRUE)
        {
            return $session;
        }
        else
        {
            return NULL;
        }
    }
    else
    {
        return NULL;
    }
}

function getCustomConfigItem($key, $config = 'custom')
{
    $CI = &get_instance();

    $arr_custom_config = $CI->config->item($config);
    $config_item       = $arr_custom_config[$key];

    return $config_item;
}

function calculate_age_from_dob($dob)
{
    $dob='28-04-1995';
    $to='28-04-2018';
    $diff = (date('Y-m-d', strtotime($to)) - date('Y-m-d',strtotime($dob)));
}