<?php defined('BASEPATH') OR exit('No direct script access allowed');

class App_model extends MY_Model
{
    public $tbl_name = 'std_registration';

    /**
     * App_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param string $tbl_name
     * @param array  $params
     * @param null   $id
     *
     * @return null
     */
    function save_details($tbl_name = '',$params = [], $id = NULL)
    {
        $return = NULL;

        if ( ! empty($id))
        {
            $this->db->update($tbl_name, $params, ['id' => $id]);
            $return = $id;
        }
        else
        {
            $this->db->insert($tbl_name, $params);
            $return = $this->db->insert_id();
        }

        return $return;
    }

    /**
     * @param string $tbl_name
     * @param array  $params
     * @param array  $conditions
     *
     * @return null
     */
    function save_details_by_conditions($tbl_name = '',$params = [], $conditions = [])
    {
        $return = NULL;

        if ( ! empty($conditions))
        {
            $return = $this->db->update($tbl_name, $params, $conditions);
        }
        else
        {
            $this->db->insert($tbl_name, $params);
            $return = $this->db->insert_id();
        }

        return $return;
    }

    /**
     * @param string $tbl_name
     * @param array  $params
     * @param string $return_type
     *
     * @return null
     */
    function get_details_by($tbl_name = '',$params = [], $return_type = 'row_array')
    {
        $result = NULL;

        if ($return_type == 'result_array')
        {
            $result = $this->db->get_where($tbl_name, $params)->result_array();
        }
        else
        {
            $result = $this->db->get_where($tbl_name, $params)->row_array();
        }

        return $result;
    }

    /**
     * @param $tbl_name
     * @param $params
     *
     * @return array
     */
    function delete_details($tbl_name, $params)
    {
        $status = $this->db->delete($tbl_name, $params);

        if ($status == TRUE)
        {
            $statusMessage = 'Record successfully deleted.';
        }
        else
        {
            $statusMessage = 'Record delete error.';
        }

        return [
            'status' => $status,
            'msg'    => $statusMessage,
        ];
    }
}
