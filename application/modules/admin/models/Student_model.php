<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Student_model extends MY_Model
{
    public $tbl_name = 'std_registration';

    /**
     * Student_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array $params
     * @param null  $id
     *
     * @return null
     */
    function save_student($params = [], $id = NULL)
    {
        $return = NULL;

        if ( ! empty($id))
        {
            $this->db->update($this->tbl_name, $params, ['id' => $id]);
            $return = $id;
        }
        else
        {
            $this->db->insert($this->tbl_name, $params);
            $return = $this->db->insert_id();
        }

        return $return;
    }

    /**
     * @param array  $params
     * @param string $return_type
     *
     * @return null
     */
    function get_student_detail_by($params = [], $return_type = 'row_array')
    {
        $result = NULL;

        if ($return_type == 'result_array')
        {
            $result = $this->db->get_where($this->tbl_name, $params)->result_array();
        }
        else
        {
            $result = $this->db->get_where($this->tbl_name, $params)->row_array();
        }

        return $result;
    }

    /**
     * @param bool   $limit_offset_status
     * @param int    $limit
     * @param int    $offset
     * @param null   $search_value
     * @param string $sortable_column
     * @param string $sortType
     *
     * @return mixed
     */
    function get_student_details_list_by_condition($limit_offset_status = FALSE, $limit = 0, $offset = 0, $search_value = NULL, $sortable_column = '', $sortType = '')
    {
        $this->db->select('std_registration.*');
        $this->db->from('std_registration');
        $this->db->join('student_extra_informations', 'student_extra_informations.student_id = std_registration.id');

        if ( ! empty($search_value))
        {
            $date_search = date('Y-m-d', strtotime($search_value));

            $this->db->where("(std_registration.reg_id LIKE '%$search_value%' ESCAPE '!'
                             OR std_registration.std_name LIKE '%$search_value%' ESCAPE '!'
                             OR std_registration.dob LIKE '$date_search' ESCAPE '!')");
        }

        if ( ! empty($sortable_column))
        {
            if ($sortable_column == 1)
            {
                $this->db->order_by("std_registration.reg_id", $sortType);
            }
            else if ($sortable_column == 2)
            {
                $this->db->order_by("std_registration.std_name", $sortType);
            }
            else if ($sortable_column == 3)
            {
                $this->db->order_by("std_registration.dob", $sortType);
            }
            else
            {
                $this->db->order_by("std_registration.std_name", 'ASC');
            }
        }

        if ($limit_offset_status == TRUE)
        {
            $this->db->limit($limit, $offset);
        }
        $this->db->where('student_extra_informations.session_id',$this->session->userdata['user_auth']['session']);
        return $this->db->get()->result_array();
    }

    /**
     * @param $params
     *
     * @return array
     */
    function delete_student($params)
    {
        $status = $this->db->delete($this->tbl_name, $params);

        if ($status == TRUE)
        {
            $this->db->delete('student_extra_informations', ['student_id' => $params['id']]);

            $statusMessage = 'Student record successfully deleted.';
        }
        else
        {
            $statusMessage = 'Student record delete error.';
        }

        return [
            'status' => $status,
            'msg'    => $statusMessage,
        ];
    }

    /**
     * @param null $student_id
     *
     * @return mixed
     */
    function get_student_details($student_id = NULL)
    {
        $this->db->select('std_registration.*, religion.*, caste.*, sub_caste.*, guardians.*');

        $this->db->join('religion', 'religion.id = std_registration.religion_id', 'left');
        $this->db->join('caste', 'caste.id = std_registration.caste_id', 'left');
        $this->db->join('sub_caste', 'sub_caste.id = std_registration.sub_caste_id', 'left');
        $this->db->join('guardians', 'guardians.id = std_registration.guardians_id', 'left');

        $this->db->where('std_registration.id', $student_id);

        return $this->db->get('std_registration')->row_array();
    }

    /**
     * @param null $student_id
     *
     * @return mixed
     */
    function get_student_admission_details($student_id = NULL)
    {
        $this->db->select('student_extra_informations.*, class.*, section.*, session.*, exam_settings_and_subject.*');

        $this->db->join('class', 'class.id = student_extra_informations.admission_class_id', 'left');
        $this->db->join('section', 'section.id = student_extra_informations.section_id', 'left');
        $this->db->join('session', 'session.id = student_extra_informations.session_id', 'left');
        $this->db->join('exam_settings_and_subject', 'exam_settings_and_subject.id = student_extra_informations.subject_combination_id', 'left');

        $this->db->where('student_extra_informations.student_id', $student_id);

        return $this->db->get('student_extra_informations')->result_array();
    }

    /**
     * @param array  $params
     * @param string $return_type
     *
     * @return null
     */
    function get_all_occupations($params = [], $return_type = 'row_array')
    {
        $return = NULL;

        if ($return_type == 'result_array')
        {
            $result = $this->db->get_where('occupation', $params)->result_array();

            if(!empty($result))
            {
                foreach ($result as $k => $v)
                {
                    $return[$v['id']] = $v['occupation_name'];
                }
            }
        }
        else
        {
            $return = $this->db->get_where('occupation', $params)->row_array();
        }

        return $return;
    }
}
