<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Letter_grade_model extends MY_Model
{
    public $tbl_name = 'letter_grade';

    /**
     * Session_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array $params
     * @param null  $id
     *
     * @return null
     */
    function save_letter_grade($params = [], $id = NULL)
    {
        $return = NULL;

        if ( ! empty($id))
        {
            $this->db->update($this->tbl_name, $params, ['id' => $id]);
            $return = $id;
        }
        else
        {
            $this->db->insert($this->tbl_name, $params);
            $return = $this->db->insert_id();
        }

        return $return;
    }

    /**
     * @param array  $params
     * @param string $return_type
     *
     * @return null
     */
    function get_letter_grade_detail_by($params = [], $return_type = 'row_array')
    {
        $result = NULL;

        if ($return_type == 'result_array')
        {
            $result = $this->db->get_where($this->tbl_name, $params)->result_array();
        }
        else
        {
            $result = $this->db->get_where($this->tbl_name, $params)->row_array();
        }

        return $result;
    }

}
