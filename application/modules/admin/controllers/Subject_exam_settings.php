<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subject_exam_settings extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array(
            'single_insert/Subject_model',
            'single_insert/Class_model',
            'Exam_settings_and_subject_model'
        ),
    );

    /**
     * Subject_exam_settings constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $settings_id
     */
    public function settings_add($settings_id = NULL)
    {
        $update_status = $this->insert_or_update_check($this->input->post('class_id'));

        $session_id = $_SESSION['user_auth']['session'];
        $no_of_exam = json_encode($this->input->post('no_of_exam'));

        $this->form_validation->set_rules('class_id', 'Class Name', "trim|required|exam_number_serially_selected_or_not[{$no_of_exam}]");
        $this->form_validation->set_rules('subject_combination_name', 'Subject Combination Name', "trim|required|unique[exam_settings_and_subject.subject_combination_name.id.{$update_status['settings_id']}]");

        if ( ! $this->form_validation->run())
        {
            $dataArray['allClass']    = $this->Class_model->get_class_detail_by([], 'result_array');
            $dataArray['allSubjects'] = $this->Subject_model->get_subject_detail_by([], 'result_array');

            $dataArray['css_local'] = array('');
            $dataArray['js_local']  = array('subject-and-exam-settings');

            $dataArray['exams'] = getCustomConfigItem('exam');

            $dataArray['form_action'] = 'Exam Settings';
            $dataArray['page_title']  = 'Exam Settings';
            $dataArray['breadcrumb']  = 'Exam Settings';

            $this->session->flashdata('flash_message', validation_errors());
            $this->session->flashdata('flash_message_status', FALSE);

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('subject-and-exam-settings/add-form', $dataArray);
        }
        else
        {
            $class_id     = $this->input->post('class_id');
            $subject_type = $this->input->post('subject_type');
            $no_of_exam   = $this->input->post('no_of_exam');

            $subjects                 = $this->input->post('subjects');
            $subject_combination_name = $this->input->post('subject_combination_name');

            $duplicate_subject_status = $this->check_duplicate_subject($subjects);

            if ($duplicate_subject_status == FALSE)
            {

                $params['session_id']               = $session_id;
                $params['class_id']                 = $class_id;
                $params['subject_type']             = $subject_type;
                $params['no_of_exam']               = json_encode($no_of_exam);
                $params['subjects']                 = json_encode($subjects);
                $params['subject_combination_name'] = $subject_combination_name;
                $params['exam_settings']            = json_encode([]);
                $params['session_id']               = $session_id;
                $params['class_id']                 = $class_id;
                $params['created_at']               = date("Y-m-d H:i:s");
                $params['updated_at']               = date("Y-m-d H:i:s");

                $new_settings_id = $this->Exam_settings_and_subject_model->save_exam_settings_and_subject($params);

                $this->session->set_flashdata('flash_message', 'Class and Subject settings successfully added');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/settings/exam-settings-add/' . $new_settings_id);
            }
            else
            {

                $this->session->set_flashdata('flash_message', 'Invalid operation performed!!');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/settings/add');
            }
        }

    }


    /**
     * @param $subjects
     *
     * @return bool
     */
    private function check_duplicate_subject($subjects)
    {
        $status = FALSE;

        if ( ! empty($subjects))
        {
            foreach (array_count_values($subjects) as $val => $c)
            {
                if ($c > 1)
                {
                    $status = TRUE;
                }
            }
        }

        return $status;
    }

    /**
     * @param $class_id
     *
     * @return array
     */
    private function insert_or_update_check($class_id)
    {
        $session_id = $_SESSION['user_auth']['session'];

        $Exam_settings_and_subject_info = $this->Exam_settings_and_subject_model->get_exam_settings_and_subject_detail_by(['session_id' => $session_id, 'class_id' => $class_id]);

        if (empty($Exam_settings_and_subject_info))
        {
            $return_value = 'insert';
        }
        else
        {
            $return_value = 'update';
        }

        return ['return_value' => $return_value, 'settings_id' => ! empty($Exam_settings_and_subject_info['id']) ? $Exam_settings_and_subject_info['id'] : NULL];
    }

    //http://localhost/school-management-system/index.php/admin/settings/exam-settings-add/3

    /**
     * @param $settings_id
     */
    public function exam_settings_add($settings_id)
    {
        $settings_det = $this->Exam_settings_and_subject_model->get_exam_settings_and_subject_detail_by(['id' => $settings_id], 'row_array');

        $no_of_exam = json_decode($settings_det['no_of_exam'], 1);
        $subjects   = json_decode($settings_det['subjects'], 1);

        $settingsPOST = $this->input->post('settings');

        $exam_settings_array = [];

        if ( ! empty($settings_det))
        {
            $this->form_validation->set_rules('form_submit', 'Submit Error', "trim|required");

            if ( ! $this->form_validation->run())
            {

                foreach ($no_of_exam as $k => $val)
                {
                    $setting = [];
                    foreach ($subjects as $key => $subject)
                    {

                        $subject_det = $this->Subject_model->get_subject_detail_by(['id' => $subject], 'row_array');

                        $setting [] = [
                            'exam_number'     => $val,
                            'subject_id'      => $subject,
                            'subject_name'    => $subject_det['subject_name'],
                            'subject_fm'      => 0,
                            'subject_pass_mark'      => 0,
                            'subject_addable' => TRUE,
                        ];
                    }
                    $exam_settings_array[$val] = $setting;
                }

                $settings_det_previous = json_decode($settings_det['exam_settings'], 1);

                $dataArray['settings_det_previous'] = $settings_det_previous;

                $dataArray['exam_settings_array'] = $exam_settings_array;

                $dataArray['css_local'] = array('');
                $dataArray['js_local']  = array('');

                $dataArray['form_action'] = 'Exam Settings Step 2';
                $dataArray['page_title']  = 'Exam Settings';
                $dataArray['breadcrumb']  = 'Exam Settings';

                $dataArray['flash_message']        = $this->session->flashdata('flash_message');
                $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

                $this->load->view('subject-and-exam-settings/settings-form', $dataArray);

            }
            else
            {
                $validation_error = FALSE;

                foreach ($no_of_exam as $k => $val)
                {

                    foreach ($subjects as $key => $subject)
                    {

                        if (empty($settingsPOST['fm'][$val][$subject]))
                        {

                            $validation_error = TRUE;

                        }
                        else
                        {

                            if ( ! is_numeric($settingsPOST['fm'][$val][$subject]))
                            {
                                $validation_error = TRUE;
                            }
                        }

                    }
                }

                if ($validation_error == FALSE)
                {

                    $params['exam_settings'] = json_encode($settingsPOST);
                    $params['updated_at']    = date("Y-m-d H:i:s");

                    $new_settings_id = $this->Exam_settings_and_subject_model->save_exam_settings_and_subject($params, $settings_id);

                    $this->session->set_flashdata('flash_message', 'Settings Create successful.');
                    $this->session->set_flashdata('flash_message_status', TRUE);
                    redirect('admin/settings/view-exam-settings/'.$new_settings_id);
                }
                else
                {

                    $this->session->set_flashdata('flash_message', 'Validation Error!!');
                    $this->session->set_flashdata('flash_message_status', FALSE);
                    redirect('admin/settings/add');
                }
            }
        }
        else
        {
            $this->session->set_flashdata('flash_message', 'Invalid operation performed!!');
            $this->session->set_flashdata('flash_message_status', FALSE);

            redirect('admin/settings/add');
        }
    }

    /**
     * @param $settings_id
     */
    public function view_exam_settings($settings_id)
    {
        $settings_det = $this->Exam_settings_and_subject_model->get_exam_settings_and_subject_detail_by(['id' => $settings_id], 'row_array');

        $subjects    = !empty($settings_det['subjects']) ? json_decode($settings_det['subjects'],1) : [];
        $no_of_exams = !empty($settings_det['no_of_exam']) ? json_decode($settings_det['no_of_exam'],1) : [];
        $exam_settings = !empty($settings_det['exam_settings']) ? json_decode($settings_det['exam_settings'],1) : [];

        $exam_config = getCustomConfigItem('exam');

        $exam_settings_array=[];

        foreach ($no_of_exams as $k => $val)
        {
            $setting = [];
            foreach ($subjects as $key => $subject)
            {

                $subject_det = $this->Subject_model->get_subject_detail_by(['id' => $subject], 'row_array');

                $setting [] = [
                    'exam_number'     => $val,
                    'subject_id'      => $subject,
                    'subject_name'    => $subject_det['subject_name'],
                    'subject_fm'      => $exam_settings['fm'][$val][$subject],
                    'pass_mark'      => $exam_settings['pass_mark'][$val][$subject],
                    'subject_addable' => $exam_settings['acceptable'][$val][$subject],
                ];
            }
            $exam_settings_array[$val] = $setting;
        }

        $dataArray['exam_settings_array'] = $exam_settings_array;

        $dataArray['css_local'] = array('');
        $dataArray['js_local']  = array('');

        $dataArray['form_action'] = 'Exam Settings List';
        $dataArray['page_title']  = 'Exam Settings';
        $dataArray['breadcrumb']  = 'Exam Settings';

        $dataArray['flash_message']        = $this->session->flashdata('flash_message');
        $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

        $this->load->view('subject-and-exam-settings/settings-view', $dataArray);
    }
}