<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation'),
        'model'     => array('App_model'),
    );

    /**
     * Profile constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Update & View Profile
     */
    public function index()
    {
        $this->form_validation->set_rules('name', 'Name', "trim|required");
        $this->form_validation->set_rules('email', 'E-mail', "trim|required|valid_email");
        $this->form_validation->set_rules('mobile', 'Mobile', "trim|required|numeric|max_length[10]|min_length[10]");

        $user_auth_session = get_current_logged_in_session();

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($user_auth_session))
            {
                $dataArray = array(
                    'name'   => $user_auth_session['name'],
                    'email'  => $user_auth_session['email'],
                    'mobile' => $user_auth_session['mobile']
                );
            }

            $dataArray['css_local'] = array();
            $dataArray['js_local']  = array();

            $dataArray['page_title']  = 'My Profile';
            $dataArray['breadcrumb']  = 'My Profile';
            $dataArray['form_action'] = 'My Profile';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('my-profile', $dataArray);
        }
        else
        {
            $params = array(
                'name'   => $this->input->post('name'),
                'email'  => $this->input->post('email'),
                'mobile' => $this->input->post('mobile')
            );

            $params['updated_at'] = date("Y-m-d H:i:s");

            $new_session_id = $this->App_model->save_details('admin', $params, $user_auth_session['id']);

            if ( ! empty($new_session_id))
            {
                $this->session->set_flashdata('flash_message', 'Profile updated successfully.');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/profile');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/profile');
            }
        }
    }
}