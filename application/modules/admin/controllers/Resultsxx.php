<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resultsxx extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation',),
        'model'     => array(
            'single_insert/Class_model',
            'single_insert/Section_model',
            'Cron_request',
        ),
    );

    /**
     * Cast constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        $this->excel->roll_number_download();
    }

    public function empty_excel_file_download()
    {
        $this->form_validation->set_rules('class_id', 'Class name', "trim|required");
        $this->form_validation->set_rules('section_id', 'Section name', "trim|required");
        $this->form_validation->set_rules('subject_type', 'Subject Type', "trim|required");
        $this->form_validation->set_rules('subject_combination_name', 'Subject Combination Name', "trim|required");

        if ($this->form_validation->run())
        {

            $body = [
                'class_id' => $this->input->post('class_id'),
                'section_id' => $this->input->post('section_id'),
                'subject_type' => $this->input->post('subject_type'),
                'subject_combination_id' => $this->input->post('subject_combination_name'),
            ];

            $this->Cron_request->save([
                'request_type' => 'result-request',
                'body' => json_encode($body),
            ]);

            $this->session->set_flashdata('flash_message', 'Request accepted.');
            $this->session->set_flashdata('flash_message_status', TRUE);

            redirect('admin/results/empty-excel-file-download');
        }
        else
        {
            $dataArray['allClass'] = $this->Class_model->get_class_detail_by([], 'result_array');

            $dataArray['css_local'] = array('');
            $dataArray['js_local'] = array('request-result');

            $dataArray['page_title'] = 'Excel File Download';

            $dataArray['form_action'] = 'Request Empty result';
            $dataArray['page_title'] = 'Request Empty result';
            $dataArray['breadcrumb'] = 'Request Empty result';
            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message'] = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('result/empty-excel-file-download', $dataArray);
        }

    }

}