<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sub_caste extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('single_insert/Caste_model', 'single_insert/Sub_caste_model'),
    );

    /**
     * Session constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $sub_caste_id
     */
    public function add($sub_caste_id = NULL)
    {
        $this->form_validation->set_rules('sub_caste_name', 'Sub Caste Name', "trim|required");
        $this->form_validation->set_rules('caste_id', 'Caste Name', "trim|required");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($sub_caste_id))
            {
                $sub_caste_details = $this->Sub_caste_model->get_sub_caste_detail_by(['id' => $sub_caste_id]);

                if ( ! empty($sub_caste_details))
                {
                    $dataArray = array(
                        'sub_caste_name' => $sub_caste_details['sub_caste_name'],
                        'caste_id'       => $sub_caste_details['caste_id'],
                    );

                    $dataArray['form_action'] = 'Edit Sub Caste';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/sub-caste/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Sub Caste';
            }

            $allCaste              = $this->Caste_model->get_caste_detail_by([], 'result_array');
            $dataArray['allCaste'] = $allCaste;


            $dataArray['css_local'] = array('bootstrap-datepicker');
            $dataArray['js_local']  = array('bootstrap-datepicker');

            $dataArray['page_title'] = 'Sub Caste';
            $dataArray['breadcrumb'] = 'Initial Information / Sub Caste';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('single-insert/sub-caste/add-form', $dataArray);
        }
        else
        {
            $params = array(
                'caste_id'       => $this->input->post('caste_id'),
                'sub_caste_name' => $this->input->post('sub_caste_name'),
            );

            if ( ! empty($sub_caste_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_sub_caste_id = $this->Sub_caste_model->save_sub_caste($params, $sub_caste_id);
            }
            else
            {
                $params['created_at'] = date("Y-m-d H:i:s");
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_sub_caste_id = $this->Sub_caste_model->save_sub_caste($params);
            }

            if ( ! empty($new_sub_caste_id))
            {
                $this->session->set_flashdata('flash_message', (empty($sub_caste_id)) ? 'Sub Caste created successfully' : 'Sub Caste updated successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/sub-caste/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/sub-caste/list');
            }
        }
    }

    /**
     *
     */
    public function index()
    {
        $listing_headers = 'sub_caste_listing_headers';

        $data['source']          = site_url('admin/sub_caste/listSubCaste_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Sub Caste';
        $data['page_title']      = 'Sub Caste';
        $data['breadcrumb']      = 'Initial Information / Sub Caste';

        $dataArray = $this->_table_listing($data);

        $this->load->view('single-insert/sub-caste/index', $dataArray);
    }

    /**
     *
     */
    public function listSubCaste_Json()
    {
        $listing_headers = 'sub_caste_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Sub_caste_model->tbl_name = 'sub_caste';

        $this->Sub_caste_model->join_tbl_name = 'caste';

        $this->Sub_caste_model->join_cond = "{$this->Sub_caste_model->tbl_name}.caste_id = {$this->Sub_caste_model->join_tbl_name}.id";

        $this->Sub_caste_model->select_db_cols = "sub_caste.id, sub_caste.sub_caste_name, sub_caste.created_at, sub_caste.updated_at, caste.id as c_id, caste.caste_name";

        $this->Sub_caste_model->list_search_key = 'sub_caste.sub_caste_name';

        $resultdata = $this->Sub_caste_model->get_all_sub_caste_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Session
     *
     * @param $sub_caste_id
     */
    public function delete($sub_caste_id)
    {
        $res = $this->Sub_caste_model->delete_sub_caste(['id' => $sub_caste_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/sub-caste/list');
    }
}