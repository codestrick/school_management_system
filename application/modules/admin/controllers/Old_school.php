<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Old_school extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('single_insert/Old_school_model'),
    );

    /**
     * Old_school constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $old_school_id
     */
    public function add($old_school_id = NULL)
    {
        $this->form_validation->set_rules('old_school_name', 'Old School Name', "trim|required|unique[old_school.old_school_name.id.{$old_school_id}]");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($old_school_id))
            {
                $old_school_details = $this->Old_school_model->get_old_school_detail_by(['id' => $old_school_id]);

                if ( ! empty($old_school_details))
                {
                    $dataArray = array(
                        'old_school_name' => $old_school_details['old_school_name'],
                    );

                    $dataArray['form_action'] = 'Edit Old School';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/old-school/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Old School';
            }

            $dataArray['css_local'] = array('bootstrap-datepicker');
            $dataArray['js_local']  = array('bootstrap-datepicker');

            $dataArray['page_title'] = 'Old School';
            $dataArray['breadcrumb'] = 'Initial Information / Old School';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('single-insert/old-school/add-form', $dataArray);
        }
        else
        {
            $params = array(
                'old_school_name' => $this->input->post('old_school_name'),
            );

            if ( ! empty($old_school_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_old_school_id = $this->Old_school_model->save_old_school($params, $old_school_id);
            }
            else
            {
                $params['created_at'] = date("Y-m-d H:i:s");
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_old_school_id = $this->Old_school_model->save_old_school($params);
            }

            if ( ! empty($new_old_school_id))
            {
                $this->session->set_flashdata('flash_message', (empty($old_school_id)) ? 'Old School created successfully' : 'Old School updated successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/old-school/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/old-school/list');
            }
        }
    }

    /**
     *
     */
    public function index()
    {
        $listing_headers = 'old_school_listing_headers';

        $data['source']          = site_url('admin/old_school/listOldSchool_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Old School';
        $data['page_title']      = 'Old School';
        $data['breadcrumb']      = 'Initial Information / Old School';

        $dataArray = $this->_table_listing($data);

        $this->load->view('single-insert/old-school/index', $dataArray);
    }

    /**
     *
     */
    public function listOldSchool_Json()
    {
        $listing_headers = 'old_school_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Old_school_model->tbl_name = 'old_school';

        $this->Old_school_model->select_db_cols = "id, old_school_name, address, created_at, updated_at";

        $this->Old_school_model->list_search_key = 'old_school_name';

        $resultdata = $this->Old_school_model->get_all_old_school_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Session
     *
     * @param $session_id
     */
    public function delete($session_id)
    {
        $res = $this->Old_school_model->delete_session(['id' => $session_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/old-school/list');
    }
}