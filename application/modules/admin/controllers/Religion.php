<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Religion extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('single_insert/Religion_model'),
    );

    /**
     * Religion constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $religion_id
     */
    public function add($religion_id = NULL)
    {
        $this->form_validation->set_rules('religion_name', 'Religion Name', "trim|required|unique[religion.religion_name.id.{$religion_id}]");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($religion_id))
            {
                $religion_details = $this->Religion_model->get_religion_detail_by(['id' => $religion_id]);

                if ( ! empty($religion_details))
                {
                    $dataArray = array(
                        'religion_name' => $religion_details['religion_name'],
                    );

                    $dataArray['form_action'] = 'Edit Religion';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/religion/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Religion';
            }

            $dataArray['css_local'] = array('bootstrap-datepicker');
            $dataArray['js_local']  = array('bootstrap-datepicker');

            $dataArray['page_title'] = 'Religion';
            $dataArray['breadcrumb'] = 'Initial Information / Religion';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('single-insert/religion/add-form', $dataArray);
        }
        else
        {
            $params = array(
                'religion_name' => $this->input->post('religion_name'),
            );

            if ( ! empty($religion_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_religion_id = $this->Religion_model->save_religion($params, $religion_id);
            }
            else
            {
                $params['created_at'] = date("Y-m-d H:i:s");
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_religion_id = $this->Religion_model->save_religion($params);
            }

            if ( ! empty($new_religion_id))
            {
                $this->session->set_flashdata('flash_message', (empty($religion_id)) ? 'Session created successfully' : 'Session updated successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/religion/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/religion/list');
            }
        }
    }

    /**
     *
     */
    public function index()
    {
        $listing_headers = 'religion_listing_headers';

        $data['source']          = site_url('admin/religion/listReligion_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Religion';
        $data['page_title']      = 'Religion';
        $data['breadcrumb']      = 'Initial Information / Religion';

        $dataArray = $this->_table_listing($data);

        $this->load->view('single-insert/religion/index', $dataArray);
    }

    /**
     *
     */
    public function listReligion_Json()
    {
        $listing_headers = 'religion_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Religion_model->tbl_name = 'religion';

        $this->Religion_model->select_db_cols = "id, religion_name, created_at, updated_at";

        $this->Religion_model->list_search_key = 'religion_name';


        $resultdata = $this->Religion_model->get_all_religion_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Session
     *
     * @param $session_id
     */
    public function delete($session_id)
    {
        $res = $this->Religion_model->delete_session(['id' => $session_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/religion/list');
    }
}