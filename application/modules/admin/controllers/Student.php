<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array(
            'App_model',
            'Student_model',
            'single_insert/Address_model',
            'single_insert/Caste_model',
            'single_insert/Class_model',
            'single_insert/Occupation_model',
            'single_insert/Religion_model',
        ),
    );

    /**
     * Student constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add Student View
     */
    public function add()
    {
        $dataArray['allCaste']       = $this->Caste_model->get_caste_detail_by([], 'result_array');
        $dataArray['allReligions']   = $this->Religion_model->get_religion_detail_by([], 'result_array');
        $dataArray['allOccupations'] = $this->Occupation_model->get_occupation_detail_by([], 'result_array');
        $class                       = $this->Class_model->get_class_detail_by([], 'result_array');

        if ( ! empty($class))
        {
            foreach ($class as $key => $value)
            {
                $dataArray['allClass'][$value['class_type']][] = $value;
            }
        }
        else
        {
            $dataArray['allClass'] = [];
        }

        $dataArray['css_local'] = array('bootstrap-datepicker');
        $dataArray['js_local']  = array(
            'bootstrap-datepicker',
            'steps',
            'validate',
            'student-registration'
        );

        $dataArray['page_title']  = 'Student';
        $dataArray['breadcrumb']  = 'Student';
        $dataArray['form_action'] = 'Student Registration';

        $dataArray['flash_message']        = $this->session->flashdata('flash_message');
        $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

        $this->load->view('student/add-form', $dataArray);
    }

    /**
     * Save Student Details AJAX
     */
    public function save_student_ajax()
    {
        $student_id   = $this->input->post('student_id');
        $guardians_id = $this->input->post('guardians_id');

        $guardians_params = [
            'guardians_occupation_id' => $this->input->post('guardians_occupation_id'),
            'fathers_occupation_id'   => $this->input->post('fathers_occupation_id'),
            'mothers_occupation_id'   => $this->input->post('mothers_occupation_id'),
            'fathers_name'            => $this->input->post('fathers_name'),
            'mothers_name'            => $this->input->post('mothers_name'),
            'guardians_name'          => $this->input->post('guardians_name'),
            'guardians_relation'      => $this->input->post('guardians_relation'),
        ];

        if ( ! empty($guardians_id))
        {
            $params['updated_at'] = date("Y-m-d H:i:s");

            $guardians_id = $this->App_model->save_details('guardians', $guardians_params, $guardians_id);
        }
        else
        {
            $params['created_at'] = date("Y-m-d H:i:s");
            $params['updated_at'] = date("Y-m-d H:i:s");

            $guardians_id = $this->App_model->save_details('guardians', $guardians_params);
        }

        $state    = ucfirst($this->input->post('permanent_state'));
        $district = ucfirst($this->input->post('permanent_district'));
        $location = ucfirst($this->input->post('permanent_location'));
        $pincode  = $this->input->post('permanent_pincode');

        $permanent_address = $state . '|' . $district . '|' . $location . '|' . $pincode;

        $state    = ucfirst($this->input->post('correspondence_state'));
        $district = ucfirst($this->input->post('correspondence_district'));
        $location = ucfirst($this->input->post('correspondence_location'));
        $pincode  = $this->input->post('correspondence_pincode');

        $correspondence_address = $state . '|' . $district . '|' . $location . '|' . $pincode;

        $dob = $this->input->post('dob');
        $dob = str_replace('/', '-', $dob);

        $student_params = [
            'std_name'               => $this->input->post('std_name'),
            'dob'                    => date("Y-m-d", strtotime($dob)),
            'age'                    => $this->input->post('age'),
            'phone_no'               => $this->input->post('phone_no'),
            'guardians_id'           => $guardians_id,
            'caste_id'               => $this->input->post('caste_id'),
            'sub_caste_id'           => $this->input->post('sub_caste_id'),
            'gender'                 => $this->input->post('gender'),
            'religion_id'            => $this->input->post('religion_id'),
            'permanent_address'      => $permanent_address,
            'correspondence_address' => $correspondence_address,
            'same_address'           => $this->input->post('same_address'),
        ];

        if ( ! empty($student_id))
        {
            $student_params['updated_at'] = date("Y-m-d H:i:s");

            $new_student_id = $this->Student_model->save_student($student_params, $student_id);
        }
        else
        {
            $student_params['created_at'] = date("Y-m-d H:i:s");
            $student_params['updated_at'] = date("Y-m-d H:i:s");

            $new_student_id = $this->Student_model->save_student($student_params);
        }

        if ( ! empty($new_student_id))
        {
            $auth_session = get_current_logged_in_session();

            if ( ! empty($auth_session['session']))
            {
                $extra_info_params = [
                    'student_id'             => $new_student_id,
                    'session_id'             => $auth_session['session'],
                    'admission_class_id'     => $this->input->post('admission_class_id'),
                    'section_id'             => $this->input->post('section_id'),
                    'subject_type'           => $this->input->post('subject_type'),
                    'subject_combination_id' => $this->input->post('subject_combination_id'),
                    'operation_type'         => 'new_admission',
                    'created_at'             => date("Y-m-d H:i:s"),
                    'updated_at'             => date("Y-m-d H:i:s"),
                ];

                $information_id = $this->App_model->save_details('student_extra_informations', $extra_info_params);

                $student_registration_id = 'STD-00' . $new_student_id . '-' . (! empty($auth_session) ? $auth_session['session'] : '');

                $updated_student_id = $this->Student_model->save_student(['reg_id' => $student_registration_id, 'present_information_id' => $information_id], $new_student_id);
            }

            $flash_message = (empty($student_id)) ? 'Student created successfully. Do you want to display the student list?' : 'Student updated successfully. Do you want to display the student list?';

            echo json_encode(['status' => 'true', 'message' => $flash_message]);
        }
        else
        {
            echo json_encode(['status' => 'false', 'message' => 'Invalid Operation Performed!']);
        }
    }

    /**
     * View Student Registration History
     *
     * @param $student_id
     */
    public function view($student_id)
    {
        $student_info = $this->Student_model->get_student_details($student_id);

        if ( ! empty($student_info))
        {
            $dataArray = $student_info;

            $dataArray['student_extra_info'] = $this->Student_model->get_student_admission_details($student_id);

            if ( ! empty($student_info['gender']))
            {
                if ($student_info['gender'] == 'm')
                {
                    $dataArray['gender'] = 'Male';
                }
                else if ($student_info['gender'] == 'f')
                {
                    $dataArray['gender'] = 'Female';
                }
                else if ($student_info['gender'] == 'o')
                {
                    $dataArray['gender'] = 'Others';
                }
            }

            $dataArray['dob'] = date('d/m/Y', strtotime($student_info['dob']));

            $occupation_info = $this->Student_model->get_all_occupations([], 'result_array');

            if ( ! empty($occupation_info))
            {
                $dataArray['guardians_occupation'] = isset($occupation_info[$student_info['guardians_occupation_id']]) ? $occupation_info[$student_info['guardians_occupation_id']] : '';
                $dataArray['fathers_occupation']   = isset($occupation_info[$student_info['fathers_occupation_id']]) ? $occupation_info[$student_info['fathers_occupation_id']] : '';
                $dataArray['mothers_occupation']   = isset($occupation_info[$student_info['mothers_occupation_id']]) ? $occupation_info[$student_info['mothers_occupation_id']] : '';
            }
        }
        else
        {
            $this->session->set_flashdata('flash_message', 'Invalid operation performed!');
            $this->session->set_flashdata('flash_message_status', FALSE);

            redirect(base_url('admin/student/list'));
        }

        $dataArray['css_local'] = array();
        $dataArray['js_local']  = array();

        $dataArray['page_title']  = 'Student';
        $dataArray['breadcrumb']  = 'Student';
        $dataArray['form_action'] = 'Student Details';

        $dataArray['flash_message']        = $this->session->flashdata('flash_message');
        $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

        $this->load->view('student/view-details', $dataArray);
    }

    /**
     * Update student details.
     *
     * @param null $student_id
     */
    public function student_edit($student_id = NULL)
    {
        if ( ! empty($student_id))
        {
            $student_info = $this->App_model->get_details_by('std_registration', ['id' => $student_id]);

            if ( ! empty($student_info))
            {
                $dataArray['student_name'] = $student_info['std_name'];
                $dataArray['dob']          = date('d/m/Y', strtotime($student_info['dob']));
                $dataArray['age']          = $student_info['age'];
                $dataArray['gender']       = $student_info['gender'];
                $dataArray['phone_no']     = $student_info['phone_no'];
                $dataArray['caste_id']     = $student_info['caste_id'];
                $dataArray['sub_caste_id'] = $student_info['sub_caste_id'];
                $dataArray['religion_id']  = $student_info['religion_id'];
                $dataArray['guardians_id'] = $student_info['guardians_id'];

                $permanent_address      = explode('|', $student_info['permanent_address']);
                $correspondence_address = explode('|', $student_info['correspondence_address']);

                $dataArray['permanent_state']    = isset($permanent_address[0]) ? $permanent_address[0] : '';
                $dataArray['permanent_district'] = isset($permanent_address[1]) ? $permanent_address[1] : '';
                $dataArray['permanent_location'] = isset($permanent_address[2]) ? $permanent_address[2] : '';
                $dataArray['permanent_pincode']  = isset($permanent_address[3]) ? $permanent_address[3] : '';

                $dataArray['correspondence_state']    = isset($correspondence_address[0]) ? $correspondence_address[0] : '';
                $dataArray['correspondence_district'] = isset($correspondence_address[1]) ? $correspondence_address[1] : '';
                $dataArray['correspondence_location'] = isset($correspondence_address[2]) ? $correspondence_address[2] : '';
                $dataArray['correspondence_pincode']  = isset($correspondence_address[3]) ? $correspondence_address[3] : '';

                $dataArray['same_address'] = ($student_info['same_address'] == 'true' ? 'checked' : '');

                $guardians_info = $this->App_model->get_details_by('guardians', ['id' => $student_info['guardians_id']]);

                if ( ! empty($guardians_info))
                {
                    $dataArray['fathers_name']            = $guardians_info['fathers_name'];
                    $dataArray['mothers_name']            = $guardians_info['mothers_name'];
                    $dataArray['guardians_name']          = $guardians_info['guardians_name'];
                    $dataArray['guardians_relation']      = $guardians_info['guardians_relation'];
                    $dataArray['guardians_occupation_id'] = $guardians_info['guardians_occupation_id'];
                    $dataArray['fathers_occupation_id']   = $guardians_info['fathers_occupation_id'];
                    $dataArray['mothers_occupation_id']   = $guardians_info['mothers_occupation_id'];
                }
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid operation performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect(base_url('admin/student/list'));
            }

            if ( ! empty($student_info))
            {
                $student_extra_information = $this->App_model->get_details_by('student_extra_informations', ['id' => $student_info['present_information_id'], 'student_id' => $student_id]);

                if ( ! empty($student_extra_information))
                {
                    $dataArray['section_id']             = $student_extra_information['section_id'];
                    $dataArray['admission_class_id']     = $student_extra_information['admission_class_id'];
                    $dataArray['subject_type']           = $student_extra_information['subject_type'];
                    $dataArray['subject_combination_id'] = $student_extra_information['subject_combination_id'];
                }
            }

            $dataArray['page_title']  = 'Student';
            $dataArray['breadcrumb']  = 'Student';
            $dataArray['form_action'] = 'Edit Student';

            $dataArray['allCaste']       = $this->Caste_model->get_caste_detail_by([], 'result_array');
            $dataArray['allReligions']   = $this->Religion_model->get_religion_detail_by([], 'result_array');
            $dataArray['allOccupations'] = $this->Occupation_model->get_occupation_detail_by([], 'result_array');
            $class                       = $this->Class_model->get_class_detail_by([], 'result_array');

            if ( ! empty($class))
            {
                foreach ($class as $key => $value)
                {
                    $dataArray['allClass'][$value['class_type']][] = $value;
                }
            }
            else
            {
                $dataArray['allClass'] = [];
            }

            $dataArray['student_id'] = $student_id;

            $dataArray['css_local'] = array('bootstrap-datepicker');
            $dataArray['js_local']  = array(
                'bootstrap-datepicker',
                'steps',
                'validate',
                'edit-student'
            );

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('student/edit-form', $dataArray);
        }
        else
        {
            $this->session->set_flashdata('flash_message', 'Invalid operation performed!');
            $this->session->set_flashdata('flash_message_status', FALSE);

            redirect(base_url('admin/student/list'));
        }
    }

    /**
     * Update Student Data AJAX
     */
    public function update_student()
    {
        $student_id = $this->input->post('student_id');

        $student_info = $this->App_model->get_details_by('std_registration', ['id' => $student_id]);

        if ( ! empty($student_info))
        {
            $guardians_id = $this->input->post('guardians_id');

            $guardians_params = [
                'guardians_occupation_id' => $this->input->post('guardians_occupation_id'),
                'fathers_occupation_id'   => $this->input->post('fathers_occupation_id'),
                'mothers_occupation_id'   => $this->input->post('mothers_occupation_id'),
                'fathers_name'            => $this->input->post('fathers_name'),
                'mothers_name'            => $this->input->post('mothers_name'),
                'guardians_name'          => $this->input->post('guardians_name'),
                'guardians_relation'      => $this->input->post('guardians_relation'),
            ];

            if ( ! empty($guardians_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $guardians_id = $this->App_model->save_details('guardians', $guardians_params, $guardians_id);
            }

            $state    = ucfirst($this->input->post('permanent_state'));
            $district = ucfirst($this->input->post('permanent_district'));
            $location = ucfirst($this->input->post('permanent_location'));
            $pincode  = $this->input->post('permanent_pincode');

            $permanent_address = $state . '|' . $district . '|' . $location . '|' . $pincode;

            $state    = ucfirst($this->input->post('correspondence_state'));
            $district = ucfirst($this->input->post('correspondence_district'));
            $location = ucfirst($this->input->post('correspondence_location'));
            $pincode  = $this->input->post('correspondence_pincode');

            $correspondence_address = $state . '|' . $district . '|' . $location . '|' . $pincode;

            $dob = $this->input->post('dob');
            $dob = str_replace('/', '-', $dob);

            $student_params = [
                'std_name'               => $this->input->post('std_name'),
                'dob'                    => date("Y-m-d", strtotime($dob)),
                'age'                    => $this->input->post('age'),
                'phone_no'               => $this->input->post('phone_no'),
                'guardians_id'           => $guardians_id,
                'caste_id'               => $this->input->post('caste_id'),
                'sub_caste_id'           => $this->input->post('sub_caste_id'),
                'gender'                 => $this->input->post('gender'),
                'religion_id'            => $this->input->post('religion_id'),
                'permanent_address'      => $permanent_address,
                'correspondence_address' => $correspondence_address,
                'same_address'           => $this->input->post('same_address'),
                'updated_at'             => date("Y-m-d H:i:s"),
            ];

            $new_student_id = $this->Student_model->save_student($student_params, $student_id);

            if ( ! empty($new_student_id))
            {
                $extra_info_params = [
                    'admission_class_id'     => $this->input->post('admission_class_id'),
                    'section_id'             => $this->input->post('section_id'),
                    'subject_type'           => $this->input->post('subject_type'),
                    'subject_combination_id' => $this->input->post('subject_combination_id'),
                    'updated_at'             => date("Y-m-d H:i:s"),
                ];

                $information_id = $this->App_model->save_details_by_conditions('student_extra_informations', $extra_info_params, ['id' => $student_info['present_information_id'], 'student_id' => $student_id]);

                echo json_encode(['status' => 'true', 'message' => 'Student details updated successfully.']);
            }
            else
            {
                echo json_encode(['status' => 'false', 'errot_type' => 'DB error!', 'message' => 'Due to some error this operation is cancelled. Please try again.']);
            }
        }
        else
        {
            echo json_encode(['status' => 'false', 'errot_type' => 'Student not found.', 'message' => 'Due to some error this operation is cancelled. Please try again.']);
        }
    }

    /**
     * Search Student View For Student Readmission OR Upgradation
     */
    public function search_student()
    {
        $this->form_validation->set_rules('std_reg_no', 'Student Registration Number Required.', "trim|required");

        if ($this->form_validation->run())
        {
            $student_id = $this->input->post('std_reg_no');

            $student_info = $this->App_model->get_details_by('std_registration', ['reg_id' => $student_id]);

            if (empty($student_info))
            {
                $this->session->set_flashdata('flash_message', 'Invalid student registration ID provided!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect(base_url('admin/student/' . $this->uri->segment(3)));
            }

            if ($this->uri->segment(3) == 'upgradation')
            {
                redirect(base_url('admin/student/upgrade/' . $student_info['id']));
            }
            else if ($this->uri->segment(3) == 'readmission')
            {
                redirect(base_url('admin/student/re-admission/' . $student_info['id']));
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid operation performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect(base_url('admin/student/' . $this->uri->segment(3)));
            }

            p($student_info);
        }
        else
        {
            $dataArray['page_title']  = 'Student';
            $dataArray['breadcrumb']  = 'Student';
            $dataArray['form_action'] = 'Search Student';
            $dataArray['form_url']    = base_url('admin/student/' . $this->uri->segment(3));

            $dataArray['css_local'] = array();
            $dataArray['js_local']  = array();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $dataArray['validation_err'] = validation_errors();

            $this->load->view('student/search-student', $dataArray);
        }
    }

    /**
     * Upgrade Student View
     *
     * @param $student_id
     */
    public function student_upgradation($student_id)
    {
        if ( ! empty($student_id))
        {
            $student_info = $this->App_model->get_details_by('std_registration', ['id' => $student_id]);

            if ( ! empty($student_info))
            {
                $dataArray['student_name'] = $student_info['std_name'];
                $dataArray['dob']          = date('d/m/Y', strtotime($student_info['dob']));
                $dataArray['age']          = $student_info['age'];
                $dataArray['gender']       = $student_info['gender'];
                $dataArray['phone_no']     = $student_info['phone_no'];
                $dataArray['caste_id']     = $student_info['caste_id'];
                $dataArray['sub_caste_id'] = $student_info['sub_caste_id'];
                $dataArray['religion_id']  = $student_info['religion_id'];
                $dataArray['guardians_id'] = $student_info['guardians_id'];

                $permanent_address      = explode('|', $student_info['permanent_address']);
                $correspondence_address = explode('|', $student_info['correspondence_address']);

                $dataArray['permanent_state']    = isset($permanent_address[0]) ? $permanent_address[0] : '';
                $dataArray['permanent_district'] = isset($permanent_address[1]) ? $permanent_address[1] : '';
                $dataArray['permanent_location'] = isset($permanent_address[2]) ? $permanent_address[2] : '';
                $dataArray['permanent_pincode']  = isset($permanent_address[3]) ? $permanent_address[3] : '';

                $dataArray['correspondence_state']    = isset($correspondence_address[0]) ? $correspondence_address[0] : '';
                $dataArray['correspondence_district'] = isset($correspondence_address[1]) ? $correspondence_address[1] : '';
                $dataArray['correspondence_location'] = isset($correspondence_address[2]) ? $correspondence_address[2] : '';
                $dataArray['correspondence_pincode']  = isset($correspondence_address[3]) ? $correspondence_address[3] : '';

                $dataArray['same_address'] = ($student_info['same_address'] == 'true' ? 'checked' : '');

                $guardians_info = $this->App_model->get_details_by('guardians', ['id' => $student_info['guardians_id']]);

                if ( ! empty($guardians_info))
                {
                    $dataArray['fathers_name']            = $guardians_info['fathers_name'];
                    $dataArray['mothers_name']            = $guardians_info['mothers_name'];
                    $dataArray['guardians_name']          = $guardians_info['guardians_name'];
                    $dataArray['guardians_relation']      = $guardians_info['guardians_relation'];
                    $dataArray['guardians_occupation_id'] = $guardians_info['guardians_occupation_id'];
                    $dataArray['fathers_occupation_id']   = $guardians_info['fathers_occupation_id'];
                    $dataArray['mothers_occupation_id']   = $guardians_info['mothers_occupation_id'];
                }
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid operation performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect(base_url('admin/student/list'));
            }

            $dataArray['page_title']  = 'Student';
            $dataArray['breadcrumb']  = 'Student';
            $dataArray['form_action'] = 'Upgrade Student';

            $dataArray['allCaste']       = $this->Caste_model->get_caste_detail_by([], 'result_array');
            $dataArray['allReligions']   = $this->Religion_model->get_religion_detail_by([], 'result_array');
            $dataArray['allOccupations'] = $this->Occupation_model->get_occupation_detail_by([], 'result_array');
            $class                       = $this->Class_model->get_class_detail_by([], 'result_array');

            if ( ! empty($class))
            {
                foreach ($class as $key => $value)
                {
                    $dataArray['allClass'][$value['class_type']][] = $value;
                }
            }
            else
            {
                $dataArray['allClass'] = [];
            }

            $dataArray['student_id'] = $student_id;

            $dataArray['css_local'] = array('bootstrap-datepicker');
            $dataArray['js_local']  = array(
                'bootstrap-datepicker',
                'steps',
                'validate',
                'student-upgrade',
            );

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('student/edit-form', $dataArray);
        }
        else
        {
            $this->session->set_flashdata('flash_message', 'Invalid operation performed!');
            $this->session->set_flashdata('flash_message_status', FALSE);

            redirect(base_url('admin/student/list'));
        }
    }

    /**
     * Save Student Upgradation Data AJAX
     */
    public function save_student_upgradation()
    {
        $student_id = $this->input->post('student_id');

        $student_info = $this->App_model->get_details_by('std_registration', ['id' => $student_id]);

        if ( ! empty($student_info))
        {
            $session = get_current_logged_in_session();

            $extra_info_params = [
                'session_id'             => $session['session'],
                'student_id'             => $student_id,
                'admission_class_id'     => $this->input->post('admission_class_id'),
                'section_id'             => $this->input->post('section_id'),
                'subject_type'           => $this->input->post('subject_type'),
                'subject_combination_id' => $this->input->post('subject_combination_id'),
                'updated_at'             => date("Y-m-d H:i:s"),
            ];

            $information_id = $this->App_model->save_details_by_conditions('student_extra_informations', $extra_info_params);

            echo json_encode(['status' => 'true', 'message' => 'Student upgraded successfully.']);
        }
        else
        {
            echo json_encode(['status' => 'false', 'error_type' => 'Student not found.', 'message' => 'Due to some error this operation is cancelled. Please try again.']);
        }
    }

    /**
     * Readmission Student View
     * @param $student_id
     */
    public function student_readmission($student_id)
    {
        if ( ! empty($student_id))
        {
            $student_info = $this->App_model->get_details_by('std_registration', ['id' => $student_id]);

            if ( ! empty($student_info))
            {
                $dataArray['student_name'] = $student_info['std_name'];
                $dataArray['dob']          = date('d/m/Y', strtotime($student_info['dob']));
                $dataArray['age']          = $student_info['age'];
                $dataArray['gender']       = $student_info['gender'];
                $dataArray['phone_no']     = $student_info['phone_no'];
                $dataArray['caste_id']     = $student_info['caste_id'];
                $dataArray['sub_caste_id'] = $student_info['sub_caste_id'];
                $dataArray['religion_id']  = $student_info['religion_id'];
                $dataArray['guardians_id'] = $student_info['guardians_id'];

                $permanent_address      = explode('|', $student_info['permanent_address']);
                $correspondence_address = explode('|', $student_info['correspondence_address']);

                $dataArray['permanent_state']    = isset($permanent_address[0]) ? $permanent_address[0] : '';
                $dataArray['permanent_district'] = isset($permanent_address[1]) ? $permanent_address[1] : '';
                $dataArray['permanent_location'] = isset($permanent_address[2]) ? $permanent_address[2] : '';
                $dataArray['permanent_pincode']  = isset($permanent_address[3]) ? $permanent_address[3] : '';

                $dataArray['correspondence_state']    = isset($correspondence_address[0]) ? $correspondence_address[0] : '';
                $dataArray['correspondence_district'] = isset($correspondence_address[1]) ? $correspondence_address[1] : '';
                $dataArray['correspondence_location'] = isset($correspondence_address[2]) ? $correspondence_address[2] : '';
                $dataArray['correspondence_pincode']  = isset($correspondence_address[3]) ? $correspondence_address[3] : '';

                $dataArray['same_address'] = ($student_info['same_address'] == 'true' ? 'checked' : '');

                $guardians_info = $this->App_model->get_details_by('guardians', ['id' => $student_info['guardians_id']]);

                if ( ! empty($guardians_info))
                {
                    $dataArray['fathers_name']            = $guardians_info['fathers_name'];
                    $dataArray['mothers_name']            = $guardians_info['mothers_name'];
                    $dataArray['guardians_name']          = $guardians_info['guardians_name'];
                    $dataArray['guardians_relation']      = $guardians_info['guardians_relation'];
                    $dataArray['guardians_occupation_id'] = $guardians_info['guardians_occupation_id'];
                    $dataArray['fathers_occupation_id']   = $guardians_info['fathers_occupation_id'];
                    $dataArray['mothers_occupation_id']   = $guardians_info['mothers_occupation_id'];
                }
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid operation performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect(base_url('admin/student/list'));
            }

            $dataArray['page_title']  = 'Student';
            $dataArray['breadcrumb']  = 'Student';
            $dataArray['form_action'] = 'Readmission Student';

            $dataArray['allCaste']       = $this->Caste_model->get_caste_detail_by([], 'result_array');
            $dataArray['allReligions']   = $this->Religion_model->get_religion_detail_by([], 'result_array');
            $dataArray['allOccupations'] = $this->Occupation_model->get_occupation_detail_by([], 'result_array');
            $class                       = $this->Class_model->get_class_detail_by([], 'result_array');

            if ( ! empty($class))
            {
                foreach ($class as $key => $value)
                {
                    $dataArray['allClass'][$value['class_type']][] = $value;
                }
            }
            else
            {
                $dataArray['allClass'] = [];
            }

            $dataArray['student_id'] = $student_id;

            $dataArray['css_local'] = array('bootstrap-datepicker');
            $dataArray['js_local']  = array(
                'bootstrap-datepicker',
                'steps',
                'validate',
                'student-readmission',
            );

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('student/edit-form', $dataArray);
        }
        else
        {
            $this->session->set_flashdata('flash_message', 'Invalid operation performed!');
            $this->session->set_flashdata('flash_message_status', FALSE);

            redirect(base_url('admin/student/list'));
        }
    }

    /**
     * Save Student Readmission Data AJAX
     */
    public function save_student_readmission()
    {
        $student_id = $this->input->post('student_id');

        $student_info = $this->App_model->get_details_by('std_registration', ['id' => $student_id]);

        if ( ! empty($student_info))
        {
            $session = get_current_logged_in_session();

            $extra_info_params = [
                'session_id'             => $session['session'],
                'student_id'             => $student_id,
                'admission_class_id'     => $this->input->post('admission_class_id'),
                'section_id'             => $this->input->post('section_id'),
                'subject_type'           => $this->input->post('subject_type'),
                'subject_combination_id' => $this->input->post('subject_combination_id'),
                'updated_at'             => date("Y-m-d H:i:s"),
            ];

            $information_id = $this->App_model->save_details_by_conditions('student_extra_informations', $extra_info_params);

            echo json_encode(['status' => 'true', 'message' => 'Student readmission successfully.']);
        }
        else
        {
            echo json_encode(['status' => 'false', 'error_type' => 'Student not found.', 'message' => 'Due to some error this operation is cancelled. Please try again.']);
        }
    }

    /**
     * Student list view
     */
    public function index()
    {
        $dataArray['form_action'] = 'List Student';
        $dataArray['page_title']  = 'Student';
        $dataArray['breadcrumb']  = 'Student';

        $dataArray['js_local'] = array(
            'jquery.dataTables',
            'dataTables.bootstrap4',
            'dataTables.buttons',
            'jszip',
            'pdfmake',
            'vfs_fonts',
            'buttons.html5',
            'buttons.print',
            'dataTables.select',

            'moment',
            'student-list'
        );

        $dataArray['css_local'] = array(
            'dataTables.bootstrap4',
            'select.bootstrap4',
            'buttons.dataTables'
        );

        $dataArray['student_edit_form_modal'] = $this->load->viewPartial('student/edit-form-modal');

        $dataArray['flash_message']        = $this->session->flashdata('flash_message');
        $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

        $this->load->view('student/index', $dataArray);
    }

    /**
     * Student List DataTable AJAX
     */
    public function get_student_list_ajax()
    {
        $ajax_request = $this->input->post();

        $sortable_column = isset($ajax_request['order'][0]['column']) ? $ajax_request['order'][0]['column'] : '';
        $sortType        = isset($ajax_request['order'][0]['dir']) ? $ajax_request['order'][0]['dir'] : '';

        $limit  = $ajax_request['length'];
        $offset = $ajax_request['start'];
        $draw   = $ajax_request['draw'];

        $search_value = $ajax_request['search']['value'];

        $total_records = $this->Student_model->get_student_details_list_by_condition(FALSE, 0, 0, $search_value, $sortable_column, $sortType);
        $data          = $this->Student_model->get_student_details_list_by_condition(TRUE, $limit, $offset, $search_value, $sortable_column, $sortType);

        if ( ! empty($data))
        {
            echo json_encode(['draw' => $draw, 'data' => $data, 'recordsTotal' => count($total_records), 'recordsFiltered' => count($total_records)]);
        }
        else
        {
            echo json_encode(['draw' => $draw, 'data' => [], 'recordsTotal' => 0, 'recordsFiltered' => 0]);
        }
    }

    /**
     * Delete Student
     *
     * @param $student_id
     */
    public function delete($student_id)
    {
        $res = $this->Student_model->delete_student(['id' => $student_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/student/list');
    }

    /**
     * Get Sub Castes by Caste AJAX
     */
    public function get_sub_caste_by_caste_ajax()
    {
        $caste_id = $this->input->post('caste_id');

        if ( ! empty($caste_id))
        {
            $sub_caste_details = $this->App_model->get_details_by('sub_caste', ['caste_id' => $caste_id], 'result_array');

            if ( ! empty($sub_caste_details))
            {
                echo json_encode(['status' => 'true', 'data' => $sub_caste_details]);
            }
            else
            {
                echo json_encode(['status' => 'false']);
            }
        }
        else
        {
            echo json_encode(['status' => 'false']);
        }
    }

    /**
     * Get Section by Class AJAX
     */
    public function get_sub_section_by_class_ajax()
    {
        $class_id = $this->input->post('class_id');

        if ( ! empty($class_id))
        {
            $section_details = $this->App_model->get_details_by('section', ['class_id' => $class_id], 'result_array');

            if ( ! empty($section_details))
            {
                echo json_encode(['status' => 'true', 'data' => $section_details]);
            }
            else
            {
                echo json_encode(['status' => 'false']);
            }
        }
        else
        {
            echo json_encode(['status' => 'false']);
        }
    }

    /**
     * Get Subject Combinations by Class and Subject Type
     */
    public function get_subject_combination_ajax()
    {
        $class_id     = $this->input->post('class_id');
        $subject_type = $this->input->post('subject_type');

        if ( ! empty($class_id) && ! empty($subject_type))
        {
            $combination_details = $this->App_model->get_details_by('exam_settings_and_subject', ['class_id' => $class_id, 'subject_type' => $subject_type], 'result_array');

            if ( ! empty($combination_details))
            {
                echo json_encode(['status' => 'true', 'data' => $combination_details]);
            }
            else
            {
                echo json_encode(['status' => 'false']);
            }
        }
        else
        {
            echo json_encode(['status' => 'false']);
        }
    }
}