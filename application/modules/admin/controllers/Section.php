<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Section extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('single_insert/Section_model', 'single_insert/Class_model'),
    );

    /**
     * Section constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $section_id
     */
    public function add($section_id = NULL)
    {
        $this->form_validation->set_rules('section_name', 'Section Name', "trim|required|unique[section.section_name.id.{$section_id}]");
        $this->form_validation->set_rules('class_id', 'Class Name', "trim|required");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($section_id))
            {
                $section_details = $this->Section_model->get_section_detail_by(['id' => $section_id]);

                if ( ! empty($section_details))
                {
                    $dataArray = array(
                        'section_name' => $section_details['section_name'],
                        'class_id'     => $section_details['class_id'],
                    );

                    $dataArray['form_action'] = 'Edit Section';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/section/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Section';
            }

            $classes = $this->Class_model->get_class_detail_by([], 'result_array');

            $dataArray['classes'] = $classes;

            $dataArray['css_local'] = array('bootstrap-datepicker');
            $dataArray['js_local']  = array('bootstrap-datepicker');

            $dataArray['page_title'] = 'Section';
            $dataArray['breadcrumb'] = 'Initial Information / Section';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('single-insert/section/add-form', $dataArray);
        }
        else
        {

            $section_name = $this->input->post('section_name');
            $class_id     = $this->input->post('class_id');

            $params = array(
                'section_name' => $section_name,
                'class_id'     => $this->input->post('class_id'),
            );

            if ( ! empty($section_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_section_id = $this->Section_model->save_section($params, $section_id);
            }
            else
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_section_id = $this->Section_model->save_section($params);
            }

            if ( ! empty($new_section_id))
            {
                $this->session->set_flashdata('flash_message', (empty($section_id)) ? 'Section created successfully' : 'Section updated successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/section/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/section/list');
            }
        }
    }

    /**
     *
     */
    public function index()
    {
        $listing_headers = 'section_listing_headers';

        $data['source']          = site_url('admin/section/listSection_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Section';
        $data['page_title']      = 'Section';
        $data['breadcrumb']      = 'Initial Information / Section';

        $dataArray = $this->_table_listing($data);

        $this->load->view('single-insert/section/index', $dataArray);
    }

    /**
     *
     */
    public function listSection_Json()
    {
        $listing_headers = 'section_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Section_model->tbl_name       = 'section';
        $this->Section_model->join_tbl_name  = 'class';
        $this->Section_model->join_cond      = "{$this->Section_model->tbl_name}.class_id = {$this->Section_model->join_tbl_name}.id";
        $this->Section_model->select_db_cols = "section.id, section.class_id, section.section_name, section.updated_at, class.id as c_id,class.class_name";

        $this->Section_model->list_search_key = 'section.section_name';

        $resultdata = $this->Section_model->get_all_section_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Section
     *
     * @param $section_id
     */
    public function delete($section_id)
    {
        $res = $this->Section_model->delete_session(['id' => $section_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/section/list');
    }
}