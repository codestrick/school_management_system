<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Result extends My_Controller
{

    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model' => array(
            'result/Result_model',
            'single_insert/Subject_model',
            'result/Result_trans_model',
            'Exam_settings_and_subject_model'),
    );

    public function __construct()
    {
        parent::__construct();
    }

    //http://localhost/projects/school-management-system/admin/generate/result
    //akber e ai url ta call hobe ajax call korer somai. second time error message dkhabe
    public function generate_result()
    {
        $subject_combination_id = 1;
        $student_id = 1;
        $student_extra_info_id = 1;

        $Exam_settings_and_subject = $this->Exam_settings_and_subject_model->get_exam_settings_and_subject_detail_by(['id' => $subject_combination_id]);

        $no_of_exam = json_decode($Exam_settings_and_subject['no_of_exam'], 1);


        $no_of_exam_array_with_id = [];

        foreach ($no_of_exam as $k => $exam_number) {

            $result_array = [
                'student_id' => $student_id,
                'student_extra_info_id' => $student_extra_info_id,
                'exam_number' => $exam_number,
                'result_status' => 'f',
                'result_total' => 0,
            ];

            $no_of_exam_array_with_id[$exam_number] = $this->Result_model->save($result_array);

        }

        $exam_settings = $Exam_settings_and_subject['exam_settings'];
        $exam_settings = json_decode($exam_settings, 1);

        $fm_settings = $exam_settings['fm'];
        $acceptable_settings = $exam_settings['acceptable'];
        $pass_mark_settings = $exam_settings['pass_mark'];

        $total_array = [];

        foreach ($fm_settings as $k => $subject_and_marks) {

            foreach ($subject_and_marks as $key => $value) {

                $acceptable = $acceptable_settings[$k][$key];
                $pass_mark = $pass_mark_settings[$k][$key];

                $total_array[] = [
                    'exam_number' => $k,
                    'subject_id' => $key,
                    'fm' => $value,
                    'pass_mark' => $pass_mark,
                    'acceptable_status' => ($acceptable == 'yes') ? 1 : 0,
                ];
            }

        }

        if (!empty($total_array) && !empty($no_of_exam_array_with_id)) {
            foreach ($total_array as $k => $val) {

                $result_params = [
                    'result_id' => $no_of_exam_array_with_id[$val['exam_number']],
                    'subject_id' => $val['subject_id'],
                    'number' => 0,
                    'present_status' => 0,
                ];
                $this->Result_trans_model->save($result_params);
            }
        }
        die('result insert');
    }

    /**
     * Search Student View For Student Readmission OR Upgradation
     */
    public function search_student($student_id = NULL)
    {
        $this->form_validation->set_rules('std_reg_no', 'Student Registration Number Required.', "trim|required");

        if ($this->form_validation->run()) {
            $student_id = $this->input->post('std_reg_no');

            $student_info = $this->App_model->get_details_by('std_registration', ['reg_id' => $student_id]);

            if (empty($student_info)) {
                $this->session->set_flashdata('flash_message', 'Invalid student registration ID provided!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect(base_url('admin/result/student-search'));
            }

            redirect(base_url('admin/result/result-form/' . $student_info['id']));

        } else {
            $dataArray['page_title'] = 'Student';
            $dataArray['breadcrumb'] = 'Student';
            $dataArray['form_action'] = 'Search Student';
            $dataArray['form_url'] = base_url('admin/result/result-form');

            $dataArray['css_local'] = array();
            $dataArray['js_local'] = array();

            $dataArray['flash_message'] = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $dataArray['validation_err'] = validation_errors();

            $this->load->view('result/search-student', $dataArray);
        }
    }

    //http://localhost/school-management-system/admin/result/result-form/1/1
    public function result_form($student_id = NULL, $student_extra_info_id = NULL)
    {
        $dataArray['page_title'] = 'Student';
        $dataArray['breadcrumb'] = 'Student';
        $dataArray['form_action'] = 'Insert Result (SUBHAM GHORUI)';

        $dataArray['css_local'] = array();
        $dataArray['js_local'] = array('editable-table','result');

        $dataArray['flash_message'] = $this->session->flashdata('flash_message');
        $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

        $dataArray['validation_err'] = validation_errors();

        $Results = $this->Result_model->get_detail_by(['student_id' => $student_id, 'student_extra_info_id' => $student_extra_info_id], 'result_array');

        $all_details = [];

        foreach ($Results as $k => $val) {
            $Result_trans = $this->Result_trans_model->get_detail_by(['result_id' => $val['id']], 'result_array');

            $modified_result_trans = [];

            if (!empty($Result_trans)) {
                foreach ($Result_trans as $key => $value) {
                    $value['subject_name'] = $this->get_subject_name($value['subject_id']);
                    $modified_result_trans[] = $value;
                }

            }
            $val['result_trans'] = $modified_result_trans;

            $all_details[] = $val;

        }

        $dataArray['all_details'] = $all_details;

        $this->load->view('result/result-form', $dataArray);
    }

    public function ajax_submit_result(){
        $number = $this->input->post('number');
        $result_trans_id = $this->input->post('result_trans_id');
        if(!empty($result_trans_id) && $number){
            $this->Result_trans_model->save(['number' => $number], $result_trans_id);
        }
        echo true;
    }

    private function get_subject_name($subject_id = null)
    {
        $Subject = $this->Subject_model->get_subject_detail_by(['id' => $subject_id], 'row_array');
        $subject_name = '';
        if (!empty($Subject)) {

            $subject_name = $Subject['subject_name'];
        }
        return $subject_name;
    }
}