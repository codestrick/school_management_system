<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Session extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('Session_model'),
    );

    /**
     * Session constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function change_session(){

        $this->form_validation->set_rules('new_session_id', 'Session ID', "trim|required");

        if ($this->form_validation->run())
        {
            $session_data = $this->session->userdata['user_auth'];
            $session_data['session'] = $this->input->post('new_session_id');
            $this->session->unset_userdata('user_auth');
            $this->session->set_userdata('user_auth',$session_data);

        } else {

            $all_session = $this->Session_model->get_session_detail_by([],'result_array');

            $dataArray['css_local'] = array();
            $dataArray['js_local']  = array();

            $dataArray['page_title'] = 'Session';
            $dataArray['breadcrumb'] = 'Change Session';
            $dataArray['all_session'] = $all_session;

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('session/change-session', $dataArray);
        }

    }

    /**
     * Add, Edit Session
     *
     * @param null $session_id
     */
    public function add($session_id = NULL)
    {
        $this->form_validation->set_rules('ses_start_yr', 'Session start year', "trim|required");
        $this->form_validation->set_rules('ses_end_yr', 'Session end year', "trim|required");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($session_id))
            {
                $session_details = $this->Session_model->get_session_detail_by(['id' => $session_id]);

                if ( ! empty($session_details))
                {
                    $dataArray = array(
                        'ses_start_yr' => $session_details['ses_start_yr'],
                        'ses_end_yr'   => $session_details['ses_end_yr']
                    );

                    $dataArray['form_action'] = 'Edit Session';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/session/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Session';
            }

            $dataArray['css_local'] = array('bootstrap-datepicker');
            $dataArray['js_local']  = array('bootstrap-datepicker');

            $dataArray['page_title'] = 'Session';
            $dataArray['breadcrumb'] = 'Initial Information / Session';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('session/add-form', $dataArray);
        }
        else
        {
            $params = array(
                'ses_start_yr' => $this->input->post('ses_start_yr'),
                'ses_end_yr'   => $this->input->post('ses_end_yr')
            );

            if ( ! empty($session_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_session_id = $this->Session_model->save_session($params, $session_id);
            }
            else
            {
                $params['created_at'] = date("Y-m-d H:i:s");
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_session_id = $this->Session_model->save_session($params);
            }

            if ( ! empty($new_session_id))
            {
                $this->session->set_flashdata('flash_message', (empty($session_id)) ? 'Session created successfully' : 'Session updated successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/session/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/session/list');
            }
        }
    }

    /**
     * Session View
     */
    public function index()
    {
        $listing_headers = 'session_listing_headers';

        $data['source']          = site_url('admin/session/listSession_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Session';
        $data['page_title']      = 'Session';
        $data['breadcrumb']      = 'Initial Information / Session';

        $dataArray = $this->_table_listing($data);

        $this->load->view('session/index', $dataArray);
    }

    /**
     * Session DataTable JSON
     */
    public function listSession_Json()
    {
        $listing_headers = 'session_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Session_model->tbl_name       = 'session';
        $this->Session_model->select_db_cols = "id, ses_start_yr, ses_end_yr, created_at, updated_at";

        $this->Session_model->list_search_key  = 'ses_start_yr';
        $this->Session_model->list_search_key1 = 'ses_end_yr';
        $this->Session_model->list_search_key2 = 'created_at';
        $this->Session_model->list_search_key3 = 'updated_at';

        $resultdata = $this->Session_model->get_all_session_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Session
     *
     * @param $session_id
     */
    public function delete($session_id)
    {
        $res = $this->Session_model->delete_session(['id' => $session_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/session/list');
    }
}