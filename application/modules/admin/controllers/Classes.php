<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classes extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('single_insert/Class_model'),
    );

    /**
     * Classes constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $class_id
     */
    public function add($class_id = NULL)
    {
        $this->form_validation->set_rules('class_name', 'Class Name', "trim|required|unique[class.class_name.id.{$class_id}]");
        $this->form_validation->set_rules('class_type', 'Class Type', "trim|required");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($class_id))
            {
                $class_details = $this->Class_model->get_class_detail_by(['id' => $class_id]);

                if ( ! empty($class_details))
                {
                    $dataArray = array(
                        'class_name' => $class_details['class_name'],
                        'class_type' => $class_details['class_type'],
                    );

                    $dataArray['form_action'] = 'Edit Class';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/class/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Class';
            }

            $dataArray['css_local'] = array('bootstrap-datepicker');
            $dataArray['js_local']  = array('bootstrap-datepicker');

            $dataArray['page_title'] = 'Class';
            $dataArray['breadcrumb'] = 'Initial Information / Class';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('single-insert/class/add-form', $dataArray);
        }
        else
        {
            $params = array(
                'class_name' => $this->input->post('class_name'),
                'class_type' => $this->input->post('class_type'),
            );

            if ( ! empty($class_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_class_id = $this->Class_model->save_class($params, $class_id);
            }
            else
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_class_id = $this->Class_model->save_class($params);
            }

            if ( ! empty($new_class_id))
            {
                $this->session->set_flashdata('flash_message', (empty($class_id)) ? 'Class created successfully' : 'Class updated successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/class/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/class/list');
            }
        }
    }

    /**
     *
     */
    public function index()
    {
        $listing_headers = 'class_listing_headers';

        $data['source']          = site_url('admin/classes/listClass_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Class';
        $data['page_title']      = 'Class';
        $data['breadcrumb']      = 'Initial Information / Class';

        $dataArray = $this->_table_listing($data);

        $this->load->view('single-insert/class/index', $dataArray);
    }

    /**
     *
     */
    public function listClass_Json()
    {
        $listing_headers = 'class_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Class_model->tbl_name = 'class';

        $this->Class_model->select_db_cols = "id, class_name, class_type, updated_at";

        $this->Class_model->list_search_key = 'class_name';

        $resultdata = $this->Class_model->get_all_class_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Session
     *
     * @param $class_id
     */
    public function delete($class_id)
    {
        $res = $this->Class_model->delete_session(['id' => $class_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/class/list');
    }
}