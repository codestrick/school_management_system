<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_controller extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array(
            'single_insert/Class_model',
            'single_insert/Section_model',
            'Exam_settings_and_subject_model',
        ),
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function get_section()
    {
       $class_id = $this->input->post('class_id');

       $classes = $this->Section_model->get_section_detail_by(['class_id' => $class_id],'result_array');

       if(!empty($classes)){
           echo json_encode([
               'status' => TRUE,
               'data' => $classes
           ]);
       }else{
           echo json_encode([
               'status' => FALSE,
           ]);
       }

    }

    public function get_exam_combination_name(){

        $class_id = $this->input->post('class_id');
        $subject_type = $this->input->post('subject_type');

        $return = ['status' => false, 'data' => []];

        if(!empty($class_id) && !empty($subject_type)){

            $exam_settings = $this->Exam_settings_and_subject_model->get_exam_settings_and_subject_detail_by(['subject_type' => $subject_type,'class_id' => $class_id],'result_array');

            if(!empty($exam_settings)){

                $return = ['status' => true, 'data' => $exam_settings];
            }else{
                $return = ['status' => false, 'data' => []];
            }
        }


        echo json_encode($return);
    }
}