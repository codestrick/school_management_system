<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faculty extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('Faculty_model', 'single_insert/Subject_model'),
    );

    /**
     * Faculty constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $faculty_id
     */
    public function add($faculty_id = NULL)
    {
        $this->form_validation->set_rules('faculty_name', 'Faculty Name', "trim|required|unique[faculty.name.id.{$faculty_id}]");
        $this->form_validation->set_rules('faculty_email', 'Faculty E-mail', "trim|required|valid_email");
        $this->form_validation->set_rules('faculty_mobile', 'Faculty Mobile', "trim|required|max_length[10]|min_length[10]");
        $this->form_validation->set_rules('faculty_address', 'Faculty Address', "trim|required");
        $this->form_validation->set_rules('subject_id', 'Faculty Subject', "trim|required");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($faculty_id))
            {
                $faculty_details = $this->Faculty_model->get_faculty_detail_by(['id' => $faculty_id]);

                if ( ! empty($faculty_details))
                {
                    $dataArray = array(
                        'faculty_name'    => $faculty_details['name'],
                        'faculty_email'   => $faculty_details['email'],
                        'faculty_mobile'  => $faculty_details['mobile'],
                        'faculty_address' => $faculty_details['address'],
                        'subject_id'      => $faculty_details['subject_id'],
                    );

                    $dataArray['form_action'] = 'Edit Faculty';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/faculty/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Faculty';
            }

            $dataArray['subjects'] = $this->Subject_model->get_subject_detail_by([], 'result_array');

            $dataArray['css_local'] = array('');
            $dataArray['js_local']  = array('');

            $dataArray['page_title'] = 'Faculty';
            $dataArray['breadcrumb'] = 'Initial Information / Faculty';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('faculty/add-form', $dataArray);
        }
        else
        {
            $params = array(
                'name'       => $this->input->post('faculty_name'),
                'email'      => $this->input->post('faculty_email'),
                'mobile'     => $this->input->post('faculty_mobile'),
                'address'    => $this->input->post('faculty_address'),
                'subject_id' => $this->input->post('subject_id'),
            );

            if ( ! empty($faculty_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_faculty_id = $this->Faculty_model->save_faculty($params, $faculty_id);
            }
            else
            {
                $params['created_at'] = date("Y-m-d H:i:s");
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_faculty_id = $this->Faculty_model->save_faculty($params);
            }

            if ( ! empty($new_faculty_id))
            {
                $this->session->set_flashdata('flash_message', (empty($faculty_id)) ? 'Faculty created successfully' : 'Faculty updated successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/faculty/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/faculty/list');
            }
        }
    }

    /**
     *
     */
    public function index()
    {
        $listing_headers = 'faculty_listing_headers';

        $data['source']          = site_url('admin/faculty/listFaculty_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Faculty';
        $data['page_title']      = 'Faculty';
        $data['breadcrumb']      = 'Faculty';

        $dataArray = $this->_table_listing($data);

        $this->load->view('faculty/index', $dataArray);
    }

    /**
     *
     */
    public function listFaculty_Json()
    {
        $listing_headers = 'faculty_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Faculty_model->tbl_name = 'faculty';

        $this->Faculty_model->join_tbl_name = 'subject';

        $this->Faculty_model->join_cond = "{$this->Faculty_model->tbl_name}.subject_id = {$this->Faculty_model->join_tbl_name}.id";

        $this->Faculty_model->select_db_cols = "{$this->Faculty_model->tbl_name}.id, {$this->Faculty_model->tbl_name}.name,
                                                {$this->Faculty_model->tbl_name}.email, {$this->Faculty_model->tbl_name}.mobile, 
                                                {$this->Faculty_model->tbl_name}.address, {$this->Faculty_model->join_tbl_name}.subject_name, 
                                                {$this->Faculty_model->tbl_name}.created_at, {$this->Faculty_model->tbl_name}.updated_at";

        $this->Faculty_model->list_search_key = "{$this->Faculty_model->tbl_name}.name";
        $this->Faculty_model->list_search_key1 = "{$this->Faculty_model->tbl_name}.email";
        $this->Faculty_model->list_search_key2 = "{$this->Faculty_model->tbl_name}.mobile";
        $this->Faculty_model->list_search_key3 = "{$this->Faculty_model->tbl_name}.address";
        $this->Faculty_model->list_search_key4 = "{$this->Faculty_model->join_tbl_name}.subject_name";

        $resultdata = $this->Faculty_model->get_all_faculty_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Faculty
     *
     * @param $faculty_id
     */
    public function delete($faculty_id)
    {
        $res = $this->Faculty_model->delete_faculty(['id' => $faculty_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/faculty/list');
    }
}