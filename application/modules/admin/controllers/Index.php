<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends My_Controller
{
    public $autoload = array(
        'model' => array('Session_model', 'Letter_grade_model'),
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function Index()
    {
        redirect(base_url('admin/dashboard'));
    }

    public function dashboard()
    {
        $to_day_date = Date("Y-m-d");
        $to_day_date = strtotime($to_day_date);
        $to_day_year = date("Y", $to_day_date);

        //get current year session
        $session_detail = $this->Session_model->get_session_detail_by(['ses_start_yr' => $to_day_year]);

        if (empty($session_detail))
        {
            $params['ses_start_yr'] = $to_day_year;
            $params['ses_end_yr']   = ($to_day_year + 1);
            $params['created_at']   = date("Y-m-d H:i:s");
            $params['updated_at']   = date("Y-m-d H:i:s");

            $new_session_id = $this->Session_model->save_session($params);
        }

        $dataArray['page_title']  = 'Dashboard';
        $dataArray['breadcrumb']  = 'Dashboard';
        $dataArray['form_action'] = 'Dashboard';

        $this->load->view('dashboard', $dataArray);
    }

    public function letter_grade($status = '')
    {
        $result = $this->Letter_grade_model->get_letter_grade_detail_by([], 'result_array');

        if ($status == 'update')
        {
            $data = $this->input->post();

            $a = $this->generate_grade_array($data);

            if (empty($result))
            {
                $params['grade_json'] = json_encode($data);
                $params['all_data']   = json_encode($a);
                $params['updated_at'] = date("Y-m-d H:i:s");

                $res = $this->Letter_grade_model->save_letter_grade($params);

                echo ! empty($res) ? 'success' : 'err';

            }
            else
            {
                $params['grade_json'] = json_encode($data);
                $params['all_data']   = json_encode($a);
                $params['updated_at'] = date("Y-m-d H:i:s");

                if ( ! empty($result[0]['id']))
                {
                    $id  = $result[0]['id'];
                    $res = $this->Letter_grade_model->save_letter_grade($params, $id);

                    echo ! empty($res) ? 'success' : 'err';
                }
                else
                {
                    echo 'err';
                }
            }
        }
        else
        {
            if ( ! empty($result[0]))
            {
                $dataArray['data'] = json_decode($result[0]['grade_json'], 1);
            }
            else
            {
                $dataArray['data'] = [];
            }

            $dataArray['page_title']  = 'Letter Grade';
            $dataArray['breadcrumb']  = 'Letter Grade';
            $dataArray['form_action'] = 'Letter Grade';

            $this->load->view('letter-grade', $dataArray);
        }

    }

    public function generate_grade_array($data)
    {
        $a = [];

        for ($i = 0; $i <= 100; $i++)
        {

            if ( ! empty($data['end_number_1']) && $i >= $data['start_number_1'] && $i <= $data['end_number_1'])
            {

                $a[$i] = [
                    'grade'      => $data['grade_1'],
                    'grade_name' => $data['grade_name_1']
                ];
            }
            else if ( ! empty($data['start_number_2']) && ! empty($data['end_number_2']) && $i >= $data['start_number_2'] && $i <= $data['end_number_2'])
            {
                $a[$i] = [
                    'grade'      => $data['grade_2'],
                    'grade_name' => $data['grade_name_2']
                ];
            }
            else if ( ! empty($data['start_number_3']) && ! empty($data['end_number_3']) && $i >= $data['start_number_3'] && $i <= $data['end_number_3'])
            {
                $a[$i] = [
                    'grade'      => $data['grade_3'],
                    'grade_name' => $data['grade_name_3']
                ];
            }
            else if ( ! empty($data['start_number_4']) && ! empty($data['end_number_4']) && $i >= $data['start_number_4'] && $i <= $data['end_number_4'])
            {
                $a[$i] = [
                    'grade'      => $data['grade_4'],
                    'grade_name' => $data['grade_name_4']
                ];
            }
            else if ( ! empty($data['start_number_5']) && ! empty($data['end_number_5']) && $i >= $data['start_number_5'] && $i <= $data['end_number_5'])
            {
                $a[$i] = [
                    'grade'      => $data['grade_5'],
                    'grade_name' => $data['grade_name_5']
                ];
            }
            else if ( ! empty($data['start_number_6']) && ! empty($data['end_number_6']) && $i >= $data['start_number_6'] && $i <= $data['end_number_6'])
            {
                $a[$i] = [
                    'grade'      => $data['grade_6'],
                    'grade_name' => $data['grade_name_6']
                ];
            }
            else if ( ! empty($data['start_number_7']) && ! empty($data['end_number_7']) && $i >= $data['start_number_7'] && $i <= $data['end_number_7'])
            {
                $a[$i] = [
                    'grade'      => $data['grade_7'],
                    'grade_name' => $data['grade_name_7']
                ];
            }
            else if ( ! empty($data['start_number_8']) && ! empty($data['end_number_8']) && $i >= $data['start_number_8'] && $i <= $data['end_number_8'])
            {
                $a[$i] = [
                    'grade'      => $data['grade_8'],
                    'grade_name' => $data['grade_name_8']
                ];
            }
            else if ( ! empty($data['start_number_9']) && ! empty($data['end_number_9']) && $i >= $data['start_number_9'] && $i <= $data['end_number_9'])
            {
                $a[$i] = [
                    'grade'      => $data['grade_9'],
                    'grade_name' => $data['grade_name_9']
                ];
            }
            else if ( ! empty($data['start_number_10']) && ! empty($data['end_number_10']) && $i >= $data['start_number_10'] && $i <= $data['end_number_10'])
            {
                $a[$i] = [
                    'grade'      => $data['grade_10'],
                    'grade_name' => $data['grade_name_10']
                ];
            }
            else
            {
                $a[$i] = [
                    'grade'      => 'undefined',
                    'grade_name' => 'undefined'
                ];
            }
        }

        return $a;
    }
}