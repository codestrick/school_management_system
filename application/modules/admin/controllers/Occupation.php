<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Occupation extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('single_insert/Occupation_model'),
    );

    /**
     * Occupation constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $occupation_id
     */
    public function add($occupation_id = NULL)
    {
        $this->form_validation->set_rules('occupation_name', 'Occupation Name', "trim|required|unique[occupation.occupation_name.id.{$occupation_id}]");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($occupation_id))
            {
                $occupation_details = $this->Occupation_model->get_occupation_detail_by(['id' => $occupation_id]);

                if ( ! empty($occupation_details))
                {
                    $dataArray = array(
                        'occupation_name' => $occupation_details['occupation_name'],
                    );

                    $dataArray['form_action'] = 'Edit Occupation';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/occupation/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Occupation';
            }

            $dataArray['css_local'] = array('bootstrap-datepicker');
            $dataArray['js_local']  = array('bootstrap-datepicker');

            $dataArray['page_title'] = 'Occupation';
            $dataArray['breadcrumb'] = 'Initial Information / Occupation';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('single-insert/occupation/add-form', $dataArray);
        }
        else
        {
            $params = array(
                'occupation_name' => $this->input->post('occupation_name'),
            );

            if ( ! empty($occupation_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_occupation_id = $this->Occupation_model->save_occupation($params, $occupation_id);
            }
            else
            {
                $params['created_at'] = date("Y-m-d H:i:s");
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_occupation_id = $this->Occupation_model->save_occupation($params);
            }

            if ( ! empty($new_occupation_id))
            {
                $this->session->set_flashdata('flash_message', (empty($new_occupation_id)) ? 'Occupation created successfully' : 'Occupation updated successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/occupation/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/occupation/list');
            }
        }
    }

    /**
     *
     */
    public function index()
    {
        $listing_headers = 'occupation_listing_headers';

        $data['source']          = site_url('admin/occupation/listOccupation_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Occupation';
        $data['page_title']      = 'Occupation';
        $data['breadcrumb']      = 'Initial Information / Occupation';

        $dataArray = $this->_table_listing($data);

        $this->load->view('single-insert/occupation/index', $dataArray);
    }

    /**
     *
     */
    public function listOccupation_Json()
    {
        $listing_headers = 'occupation_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Occupation_model->tbl_name = 'occupation';

        $this->Occupation_model->select_db_cols = "id, occupation_name, created_at, updated_at";

        $this->Occupation_model->list_search_key = 'occupation_name';

        $resultdata = $this->Occupation_model->get_all_occupation_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Session
     *
     * @param $occupation_id
     */
    public function delete($occupation_id)
    {
        $res = $this->Occupation_model->delete_occupation(['id' => $occupation_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/occupation/list');
    }
}