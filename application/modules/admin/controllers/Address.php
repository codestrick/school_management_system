<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('single_insert/Address_model'),
    );

    /**
     * Session constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $address_id
     */
    public function add($address_id = NULL)
    {
        $this->form_validation->set_rules('state', 'State', "trim|required");
        $this->form_validation->set_rules('district', 'District', "trim|required");
        $this->form_validation->set_rules('location', 'Location', "trim|required");
        $this->form_validation->set_rules('pincode', 'Address', "trim|required");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($address_id))
            {
                $address_details = $this->Address_model->get_address_detail_by(['id' => $address_id]);


                $arr = explode('|', $address_details['address']);

                if ( ! empty($address_details))
                {
                    $dataArray = array(
                        'state'    => $arr[0],
                        'district' => $arr[1],
                        'location' => $arr[2],
                        'pincode'  => $address_details['pincode'],
                    );

                    $dataArray['form_action'] = 'Edit Address';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/address/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Address';
            }

            $dataArray['css_local'] = array('bootstrap-datepicker');
            $dataArray['js_local']  = array('bootstrap-datepicker');

            $dataArray['page_title'] = 'Address';
            $dataArray['breadcrumb'] = 'Address';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('single-insert/address/add-form', $dataArray);
        }
        else
        {
            $state    = ucwords($this->input->post('state'));
            $district = ucwords($this->input->post('district'));
            $location = ucwords($this->input->post('location'));

            $params = array(
                'address' => $state . '|' . $district . '|' . $location,
                'pincode' => $this->input->post('pincode'),
            );

            if ( ! empty($address_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_address_id = $this->Address_model->save_address($params, $address_id);
            }
            else
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_address_id = $this->Address_model->save_address($params);
            }

            if ( ! empty($new_address_id))
            {
                $this->session->set_flashdata('flash_message', (empty($address_id)) ? 'Address created successfully' : 'Address updated successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/address/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/address/list');
            }
        }
    }

    /**
     *
     */
    public function index()
    {
        $listing_headers = 'address_listing_headers';

        $data['source']          = site_url('admin/address/listAddress_Json');
        $data['listing_headers'] = $listing_headers;
        $data['page_title']      = 'Address';
        $data['breadcrumb']      = 'Initial Information / Address';
        $data['form_action']     = 'List Address';

        $dataArray = $this->_table_listing($data);

        $this->load->view('single-insert/address/index', $dataArray);
    }

    /**
     *
     */
    public function listAddress_Json()
    {
        $listing_headers = 'address_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Address_model->tbl_name = 'address';

        $this->Address_model->select_db_cols = "id, address, pincode, updated_at";

        $this->Address_model->list_search_key = 'address';

        $resultdata = $this->Address_model->get_all_address_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Session
     *
     * @param $session_id
     */
    public function delete($session_id)
    {
        $res = $this->Address_model->delete_address(['id' => $session_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/address/list');
    }

    /**
     *
     */
    public function ajax_get_address()
    {
        $id = $this->input->post('id');

        $address = [
            'status'   => FALSE,
            'state'    => '',
            'district' => '',
            'location' => '',
            'pincode'  => '',
        ];

        if ( ! empty($id))
        {
            $address_details = $this->Address_model->get_address_detail_by(['id' => $id]);
            $arr             = explode('|', $address_details['address']);
            $address         = [
                'status'   => TRUE,
                'state'    => $arr[0],
                'district' => $arr[1],
                'location' => $arr[2],
                'pincode'  => $address_details['pincode'],
            ];
        }

        echo json_encode($address);
    }
}