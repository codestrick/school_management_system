<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subject extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('single_insert/Subject_model'),
    );

    /**
     * Subject constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $subject_id
     */
    public function add($subject_id = NULL)
    {
        $this->form_validation->set_rules('subject_name', 'Subject Name', "trim|required|unique[subject.subject_name.id.{$subject_id}]");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($subject_id))
            {
                $subject_details = $this->Subject_model->get_subject_detail_by(['id' => $subject_id]);

                if ( ! empty($subject_details))
                {
                    $dataArray = array(
                        'subject_name' => $subject_details['subject_name'],
                    );

                    $dataArray['form_action'] = 'Edit Subject';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/subject/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Subject';
            }

            $dataArray['css_local'] = array('bootstrap-datepicker');
            $dataArray['js_local']  = array('bootstrap-datepicker');

            $dataArray['page_title'] = 'Subject';
            $dataArray['breadcrumb'] = 'Initial Information / Subject';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('single-insert/subject/add-form', $dataArray);
        }
        else
        {
            $params = array(
                'subject_name' => $this->input->post('subject_name'),
            );

            if ( ! empty($subject_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_subject_id = $this->Subject_model->save_subject($params, $subject_id);
            }
            else
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_subject_id = $this->Subject_model->save_subject($params);
            }

            if ( ! empty($new_subject_id))
            {
                $this->session->set_flashdata('flash_message', (empty($subject_id)) ? 'Subject created successfully' : 'Subject updated successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/subject/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/subject/list');
            }
        }
    }

    /**
     *
     */
    public function index()
    {
        $listing_headers = 'subject_listing_headers';

        $data['source']          = site_url('admin/subject/listSubject_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Subject';
        $data['page_title']      = 'Subject';
        $data['breadcrumb']      = 'Initial Information / Subject';

        $dataArray = $this->_table_listing($data);

        $this->load->view('single-insert/subject/index', $dataArray);
    }

    /**
     *
     */
    public function listSubject_Json()
    {
        $listing_headers = 'subject_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Subject_model->tbl_name = 'subject';

        $this->Subject_model->select_db_cols = "id, subject_name, updated_at";

        $this->Subject_model->list_search_key = 'subject_name';

        $resultdata = $this->Subject_model->get_all_subject_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Session
     *
     * @param $subject_id
     */
    public function delete($subject_id)
    {
        $res = $this->Subject_model->delete_subject(['id' => $subject_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/session/list');
    }
}