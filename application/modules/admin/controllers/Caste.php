<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Caste extends My_Controller
{
    /**
     * @var array
     */
    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('single_insert/Caste_model'),
    );

    /**
     * Caste constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $caste_id
     */
    public function add($caste_id = NULL)
    {
        $this->form_validation->set_rules('caste_name', 'Caste Name', "trim|required|unique[caste.caste_name.id.{$caste_id}]");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($caste_id))
            {
                $caste_details = $this->Caste_model->get_caste_detail_by(['id' => $caste_id]);

                if ( ! empty($caste_details))
                {
                    $dataArray = array(
                        'caste_name' => $caste_details['caste_name'],
                    );

                    $dataArray['form_action'] = 'Edit Caste';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('admin/caste/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Caste';
            }

            $dataArray['css_local'] = array('bootstrap-datepicker');
            $dataArray['js_local']  = array('bootstrap-datepicker');

            $dataArray['page_title'] = 'Caste';
            $dataArray['breadcrumb'] = 'Initial Information / Caste';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('single-insert/caste/add-form', $dataArray);
        }
        else
        {
            $params = array(
                'caste_name' => $this->input->post('caste_name'),
            );

            if ( ! empty($cast_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_caste_id = $this->Caste_model->save_caste($params, $caste_id);
            }
            else
            {
                $params['created_at'] = date("Y-m-d H:i:s");
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_caste_id = $this->Caste_model->save_caste($params);
            }

            if ( ! empty($new_caste_id))
            {
                $this->session->set_flashdata('flash_message', (empty($caste_id)) ? 'Caste created successfully' : 'Caste updated successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('admin/caste/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('admin/caste/list');
            }
        }
    }

    /**
     *
     */
    public function index()
    {
        $listing_headers = 'caste_listing_headers';

        $data['source']          = site_url('admin/caste/listCaste_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Caste';
        $data['page_title']      = 'Caste';
        $data['breadcrumb']      = 'Initial Information / Caste';

        $dataArray = $this->_table_listing($data);

        $this->load->view('single-insert/caste/index', $dataArray);
    }

    /**
     *
     */
    public function listCaste_Json()
    {
        $listing_headers = 'caste_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Caste_model->tbl_name = 'caste';

        $this->Caste_model->select_db_cols = "id, caste_name, created_at, updated_at";

        $this->Caste_model->list_search_key = 'caste_name';

        $resultdata = $this->Caste_model->get_all_caste_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Caste
     *
     * @param $caste_id
     */
    public function delete($caste_id)
    {
        $res = $this->Caste_model->delete_caste(['id' => $caste_id]);

        $this->session->set_flashdata('flash_message', $res['msg']);
        $this->session->set_flashdata('flash_message_status', $res['status']);

        redirect('admin/caste/list');
    }
}