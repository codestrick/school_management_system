<!-- Basic layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }

        if ( ! empty($validation_err))
        {
            ?>
            <div class="alert alert-danger alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
            <?php
        }
        ?>

        <!--jquery validation error container-->
        <div id="errorContainer" class="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p class="text-bold">Please correct the following errors and try again:</p>
            <ul></ul>
        </div>

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title text-bold"><?php echo ! empty($form_action) ? $form_action : ''; ?></h5>
                <div class="heading-elements"></div>
            </div>

            <div class="panel-body">
                <form id="save_form" action="" method="post" class="form-horizontal">

                    <div class="form-group">
                        <label class="col-lg-1 control-label text-bold">Name</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="name" id="name" autocomplete="off"
                                   value="<?php echo ! empty($name) ? $name : $this->input->post('name'); ?>"
                                   placeholder="Enter Name" required data-msg-required="Name Required"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-1 control-label text-bold">E-mail</label>
                        <div class="col-lg-4">
                            <input type="email" class="form-control" name="email" id="email" autocomplete="off"
                                   value="<?php echo ! empty($email) ? $email : $this->input->post('email'); ?>"
                                   placeholder="Enter E-mail" required data-msg-required="E-mail Required"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-1 control-label text-bold">Mobile</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="mobile" id="mobile"
                                   maxlength="10" minlength="10" autocomplete="off"
                                   value="<?php echo ! empty($mobile) ? $mobile : $this->input->post('mobile'); ?>"
                                   placeholder="Enter Mobile" required data-msg-required="Mobile Required"/>
                        </div>
                    </div>

                    <div class="text-right col-lg-5">
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<!-- /basic layout -->