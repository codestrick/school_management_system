<style>
    .gradation-div {
        border: 1px solid gainsboro;
        padding-top: 8px;
        text-align: center;
        margin-right: 11px;
        margin-left: 40px;
    }

    @media (max-width: 900px) {
        .gradation-div {
            margin-left: 11px !important;
        }
    }
</style>

<!-- Basic layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }

        if ( ! empty($validation_err))
        {
            ?>
            <div class="alert alert-danger alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
            <?php
        }
        ?>

        <!--jquery validation error container-->
        <div id="errorContainer" class="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p class="text-bold">Please correct the following errors and try again:</p>
            <ul></ul>
        </div>
        <form id="save_form" class="letter-grade" action="#" method="post">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class="panel-title text-bold"><?php echo ! empty($form_action) ? $form_action : ''; ?></h5>
                            <div class="heading-elements">
                                <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <div class="col-lg-2 gradation-div">
                                        <label class="control-label"><strong>Grade 1</strong></label>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="start_number_1" id="start_number_1"
                                               value="0"
                                               placeholder="Start number" data-msg-required="" disabled/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="end_number_1" id="end_number_1"
                                               value="<?php echo ! empty($data['end_number_1']) ? $data['end_number_1'] : ''; ?>"
                                               placeholder="End Number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="grade_1" id="grade_1"
                                               value="<?php echo ! empty($data['grade_1']) ? $data['grade_1'] : ''; ?>"
                                               placeholder="Grade" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="grade_name_1" id="grade_name_1"
                                               value="<?php echo ! empty($data['grade_name_1']) ? $data['grade_name_1'] : ''; ?>"
                                               placeholder="Grade Name" data-msg-required=""/>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-lg-2 gradation-div col-md-offset-1">
                                        <label class="control-label"><strong>Grade 2</strong></label>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="start_number_2" id="start_number_2"
                                               value="<?php echo ! empty($data['start_number_2']) ? $data['start_number_2'] : ''; ?>"
                                               placeholder="Start number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="end_number_2" id="end_number_2"
                                               value="<?php echo ! empty($data['end_number_2']) ? $data['end_number_2'] : ''; ?>"
                                               placeholder="End Number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="grade_2" id="grade_2"
                                               value="<?php echo ! empty($data['grade_2']) ? $data['grade_2'] : ''; ?>"
                                               placeholder="Grade" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="grade_name_2" id="grade_name_2"
                                               value="<?php echo ! empty($data['grade_name_2']) ? $data['grade_name_2'] : ''; ?>"
                                               placeholder="Grade Name" data-msg-required=""/>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-lg-2 gradation-div col-md-offset-1">
                                        <label class="control-label"><strong>Grade 3</strong></label>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="start_number_3" id="start_number_3"
                                               value="<?php echo ! empty($data['start_number_3']) ? $data['start_number_3'] : ''; ?>"
                                               placeholder="Start number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="end_number_3" id="end_number_3"
                                               value="<?php echo ! empty($data['end_number_3']) ? $data['end_number_3'] : ''; ?>"
                                               placeholder="End Number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="grade_3" id="grade_3"
                                               value="<?php echo ! empty($data['grade_3']) ? $data['grade_3'] : ''; ?>"
                                               placeholder="Grade" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="grade_name_3" id="grade_name_3"
                                               value="<?php echo ! empty($data['grade_name_3']) ? $data['grade_name_3'] : ''; ?>"
                                               placeholder="Grade Name" data-msg-required=""/>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-lg-2 gradation-div">
                                        <label class="control-label"><strong>Grade 3</strong></label>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="start_number_4" id="start_number_4"
                                               value="<?php echo ! empty($data['start_number_4']) ? $data['start_number_4'] : ''; ?>"
                                               placeholder="Start number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="end_number_4" id="end_number_4"
                                               value="<?php echo ! empty($data['end_number_4']) ? $data['end_number_4'] : ''; ?>"
                                               placeholder="End Number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="grade_4" id="grade_4"
                                               value="<?php echo ! empty($data['grade_4']) ? $data['grade_4'] : ''; ?>"
                                               placeholder="Grade" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="grade_name_4" id="grade_name_4"
                                               value="<?php echo ! empty($data['grade_name_4']) ? $data['grade_name_4'] : ''; ?>"
                                               placeholder="Grade Name" data-msg-required=""/>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-lg-2 gradation-div">
                                        <label class="control-label"><strong>Grade 5</strong></label>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="start_number_5" id="start_number_5"
                                               value="<?php echo ! empty($data['start_number_5']) ? $data['start_number_5'] : ''; ?>"
                                               placeholder="Start number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="end_number_5" id="end_number_5"
                                               value="<?php echo ! empty($data['end_number_5']) ? $data['end_number_5'] : ''; ?>"
                                               placeholder="End Number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="grade_5" id="grade_5"
                                               value="<?php echo ! empty($data['grade_5']) ? $data['grade_5'] : ''; ?>"
                                               placeholder="Grade" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="grade_name_5" id="grade_name_5"
                                               value="<?php echo ! empty($data['grade_name_5']) ? $data['grade_name_5'] : ''; ?>"
                                               placeholder="Grade Name" data-msg-required=""/>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-lg-2 gradation-div">
                                        <label class="control-label"><strong>Grade 6</strong></label>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="start_number_6" id="start_number_6"
                                               value="<?php echo ! empty($data['start_number_6']) ? $data['start_number_6'] : ''; ?>"
                                               placeholder="Start number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="end_number_6" id="end_number_6"
                                               value="<?php echo ! empty($data['end_number_6']) ? $data['end_number_6'] : ''; ?>"
                                               placeholder="End Number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="grade_6" id="grade_6"
                                               value="<?php echo ! empty($data['grade_6']) ? $data['grade_6'] : ''; ?>"
                                               placeholder="Grade" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="grade_name_6" id="grade_name_6"
                                               value="<?php echo ! empty($data['grade_name_6']) ? $data['grade_name_6'] : ''; ?>"
                                               placeholder="Grade Name" data-msg-required=""/>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-lg-2 gradation-div">
                                        <label class="control-label"><strong>Grade 7</strong></label>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="start_number_7" id="start_number_7"
                                               value="<?php echo ! empty($data['start_number_7']) ? $data['start_number_7'] : ''; ?>"
                                               placeholder="Start number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="end_number_7" id="end_number_7"
                                               value="<?php echo ! empty($data['end_number_7']) ? $data['end_number_7'] : ''; ?>"
                                               placeholder="End Number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="grade_7" id="grade_7"
                                               value="<?php echo ! empty($data['grade_7']) ? $data['grade_7'] : ''; ?>"
                                               placeholder="Grade" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="grade_name_7" id="grade_name_7"
                                               value="<?php echo ! empty($data['grade_name_7']) ? $data['grade_name_7'] : ''; ?>"
                                               placeholder="Grade Name" data-msg-required=""/>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-lg-2 gradation-div">
                                        <label class="control-label"><strong>Grade 8</strong></label>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="start_number_8" id="start_number_8"
                                               value="<?php echo ! empty($data['start_number_8']) ? $data['start_number_8'] : ''; ?>"
                                               placeholder="Start number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="end_number_8" id="end_number_8"
                                               value="<?php echo ! empty($data['end_number_8']) ? $data['end_number_8'] : ''; ?>"
                                               placeholder="End Number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="grade_8" id="grade_8"
                                               value="<?php echo ! empty($data['grade_8']) ? $data['grade_8'] : ''; ?>"
                                               placeholder="Grade" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="grade_name_8" id="grade_name_8"
                                               value="<?php echo ! empty($data['grade_name_8']) ? $data['grade_name_8'] : ''; ?>"
                                               placeholder="Grade Name" data-msg-required=""/>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-lg-2 gradation-div">
                                        <label class="control-label"><strong>Grade 9</strong></label>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="start_number_9" id="start_number_9"
                                               value="<?php echo ! empty($data['start_number_9']) ? $data['start_number_9'] : ''; ?>"
                                               placeholder="Start number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="end_number_9" id="end_number_9"
                                               value="<?php echo ! empty($data['end_number_9']) ? $data['end_number_9'] : ''; ?>"
                                               placeholder="End Number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="grade_9" id="grade_9"
                                               value="<?php echo ! empty($data['grade_9']) ? $data['grade_9'] : ''; ?>"
                                               placeholder="Grade" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="grade_name_9" id="grade_name_9"
                                               value="<?php echo ! empty($data['grade_name_9']) ? $data['grade_name_9'] : ''; ?>"
                                               placeholder="Grade Name" data-msg-required=""/>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="col-lg-2 gradation-div">
                                        <label class="control-label"><strong>Grade 10</strong></label>
                                    </div>
                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="start_number_10" id="start_number_10"
                                               value="<?php echo ! empty($data['start_number_10']) ? $data['start_number_10'] : ''; ?>"
                                               placeholder="Start number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="number" class="form-control" name="end_number_10" id="end_number_10"
                                               value="<?php echo ! empty($data['end_number_10']) ? $data['end_number_10'] : ''; ?>"
                                               placeholder="End Number" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-2">
                                        <input type="text" class="form-control" name="grade_10" id="grade_10"
                                               value="<?php echo ! empty($data['grade_10']) ? $data['grade_10'] : ''; ?>"
                                               placeholder="Grade" data-msg-required=""/>
                                    </div>

                                    <div class="col-lg-3">
                                        <input type="text" class="form-control" name="grade_name_10" id="grade_name_10"
                                               value="<?php echo ! empty($data['grade_name_10']) ? $data['grade_name_10'] : ''; ?>"
                                               placeholder="Grade Name" data-msg-required=""/>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $("#save_form").submit(function (event) {
            event.preventDefault();
            $.ajax({
                url: base_url + 'admin/letter-grade/update',
                data: {
                    start_number_1: $("#start_number_1").val(),
                    start_number_2: $("#start_number_2").val(),
                    start_number_3: $("#start_number_3").val(),
                    start_number_4: $("#start_number_4").val(),
                    start_number_5: $("#start_number_5").val(),
                    start_number_6: $("#start_number_6").val(),
                    start_number_7: $("#start_number_7").val(),
                    start_number_8: $("#start_number_8").val(),
                    start_number_9: $("#start_number_9").val(),
                    start_number_10: $("#start_number_10").val(),
                    end_number_1: $("#end_number_1").val(),
                    end_number_2: $("#end_number_2").val(),
                    end_number_3: $("#end_number_3").val(),
                    end_number_4: $("#end_number_4").val(),
                    end_number_5: $("#end_number_5").val(),
                    end_number_6: $("#end_number_6").val(),
                    end_number_7: $("#end_number_7").val(),
                    end_number_8: $("#end_number_8").val(),
                    end_number_9: $("#end_number_9").val(),
                    end_number_10: $("#end_number_10").val(),
                    grade_1: $("#grade_1").val(),
                    grade_2: $("#grade_2").val(),
                    grade_3: $("#grade_3").val(),
                    grade_4: $("#grade_4").val(),
                    grade_5: $("#grade_5").val(),
                    grade_6: $("#grade_6").val(),
                    grade_7: $("#grade_7").val(),
                    grade_8: $("#grade_8").val(),
                    grade_9: $("#grade_9").val(),
                    grade_10: $("#grade_10").val(),
                    grade_name_1: $("#grade_name_1").val(),
                    grade_name_2: $("#grade_name_2").val(),
                    grade_name_3: $("#grade_name_3").val(),
                    grade_name_4: $("#grade_name_4").val(),
                    grade_name_5: $("#grade_name_5").val(),
                    grade_name_6: $("#grade_name_6").val(),
                    grade_name_7: $("#grade_name_7").val(),
                    grade_name_8: $("#grade_name_8").val(),
                    grade_name_9: $("#grade_name_9").val(),
                    grade_name_10: $("#grade_name_10").val()
                },
                type: "POST"
            }).done(function (res) {
                console.log(res);
            });
        });
    });

</script>