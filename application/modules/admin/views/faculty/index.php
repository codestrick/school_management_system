<!-- Basic layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }
        ?>

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title text-bold"><?php echo ! empty($form_action) ? $form_action : ''; ?></h5>
                <div class="heading-elements">
                    <a href="<?php echo base_url('admin/faculty/add'); ?>" class="btn btn-primary" role="button">
                        <i class="fa fa-plus"></i>&nbsp;
                        Add Faculty
                    </a>
                </div>
            </div>

            <div class="panel-body">
                <?php echo $table; ?>
            </div>
        </div>

    </div>
</div>
<!-- /basic layout -->

<script type="text/javascript">
    var export_file_for = 'faculty';
</script>