<!-- Basic layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }

        if ( ! empty($validation_err))
        {
            ?>
            <div class="alert alert-danger alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
            <?php
        }
        ?>

        <!--jquery validation error container-->
        <div id="errorContainer" class="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p class="text-bold">Please correct the following errors and try again:</p>
            <ul></ul>
        </div>

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title text-bold"><?php echo ! empty($form_action) ? $form_action : ''; ?></h5>
                <div class="heading-elements">
                    <a href="<?php echo base_url('admin/faculty/list'); ?>" class="btn btn-primary">
                        <i class="fa fa-list"></i>&nbsp;
                        List Faculty
                    </a>
                </div>
            </div>

            <div class="panel-body">
                <form id="save_form" action="" method="post" class="form-horizontal">

                    <div class="form-group">
                        <label class="col-lg-2 control-label text-bold">Name: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="faculty_name" id="faculty_name"
                                   value="<?php echo ! empty($faculty_name) ? $faculty_name : $this->input->post('faculty_name'); ?>"
                                   placeholder="Name" required data-msg-required="Faculty Name Required"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label text-bold">E-mail: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="email" class="form-control" name="faculty_email" id="faculty_email"
                                   value="<?php echo ! empty($faculty_email) ? $faculty_email : $this->input->post('faculty_email'); ?>"
                                   placeholder="E-mail" required data-msg-required="Faculty E-mail Required"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label text-bold">Mobile: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="faculty_mobile" id="mobile"
                                   value="<?php echo ! empty($faculty_mobile) ? $faculty_mobile : $this->input->post('faculty_mobile'); ?>"
                                   placeholder="Mobile" required data-msg-required="Faculty Mobile Required"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label text-bold">Address: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <textarea class="form-control" name="faculty_address" id="faculty_address"
                                      placeholder="Address" required data-msg-required="Faculty Address Required"><?php
                                echo ! empty($faculty_address) ? $faculty_address : $this->input->post('faculty_address');
                                ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label text-bold">Subject: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <select class="form-control select2" name="subject_id" id="subject_id" required data-msg-required="Faculty Subject Required">
                                <option value="">Select Class</option><?php
                                if ( ! empty($subjects))
                                {
                                    foreach ($subjects as $key => $subject)
                                    {
                                        $selected = ! empty($subject_id) ? ($subject_id == $subject['id'] ? 'selected' : '') : '';

                                        echo '<option value="' . $subject['id'] . '" ' . $selected . '>' . $subject['subject_name'] . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="text-right col-lg-6">
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<!-- /basic layout -->