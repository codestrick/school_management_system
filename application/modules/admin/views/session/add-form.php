<!-- Basic layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }

        if ( ! empty($validation_err))
        {
            ?>
            <div class="alert alert-danger alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
            <?php
        }
        ?>

        <!--jquery validation error container-->
        <div id="errorContainer" class="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p class="text-bold">Please correct the following errors and try again:</p>
            <ul></ul>
        </div>

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title text-bold"><?php echo ! empty($form_action) ? $form_action : ''; ?></h5>
                <div class="heading-elements">
                    <a href="<?php echo base_url('admin/session/list'); ?>" class="btn btn-primary">
                        <i class="fa fa-list"></i>&nbsp;
                        List Session
                    </a>
                </div>
            </div>

            <div class="panel-body">
                <form id="save_form" action="" method="post" class="form-horizontal">

                    <div class="form-group">
                        <label class="col-lg-2 control-label text-bold">Start Year: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="ses_start_yr" id="ses_start_yr"
                                   value="<?php echo ! empty($ses_start_yr) ? $ses_start_yr : $this->input->post('ses_start_yr'); ?>"
                                   placeholder="Enter Start Year" required data-msg-required="Start Year Required"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-2 control-label text-bold">End Year: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="ses_end_yr" id="ses_end_yr"
                                   value="<?php echo ! empty($ses_end_yr) ? $ses_end_yr : $this->input->post('ses_end_yr'); ?>"
                                   placeholder="Enter End Year" required data-msg-required="End Year Required"/>
                        </div>
                    </div>

                    <div class="text-right col-lg-6">
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<!-- /basic layout -->