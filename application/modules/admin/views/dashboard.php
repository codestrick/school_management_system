<?php $user_auth_session = get_current_logged_in_session(); ?>

<!-- Message layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }
        ?>

    </div>
</div>
<!-- /Message layout -->

<!-- Dashboard layout-->
<div class="row">
    <div class="col-md-12">
        <div class="text-size-small text-uppercase text-semibold text-muted mb-10">DASHBOARD</div>
        <div class="panel panel-body">
            <h5 class="panel-title text-center text-bold text-uppercase">
                <?php echo ! empty($user_auth_session) ? 'Welcome ' . $user_auth_session['name'] . ' in the dashboard!' : 'Welcome in the dashboard!'; ?>
            </h5>
        </div>
    </div>
</div>
<!-- /Dashboard layout -->

<!-- Useful links layout -->
<div class="text-size-small text-uppercase text-semibold text-muted mb-10">USEFUL LINKS</div>
<div class="row">
    <div class="col-sm-8">
        <div class="panel panel-body">
            <div class="media">
                <a class="media-left">
                    <i class="icon-grid2" style="font-size: 40px !important; color: #37474F !important;"></i>
                </a>

                <div class="media-body">
                    <h6 class="media-heading text-black">Initial Information</h6>
                    <span class="text-muted text-black"><a href="<?php echo base_url('admin/session/list'); ?>">Session</a> |</span>
                    <span class="text-muted text-black"><a href="<?php echo base_url('admin/class/list'); ?>">Class</a> |</span>
                    <span class="text-muted text-black"><a href="<?php echo base_url('admin/section/list'); ?>">Section</a> |</span>
                    <span class="text-muted text-black"><a href="<?php echo base_url('admin/subject/list'); ?>">Subject</a> |</span>
                    <span class="text-muted text-black"><a href="<?php echo base_url('admin/letter-grade/list'); ?>">Letter Grade</a> |</span>
                    <span class="text-muted text-black"><a href="<?php echo base_url('admin/religion/list'); ?>">Religion</a> |</span>
                    <span class="text-muted text-black"><a href="<?php echo base_url('admin/cast/list'); ?>">Cast</a> |</span>
                    <span class="text-muted text-black"><a href="<?php echo base_url('admin/sub-cast/list'); ?>">Sub Cast</a> |</span>
                    <span class="text-muted text-black"><a href="<?php echo base_url('admin/occupation/list'); ?>">Occupation</a></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="panel panel-body">
            <div class="media">
                <a class="media-left">
                    <i class="fa fa-users" style="font-size: 40px !important; color: #37474F !important;"></i>
                </a>

                <div class="media-body">
                    <h6 class="media-heading text-black">Student</h6>
                    <span class="text-muted text-black"><a href="<?php echo base_url('admin/student/add'); ?>">Add Student</a> |</span>
                    <span class="text-muted text-black"><a href="<?php echo base_url('admin/student/list'); ?>">List Student</a></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Useful links layout -->