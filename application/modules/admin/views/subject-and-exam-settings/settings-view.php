<!-- Basic layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }

        if ( ! empty($validation_err))
        {
            ?>
            <div class="alert alert-danger alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
            <?php
        }
        ?>

        <!--jquery validation error container-->
        <div id="errorContainer" class="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p class="text-bold">Please correct the following errors and try again:</p>
            <ul></ul>
        </div>

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title text-bold"><?php echo ! empty($form_action) ? $form_action : ''; ?></h5>
                <div class="heading-elements"></div>
            </div>

            <div class="panel-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th class="text-bold">Subject Name</th>
                        <th class="text-bold text-center" style="width: 140px">Full Marks</th>
                        <th class="text-bold text-center" style="width: 140px">Pass Marks</th>
                        <th class="text-bold text-center" style="width: 230px">Subject Acceptable (Yes/No)</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(!empty($exam_settings_array))
                    {
                        foreach ($exam_settings_array as $unit => $subjects)
                        {
                            echo '<tr><td colspan="3" class="text-bold text-center" style="font-size: 15px">Unit - ' . $unit . '</td></tr>';

                            foreach ($subjects as $id => $value)
                            {
                                echo '<tr>';
                                echo '<td>' . $value['subject_name'] . '</td>';
                                echo '<td class="text-center">' . $value['subject_fm'] . '</td>';
                                echo '<td class="text-center">' . $value['pass_mark'] . '</td>';
                                echo '<td class="text-center">' . ucfirst($value['subject_addable']) . '</td>';
                                echo '</tr>';
                            }
                        }
                    }
                    else
                    {
                        echo '<tr><td colspan="3" class="text-center text-bold">No settings found.</td></tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
</div>
<!-- /basic layout -->