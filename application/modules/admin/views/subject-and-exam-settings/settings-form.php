<!-- Basic layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }

        if ( ! empty($validation_err))
        {
            ?>
            <div class="alert alert-danger alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
            <?php
        }
        ?>

        <!--jquery validation error container-->
        <div id="errorContainer" class="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p class="text-bold">Please correct the following errors and try again:</p>
            <ul></ul>
        </div>

        <form id="save_form" action="" method="post" class="form-horizontal">
            <input type="hidden" name="form_submit" value="form_submit">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-body">
                        <h5 class="panel-title text-bold text-uppercase">
                            <?php echo ! empty($form_action) ? $form_action : ''; ?>
                        </h5>
                        <div class="heading-elements">
                            <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <?php

            if ( ! empty($exam_settings_array))
            {
                foreach ($exam_settings_array as $k => $no_of_exam)
                {
                    $unit = 'Unit ' . $k;
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h5 class="panel-title text-bold"><?php echo ! empty($unit) ? $unit : ''; ?></h5>
                                    <div class="heading-elements"></div>
                                </div>

                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-2" style="margin-right: 30px;text-align: center">
                                                <h5><b>Subject</b></h5>
                                            </div>
                                            <div class="col-md-3" style="margin-right: 30px;text-align: center">
                                                <h5><b>Full Mark</b></h5>
                                            </div>
                                            <div class="col-md-3" style="margin-right: 30px;text-align: center">
                                                <h5><b>Pass Mark</b></h5>
                                            </div>
                                            <div class="col-md-2" style="text-align: center">
                                                <h5><b>Acceptable</b></h5>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                    <?php
                                    foreach ($no_of_exam as $key => $settings)
                                    {
                                        $selected = !empty($settings_det_previous) ? ($settings_det_previous['fm'][$k][$settings['subject_id']] == 'yes') ? 'selected' :'' : '';
                                        ?>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-2" style="margin-right: 30px">
                                                    <div class="form-group">
                                                        <p class="text-bold"><?php echo $settings['subject_name']; ?></p>
                                                    </div>
                                                </div>

                                                <div class="col-md-3" style="margin-right: 30px">
                                                    <div class="form-group">
                                                        <input type="number" min="0" class="form-control"
                                                               name="settings[fm][<?php echo $k ?>][<?php echo $settings['subject_id']; ?>]"
                                                               value="<?php echo ! empty($settings_det_previous) ? $settings_det_previous['fm'][$k][$settings['subject_id']] : 0; ?>"
                                                               placeholder="Enter Full Marks" autocomplete="off"
                                                               required data-msg-required="Full Marks Required"/>
                                                    </div>
                                                </div>

                                                <div class="col-md-3" style="margin-right: 30px">
                                                    <div class="form-group">
                                                        <input type="number" min="0" class="form-control"
                                                               name="settings[pass_mark][<?php echo $k ?>][<?php echo $settings['subject_id']; ?>]"
                                                               value="<?php echo ! empty($settings_det_previous) ? $settings_det_previous['pass_mark'][$k][$settings['subject_id']] : 0; ?>"
                                                               placeholder="Enter Full Marks" autocomplete="off"
                                                               required data-msg-required="Pass Marks Required"/>
                                                    </div>
                                                </div>

                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <select class="form-control select2" name="settings[acceptable][<?php echo $k ?>][<?php echo $settings['subject_id']; ?>]"
                                                                required data-msg-required="Subject Status Required">
                                                            <option value="">Select Subject Status</option>
                                                            <option value="yes" <?php echo !empty($settings_det_previous) ? ($settings_det_previous['acceptable'][$k][$settings['subject_id']] == 'yes') ? 'selected' :'' : ''; ?>>Yes</option>
                                                            <option value="no" <?php echo !empty($settings_det_previous) ? ($settings_det_previous['acceptable'][$k][$settings['subject_id']] == 'no') ? 'selected' :'' : ''; ?>>No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </form>
    </div>
</div>
<!-- /basic layout -->