<!-- Basic layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }

        if ( ! empty($validation_err))
        {
            ?>
            <div class="alert alert-danger alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
            <?php
        }
        ?>

        <!--jquery validation error container-->
        <div id="errorContainer" class="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p class="text-bold">Please correct the following errors and try again:</p>
            <ul></ul>
        </div>

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title text-bold"><?php echo ! empty($form_action) ? $form_action : ''; ?></h5>
                <div class="heading-elements">
                    <a href="<?php echo base_url('admin/class/list'); ?>" class="btn btn-primary">
                        <i class="fa fa-list"></i>&nbsp;
                        List Class
                    </a>
                </div>
            </div>

            <div class="panel-body">
                <form id="save_form" action="" method="post" class="form-horizontal">

                    <div class="form-group">
                        <label class="col-lg-2 control-label text-bold">Class Type: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <select class="form-control select2" name="class_type" id="class_type" required data-msg-required="Class Type Required">
                                <option value="">Select Class Type</option>
                                <option value="lower" <?php echo ! empty($class_type) ? ($class_type == 'lower') ? 'selected' : '' : ''; ?>>Lower (V - X)</option>
                                <option value="upper" <?php echo ! empty($class_type) ? ($class_type == 'upper') ? 'selected' : '' : ''; ?>>Upper (XI - XII)</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label text-bold">Class Name: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="class_name" id="class_name"
                                   value="<?php echo ! empty($class_name) ? $class_name : $this->input->post('class_name'); ?>"
                                   placeholder="Class Name" required data-msg-required="Class Name Required"/>
                        </div>
                    </div>

                    <div class="text-right col-lg-6">
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<!-- /basic layout -->