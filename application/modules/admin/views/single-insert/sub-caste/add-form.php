<!-- Basic layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }

        if ( ! empty($validation_err))
        {
            ?>
            <div class="alert alert-danger alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
            <?php
        }
        ?>

        <!--jquery validation error container-->
        <div id="errorContainer" class="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p class="text-bold">Please correct the following errors and try again:</p>
            <ul></ul>
        </div>

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title text-bold"><?php echo ! empty($form_action) ? $form_action : ''; ?></h5>
                <div class="heading-elements">
                    <a href="<?php echo base_url('admin/sub-caste/list'); ?>" class="btn btn-primary">
                        <i class="fa fa-list"></i>&nbsp;
                        List Sub Caste
                    </a>
                </div>
            </div>

            <div class="panel-body">
                <form id="save_form" action="" method="post" class="form-horizontal">

                    <div class="form-group">
                        <label class="col-lg-2 control-label text-bold">Cast Name: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <select class="form-control select2" name="caste_id" id="caste_id"
                                    required data-msg-required="Caste Required">
                                <option value="">Select Caste</option>
                                <?php
                                if ( ! empty($allCaste))
                                {
                                    foreach ($allCaste as $k => $caste)
                                    {

                                        if(!empty($caste_id))
                                        {
                                            $selected = ($caste['id'] == $caste_id) ? 'selected' : '';
                                        }else{
                                            $selected = '';
                                        }

                                        echo '<option value="'. $caste['id'].'"' . $selected . '>'.$caste['caste_name'].'</option>';

                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label text-bold">Sub Caste Name: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="sub_caste_name" id="sub_caste_name"
                                   value="<?php echo ! empty($sub_caste_name) ? $sub_caste_name : $this->input->post('sub_caste_name'); ?>"
                                   placeholder="Sub Caste Name" required data-msg-required="Sub Caste Name Required"/>
                        </div>
                    </div>

                    <div class="text-right col-lg-6">
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<!-- /basic layout -->