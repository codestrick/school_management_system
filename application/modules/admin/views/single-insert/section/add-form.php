<!-- Basic layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }

        if ( ! empty($validation_err))
        {
            ?>
            <div class="alert alert-danger alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
            <?php
        }
        ?>

        <!--jquery validation error container-->
        <div id="errorContainer" class="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p class="text-bold">Please correct the following errors and try again:</p>
            <ul></ul>
        </div>

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title text-bold"><?php echo ! empty($form_action) ? $form_action : ''; ?></h5>
                <div class="heading-elements">
                    <a href="<?php echo base_url('admin/section/list'); ?>" class="btn btn-primary">
                        <i class="fa fa-list"></i>&nbsp;
                        List Section
                    </a>
                </div>
            </div>

            <div class="panel-body">
                <form id="save_form" action="" method="post" class="form-horizontal">

                    <div class="form-group">
                        <label class="col-lg-2 control-label text-bold">Class Name: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <select class="form-control select2" name="class_id" id="class_id"
                                    required data-msg-required="Class Name Required">
                                <option value="">Select Class</option>
                                <?php
                                if (!empty($classes)) {
                                    foreach ($classes as $k => $value) {

                                        if(!empty($class_id))
                                        {
                                            $selected = ($value['id'] == $class_id) ? 'selected' : '';
                                        }else{
                                            $selected = '';
                                        }

                                        echo '<option value="'.$value['id'].'"' . $selected . '>'.$value['class_name'].'</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label text-bold">Section Name: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="section_name" id="section_name"
                                   value="<?php echo !empty($section_name) ? $section_name : $this->input->post('section_name'); ?>"
                                   placeholder="Section Name" required data-msg-required="Section Name Required"/>
                        </div>
                    </div>

                    <div class="text-right col-lg-6">
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<!-- /basic layout -->=