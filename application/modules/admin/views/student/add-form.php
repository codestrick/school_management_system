<!-- Basic layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }

        if ( ! empty($validation_err))
        {
            ?>
            <div class="alert alert-danger alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
            <?php
        }
        ?>

        <!--jquery validation error container-->
        <div id="errorContainer" class="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p class="text-bold">Please correct the following errors and try again:</p>
            <ul></ul>
        </div>

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title text-bold"><?php echo ! empty($form_action) ? $form_action : ''; ?></h5>
                <div class="heading-elements">
                    <a href="<?php echo base_url('admin/student/list'); ?>" class="btn btn-primary">
                        <i class="fa fa-list"></i>&nbsp;
                        List Student
                    </a>
                </div>
            </div>

            <div class="panel-body">
                <form action="" method="post" class="steps-validation">

                    <h6>Student Information</h6>
                    <hr>
                    <fieldset>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Student Name: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="std_name" id="std_name"
                                           value="" autocomplete="off"
                                           placeholder="Student Name" data-msg-required="Student Name Required"/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Date of Birth: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="dob" id="dob"
                                           value="" autocomplete="off"
                                           placeholder="Date Of Birth" data-msg-required="Date Of Birth Required"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Age: <span class="text-danger">*</span></label>
                                    <input type="number" class="form-control required" name="age" id="age" min="10"
                                           value="" autocomplete="off" maxlength="2" minlength="2" min="10"
                                           placeholder="Age" data-msg-required="Age Required"/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Phone Number: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="phone_no" id="phone_no"
                                           value="" autocomplete="off" maxlength="10" minlength="10"
                                           placeholder="Phone Number" data-msg-required="Phone Number Required"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Gender: <span class="text-danger">*</span></label>
                                    <select class="form-control required select-2" name="gender" id="gender" data-msg-required="Gender Required">
                                        <option value="">Select Gender</option>
                                        <option value="m">Male</option>
                                        <option value="f">Female</option>
                                        <option value="o">Other</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Religion: <span class="text-danger">*</span></label>
                                    <select class="form-control required select-2" name="religion_id" id="religion_id" data-msg-required="Religion Required">
                                        <option value="">Select Religion</option>
                                        <?php
                                        if ( ! empty($allReligions))
                                        {
                                            foreach ($allReligions as $k => $religion)
                                            {
                                                $selected = ! empty($religion_id) ? ($religion_id == $religion['id'] ? 'selected' : '') : '';
                                                ?>
                                                <option value="<?php echo $religion['id']; ?>" <?php echo $selected; ?>><?php echo $religion['religion_name']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Caste: <span class="text-danger">*</span></label>
                                    <select class="form-control required select-2" name="caste_id" id="caste_id" data-msg-required="Caste Required">
                                        <option value="">Select Caste</option>
                                        <?php
                                        if ( ! empty($allCaste))
                                        {
                                            foreach ($allCaste as $k => $caste)
                                            {
                                                $selected = ! empty($caste_id) ? ($caste_id == $caste['id'] ? 'selected' : '') : '';
                                                ?>
                                                <option value="<?php echo $caste['id']; ?>" <?php echo $selected; ?>><?php echo $caste['caste_name']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Sub Caste: <span class="text-danger">*</span></label>
                                    <select class="form-control select-2" name="sub_caste_id" id="sub_caste_id"
                                            data-msg-required="Sub Caste Required">
                                        <option value="">Select Sub Caste</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                    <h6>Guardian's Information</h6>
                    <fieldset>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Father's Name: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="fathers_name" id="fathers_name"
                                           value="" autocomplete="off"
                                           placeholder="Father's Name" data-msg-required="Father's Name Required"/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Father's Occupation: <span class="text-danger">*</span></label>
                                    <select class="form-control select-2 required" name="fathers_occupation_id" id="fathers_occupation_id"
                                            data-msg-required="Father's Occupation Required">
                                        <option value="">Select Father's Occupation</option>
                                        <?php
                                        if ( ! empty($allOccupations))
                                        {
                                            foreach ($allOccupations as $k => $occupation)
                                            {
                                                ?>
                                                <option value="<?php echo $occupation['id'] ?>"><?php echo $occupation['occupation_name']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Mother's Name: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="mothers_name" id="mothers_name"
                                           value=""
                                           placeholder="Mother's Name" data-msg-required="Mother's Name Required"/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Mother's Occupation: <span class="text-danger">*</span></label>
                                    <select class="form-control select-2 required" name="mothers_occupation_id" id="mothers_occupation_id"
                                            data-msg-required="Mother's Occupation Required">
                                        <option value="">Select Mother's Occupation</option>
                                        <?php
                                        if ( ! empty($allOccupations))
                                        {
                                            foreach ($allOccupations as $k => $occupation)
                                            {
                                                ?>
                                                <option value="<?php echo $occupation['id'] ?>"><?php echo $occupation['occupation_name']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Guardian's Name: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="guardians_name" id="guardians_name"
                                           value=""
                                           placeholder="Guardian's Name" data-msg-required="Guardian's Name Required"/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-bold">Guardian's Occupation: <span class="text-danger">*</span></label>
                                            <select class="form-control select-2 required" name="guardians_occupation_id" id="guardians_occupation_id"
                                                    data-msg-required="Guardian's Occupation Required">
                                                <option value="">Select Mother's Occupation</option>
                                                <?php
                                                if ( ! empty($allOccupations))
                                                {
                                                    foreach ($allOccupations as $k => $occupation)
                                                    {
                                                        ?>
                                                        <option value="<?php echo $occupation['id'] ?>"><?php echo $occupation['occupation_name']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-bold">Relation with Guardian's <span class="text-danger">*</span></label>
                                            <select class="form-control select-2 required" name="guardians_relation" id="guardians_relation"
                                                    data-msg-required="Relation with Guardian's Required">
                                                <option value="">Select Guardian's Relation</option>
                                                <option value="Father">Father</option>
                                                <option value="Mother">Mother</option>
                                                <option value="Other">Other</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <h6>Address Information</h6>
                    <fieldset>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="text-bold">Permanent Address <span class="text-danger">*</span></label>
                                <div class="form-group">
                                    <label class="text-bold">State: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required"
                                           name="permanent_state" id="permanent_state" value=""
                                           placeholder="State" data-msg-required="State Required"/>
                                </div>
                                <div class="form-group">
                                    <label class="text-bold">District: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="permanent_district"
                                           id="permanent_district" value=""
                                           placeholder="District" data-msg-required="District Required"/>
                                </div>
                                <div class="form-group">
                                    <label class="text-bold">Location: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="permanent_location"
                                           id="permanent_location" value=""
                                           placeholder="Location" data-msg-required="Location Required"/>
                                </div>
                                <div class="form-group">
                                    <label class="text-bold">Pin Code: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required"
                                           name="permanent_pincode" id="permanent_pincode"
                                           value="" autocomplete="off" maxlength="6" minlength="6"
                                           placeholder="Pin Code" data-msg-required="Pin Code Required"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label class="text-bold" style="">Correspondence Address <span class="text-danger">*</span></label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="text-bold" style=""><input type="checkbox" name="useAddress" id="useAddress"> Same as permanent address</label>

                                <div class="form-group">
                                    <label class="text-bold">State: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="correspondence_state"
                                           id="correspondence_state" value=""
                                           placeholder="State" data-msg-required="State Required"/>
                                </div>
                                <div class="form-group">
                                    <label class="text-bold">District: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="correspondence_district"
                                           id="correspondence_district" value=""
                                           placeholder="District" data-msg-required="District Required"/>
                                </div>
                                <div class="form-group">
                                    <label class="text-bold">Location: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required"
                                           name="correspondence_location" id="correspondence_location"
                                           value="" placeholder="Location"
                                           data-msg-required="Location Required"/>
                                </div>
                                <div class="form-group">
                                    <label class="text-bold">Pin Code: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required"
                                           name="correspondence_pincode" id="correspondence_pincode"
                                           value="" placeholder="Pin Code" maxlength="6" minlength="6"
                                           data-msg-required="Pin Code Required"/>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <h6>Admission Information</h6>
                    <fieldset>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Class: <span class="text-danger">*</span></label>
                                    <select class="form-control select-2 required" name="admission_class_id" id="admission_class_id" data-msg-required="Class Required">
                                        <option value="">Select Class</option><?php
                                        if ( ! empty($allClass))
                                        {
                                            foreach ($allClass as $class_type => $class)
                                            {
                                                $selected = ! empty($admission_class_id) ? ($admission_class_id == $class['id'] ? 'selected' : '') : '';

                                                echo '<optgroup label="' . ucfirst($class_type) . '">';

                                                foreach ($class as $ke => $val)
                                                {
                                                    echo '<option value="' . $val['id'] . '">' . $val['class_name'] . '</option>';
                                                }

                                                echo '</optgroup>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Section: <span class="text-danger">*</span></label>
                                    <select class="form-control required select-2" name="section_id" id="section_id" data-msg-required="Section Required">
                                        <option value="">Select Section</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Subject Type: <span class="text-danger">*</span></label>
                                    <select class="form-control required select-2" name="subject_type" id="subject_type" data-msg-required="Subject Type Required">
                                        <option value="">Select Subject Type</option>
                                        <option value="All">All</option>
                                        <option value="Arts">Arts</option>
                                        <option value="Commerce">Commerce</option>
                                        <option value="Other">Others</option>
                                        <option value="Science">Science</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Subject Combination Name: <span class="text-danger">*</span></label>
                                    <select class="form-control required select-2" name="subject_combination_name" id="subject_combination_name"
                                            data-msg-required="Subject Combination Required">
                                        <option value="">Select Subject Combination</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- /basic layout -->