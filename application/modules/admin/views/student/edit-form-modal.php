<button id="student_edit_modal_form_button" type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#student_edit_modal_form_vertical"
        style="display: none">Launch
</button>
<!-- Vertical form modal -->
<div id="student_edit_modal_form_vertical" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title">Choose an Operation</h2>
            </div>

            <form id="student_operation">
                <div class="modal-body">
                    <!--jquery validation error container-->
                    <div id="errorContainer" class="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p class="text-bold">Please correct the following errors and try again:</p>
                        <ul></ul>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="student_id" id="student_id" class="form-control" value="">
                        <label class="col-lg-3 control-label text-bold">Operation Type <span class="text-danger">*</span></label>
                        <div class="col-lg-6">
                            <select class="form-control select-2" name="operation_type" id="operation_type" required data-msg-required="Operation Type Required">
                                <option value="">Select Operation Type</option>
                                <option value="edit_student">Update Student Details</option>
                                <option value="student_upgradetion">Student Upgradetion</option>
                                <option value="re_admission">Readmission</option>
                            </select>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /vertical form modal -->

<script type="text/javascript">
    $(document).ready(function () {

        if($('select').hasClass('select-2'))
        {
            $('select.select-2').select2();
        }

        $("#student_operation").validate({
            ignore: [],
            errorContainer: $('#errorContainer'),
            errorLabelContainer: $('#errorContainer ul'),
            wrapper: 'li',
            onfocusout: false,
            highlight: function (element, errorClass) {
                if ($(element).hasClass('select-2'))
                {
                    $(element).next('.select2-container').addClass(errorClass);
                }
                else
                {
                    $(element).addClass(errorClass);
                }
            },
            unhighlight: function (element, errorClass) {
                if ($(element).hasClass('select-2'))
                {
                    $(element).next('.select2-container').removeClass(errorClass);
                }
                else
                {
                    $(element).removeClass(errorClass);
                }
            },
            submitHandler: function (form) {

                if($(form).valid()) {

                    var operation = $('#operation_type').val();
                    var student_id = $('#student_id').val();

                    if(operation == 'edit_student'){
                        window.location.href = base_url + 'admin/student/edit/' + student_id;
                    } else if(operation == 'student_upgradetion') {
                        window.location.href = base_url + 'admin/student/upgradetion/' + student_id;
                    } else if(operation == 're_admission') {
                        window.location.href = base_url + 'admin/student/re-admission/' + student_id;
                    } else {
                        bootbox.alert({message: 'Invalid operation performed!'});
                    }
                }
            }
        });
    });
</script>