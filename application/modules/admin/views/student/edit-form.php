<!-- Basic layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }

        if ( ! empty($validation_err))
        {
            ?>
            <div class="alert alert-danger alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
            <?php
        }
        ?>

        <!--jquery validation error container-->
        <div id="errorContainer" class="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p class="text-bold">Please correct the following errors and try again:</p>
            <ul></ul>
        </div>

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title text-bold"><?php echo ! empty($form_action) ? $form_action : ''; ?></h5>
                <div class="heading-elements">
                    <a href="<?php echo base_url('admin/student/list'); ?>" class="btn btn-primary">
                        <i class="fa fa-list"></i>&nbsp;
                        List Student
                    </a>
                </div>
            </div>

            <div class="panel-body">
                <form action="" method="post" class="steps-validation">

                    <h6>Student Information</h6>
                    <hr>
                    <fieldset>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Student Name: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="std_name" id="std_name"
                                           value="<?php echo ! empty($student_name) ? $student_name : ''; ?>" autocomplete="off"
                                           placeholder="Student Name" data-msg-required="Student Name Required"/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Date of Birth: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="dob" id="dob"
                                           value="<?php echo ! empty($dob) ? $dob : ''; ?>" autocomplete="off"
                                           placeholder="Date Of Birth" data-msg-required="Date Of Birth Required"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Age: <span class="text-danger">*</span></label>
                                    <input type="number" class="form-control required" name="age" id="age" min="10"
                                           value="<?php echo ! empty($age) ? $age : ''; ?>" autocomplete="off" maxlength="2" minlength="2" min="10"
                                           placeholder="Age" data-msg-required="Age Required"/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Phone Number: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="phone_no" id="phone_no"
                                           value="<?php echo ! empty($phone_no) ? $phone_no : ''; ?>" autocomplete="off" maxlength="10" minlength="10"
                                           placeholder="Phone Number" data-msg-required="Phone Number Required"/>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Gender: <span class="text-danger">*</span></label>
                                    <select class="form-control required select-2" name="gender" id="gender" data-msg-required="Gender Required">
                                        <option value="">Select Gender</option>
                                        <option value="m" <?php echo ! empty($gender) && $gender == 'm' ? 'selected' : ''; ?> >Male</option>
                                        <option value="f" <?php echo ! empty($gender) && $gender == 'f' ? 'selected' : ''; ?> >Female</option>
                                        <option value="o" <?php echo ! empty($gender) && $gender == 'o' ? 'selected' : ''; ?> >Others</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Religion: <span class="text-danger">*</span></label>
                                    <select class="form-control required select-2" name="religion_id" id="religion_id" data-msg-required="Religion Required">
                                        <option value="">Select Religion</option>
                                        <?php
                                        if ( ! empty($allReligions))
                                        {
                                            foreach ($allReligions as $k => $religion)
                                            {
                                                $selected = ! empty($religion_id) ? ($religion_id == $religion['id'] ? 'selected' : '') : '';
                                                ?>
                                                <option value="<?php echo $religion['id']; ?>" <?php echo $selected; ?>><?php echo $religion['religion_name']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Caste: <span class="text-danger">*</span></label>
                                    <select class="form-control required select-2" name="caste_id" id="caste_id" data-msg-required="Caste Required">
                                        <option value="">Select Caste</option>
                                        <?php
                                        if ( ! empty($allCaste))
                                        {
                                            foreach ($allCaste as $k => $caste)
                                            {
                                                $selected = ! empty($caste_id) ? ($caste_id == $caste['id'] ? 'selected' : '') : '';
                                                ?>
                                                <option value="<?php echo $caste['id']; ?>" <?php echo $selected; ?>><?php echo $caste['caste_name']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Sub Caste: <span class="text-danger">*</span></label>
                                    <select class="form-control select-2" name="sub_caste_id" id="sub_caste_id"
                                            data-msg-required="Sub Caste Required" data-sub-caste-id="<?php echo ! empty($sub_caste_id) ? $sub_caste_id : ''; ?>" >
                                        <option value="">Select Sub Caste</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                    <h6>Guardian's Information</h6>
                    <input type="hidden" name="guardians_id" id="guardians_id" value="<?php echo ! empty($guardians_id) ? $guardians_id : ''; ?>">
                    <fieldset>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Father's Name: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="fathers_name" id="fathers_name"
                                           value="<?php echo ! empty($fathers_name) ? $fathers_name : ''; ?>" autocomplete="off"
                                           placeholder="Father's Name" data-msg-required="Father's Name Required"/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Father's Occupation: <span class="text-danger">*</span></label>
                                    <select class="form-control select-2 required" name="fathers_occupation_id" id="fathers_occupation_id"
                                            data-msg-required="Father's Occupation Required">
                                        <option value="">Select Father's Occupation</option>
                                        <?php
                                        if ( ! empty($allOccupations))
                                        {
                                            foreach ($allOccupations as $k => $occupation)
                                            {
                                                $selected = ! empty($fathers_occupation_id) ? ($fathers_occupation_id == $occupation['id'] ? 'selected' : '') : '';
                                                ?>
                                                <option value="<?php echo $occupation['id'] ?>" <?php echo $selected; ?> ><?php echo $occupation['occupation_name']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Mother's Name: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="mothers_name" id="mothers_name"
                                           value="<?php echo ! empty($mothers_name) ? $mothers_name : ''; ?>"
                                           placeholder="Mother's Name" data-msg-required="Mother's Name Required"/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Mother's Occupation: <span class="text-danger">*</span></label>
                                    <select class="form-control select-2 required" name="mothers_occupation_id" id="mothers_occupation_id"
                                            data-msg-required="Mother's Occupation Required">
                                        <option value="">Select Mother's Occupation</option>
                                        <?php
                                        if ( ! empty($allOccupations))
                                        {
                                            foreach ($allOccupations as $k => $occupation)
                                            {
                                                $selected = ! empty($mothers_occupation_id) ? ($mothers_occupation_id == $occupation['id'] ? 'selected' : '') : '';
                                                ?>
                                                <option value="<?php echo $occupation['id'] ?>" <?php echo $selected; ?> ><?php echo $occupation['occupation_name']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Guardian's Name: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="guardians_name" id="guardians_name"
                                           value="<?php echo ! empty($guardians_name) ? $guardians_name : ''; ?>"
                                           placeholder="Guardian's Name" data-msg-required="Guardian's Name Required"/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-bold">Guardian's Occupation: <span class="text-danger">*</span></label>
                                            <select class="form-control select-2 required" name="guardians_occupation_id" id="guardians_occupation_id"
                                                    data-msg-required="Guardian's Occupation Required">
                                                <option value="">Select Mother's Occupation</option>
                                                <?php
                                                if ( ! empty($allOccupations))
                                                {
                                                    foreach ($allOccupations as $k => $occupation)
                                                    {
                                                        $selected = ! empty($guardians_occupation_id) ? ($guardians_occupation_id == $occupation['id'] ? 'selected' : '') : '';
                                                        ?>
                                                        <option value="<?php echo $occupation['id'] ?>" <?php echo $selected; ?> ><?php echo $occupation['occupation_name']; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="text-bold">Relation with Guardian's <span class="text-danger">*</span></label>
                                            <select class="form-control select-2 required" name="guardians_relation" id="guardians_relation"
                                                    data-msg-required="Relation with Guardian's Required">
                                                <option value="">Select Guardian's Relation</option>
                                                <option value="Father" <?php echo ! empty($guardians_relation) && $guardians_relation == 'Father' ? 'selected' : ''; ?> >
                                                    Father
                                                </option>
                                                <option value="Mother" <?php echo ! empty($guardians_relation) && $guardians_relation == 'Mother' ? 'selected' : ''; ?> >
                                                    Mother
                                                </option>
                                                <option value="Other" <?php echo ! empty($guardians_relation) && $guardians_relation == 'Other' ? 'selected' : ''; ?> >Other
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <h6>Address Information</h6>
                    <fieldset>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="text-bold">Permanent Address <span class="text-danger">*</span></label>
                                <div class="form-group">
                                    <label class="text-bold">State: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required"
                                           name="permanent_state" id="permanent_state" value="<?php echo ! empty($permanent_state) ? $permanent_state : ''; ?>"
                                           placeholder="State" data-msg-required="State Required"/>
                                </div>
                                <div class="form-group">
                                    <label class="text-bold">District: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="permanent_district"
                                           id="permanent_district" value="<?php echo ! empty($permanent_district) ? $permanent_district : ''; ?>"
                                           placeholder="District" data-msg-required="District Required"/>
                                </div>
                                <div class="form-group">
                                    <label class="text-bold">Location: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="permanent_location"
                                           id="permanent_location" value="<?php echo ! empty($permanent_location) ? $permanent_location : ''; ?>"
                                           placeholder="Location" data-msg-required="Location Required"/>
                                </div>
                                <div class="form-group">
                                    <label class="text-bold">Pin Code: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required"
                                           name="permanent_pincode" id="permanent_pincode"
                                           value="<?php echo ! empty($permanent_pincode) ? $permanent_pincode : ''; ?>" autocomplete="off" maxlength="6" minlength="6"
                                           placeholder="Pin Code" data-msg-required="Pin Code Required"/>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label class="text-bold" style="">Correspondence Address <span class="text-danger">*</span></label>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="text-bold" style=""><input type="checkbox" name="useAddress" id="useAddress" <?php echo !empty($same_address) ? $same_address : ''; ?>> Same as permanent address</label>
                                <div class="form-group">
                                    <label class="text-bold">State: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required" name="correspondence_state"
                                           id="correspondence_state" value="<?php echo ! empty($correspondence_state) ? $correspondence_state : ''; ?>"
                                           placeholder="State" data-msg-required="State Required"/>
                                </div>
                                <div class="form-group">
                                    <label class="text-bold">District: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required"
                                           name="correspondence_district" id="correspondence_district"
                                           value="<?php echo ! empty($correspondence_district) ? $correspondence_district : ''; ?>" placeholder="District"
                                           data-msg-required="District Required"/>
                                </div>
                                <div class="form-group">
                                    <label class="text-bold">Location: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required"
                                           name="correspondence_location" id="correspondence_location"
                                           value="<?php echo ! empty($correspondence_location) ? $correspondence_location : ''; ?>" placeholder="Location"
                                           data-msg-required="Location Required"/>
                                </div>
                                <div class="form-group">
                                    <label class="text-bold">Pin Code: <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control required"
                                           name="correspondence_pincode" id="correspondence_pincode"
                                           value="<?php echo ! empty($correspondence_pincode) ? $correspondence_pincode : ''; ?>" placeholder="Pin Code" maxlength="6" minlength="6"
                                           data-msg-required="Pin Code Required"/>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                    <h6>Admission Information</h6>
                    <fieldset>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Class: <span class="text-danger">*</span></label>
                                    <select class="form-control select-2 required" name="admission_class_id" id="admission_class_id" data-msg-required="Class Required">
                                        <option value="">Select Class</option><?php
                                        if ( ! empty($allClass))
                                        {
                                            foreach ($allClass as $class_type => $class)
                                            {
                                                echo '<optgroup label="' . ucfirst($class_type) . '">';

                                                foreach ($class as $ke => $val)
                                                {
                                                    $selected = ! empty($admission_class_id) ? ($admission_class_id == $val['id'] ? 'selected' : '') : '';

                                                    echo '<option value="' . $val['id'] . '" ' . $selected . ' >' . $val['class_name'] . '</option>';
                                                }

                                                echo '</optgroup>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Section: <span class="text-danger">*</span></label>
                                    <select class="form-control required select-2" name="section_id" id="section_id" data-msg-required="Section Required" data-section-id="<?php echo !empty($section_id) ? $section_id : ''; ?>">
                                        <option value="">Select Section</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Subject Type: <span class="text-danger">*</span></label>
                                    <select class="form-control required select-2" name="subject_type" id="subject_type" data-msg-required="Subject Type Required">
                                        <option value="">Select Subject Type</option>
                                        <option value="All" <?php echo !empty($subject_type) && $subject_type == 'All' ? 'selected' : ''; ?>>All</option>
                                        <option value="Arts" <?php echo !empty($subject_type) && $subject_type == 'Arts' ? 'selected' : ''; ?>>Arts</option>
                                        <option value="Commerce" <?php echo !empty($subject_type) && $subject_type == 'Commerce' ? 'selected' : ''; ?>>Commerce</option>
                                        <option value="Other" <?php echo !empty($subject_type) && $subject_type == 'Other' ? 'selected' : ''; ?>>Others</option>
                                        <option value="Science" <?php echo !empty($subject_type) && $subject_type == 'Science' ? 'selected' : ''; ?>>Science</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="text-bold">Subject Combination Name: <span class="text-danger">*</span></label>
                                    <select class="form-control required select-2" name="subject_combination_name" id="subject_combination_name"
                                            data-msg-required="Subject Combination Required" data-subject-combination="<?php echo !empty($subject_combination_id)? $subject_combination_id : ''; ?>">
                                        <option value="">Select Subject Combination</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- /basic layout -->

<script type="text/javascript">
    var student_id = '<?php echo !empty($student_id) ? $student_id : ''; ?>';
</script>