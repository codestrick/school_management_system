<link rel="stylesheet" type="text/css" media="print" href="<?php echo base_url('assets/css/admin/print.css') ?>"/>
<!-- Invoice template -->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }
        ?>

        <div class="panel panel-white">
            <div class="panel-heading no-margin-bottom">
                <h6 class="panel-title text-bold">Student Details</h6>
                <div class="heading-elements">
                    <button type="button" class="btn btn-primary print-button" onclick="window.print();"><i class="icon-printer position-left"></i> Print</button>
                    <a href="<?php echo base_url('admin/student/list'); ?>" class="btn btn-primary student-list-button">
                        <i class="fa fa-list"></i>&nbsp;
                        List Student
                    </a>
                </div>
            </div>

            <div class="panel-body no-padding-bottom">
                <div class="row">
                    <div class="col-sm-6 content-group">
                        <img src="<?php echo base_url('/assets/images/user_icon.png'); ?>" class="content-group mt-10" alt="" style="width: 120px;">
                    </div>

                    <div class="col-sm-6 content-group">
                        <div class="invoice-details">
                            <h5 class="text-uppercase"><span class="text-semibold">Registration ID: </span><?php echo !empty($reg_id) ? $reg_id : ''; ?></h5>
                            <ul class="list-condensed list-unstyled text-semibold">
                                <li></li>
                                <li></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-6 col-lg-9 content-group">
                        <span class="text-muted">Student Information</span>
                        <ul class="list-condensed list-unstyled">
                            <li><span class="text-semibold">Name: </span><?php echo !empty($std_name) ? $std_name : ''; ?></li>
                            <li><span class="text-semibold">DOB: </span><?php echo !empty($dob) ? $dob : ''; ?></li>
                            <li><span class="text-semibold">Age: </span><?php echo !empty($age) ? $age : ''; ?></li>
                            <li><span class="text-semibold">Gender: </span><?php echo !empty($gender) ? $gender : ''; ?></li>
                            <li><span class="text-semibold">Religion: </span><?php echo !empty($religion_name) ? $religion_name : ''; ?></li>
                            <li><span class="text-semibold">Caste: </span><?php echo !empty($caste_name) ? $caste_name . (!empty($sub_caste_name) ? ' | ' . $sub_caste_name : '') : ''; ?></li>
                            <li><span class="text-semibold">Phone: </span><?php echo !empty($phone_no) ? $phone_no : ''; ?></li>
                            <li><span class="text-semibold">Permanent Address: </span><?php echo !empty($permanent_address) ? str_replace('|', ', ', $permanent_address) : ''; ?></li>
                            <li><span class="text-semibold">Correspondence Address: </span><?php echo !empty($correspondence_address) ? str_replace('|', ', ', $correspondence_address) : ''; ?></li>
                        </ul>
                    </div>

                    <div class="col-md-6 col-lg-3 content-group">
                        <span class="text-muted">Guardian's Information</span>
                        <ul class="list-condensed list-unstyled">
                            <li><span class="text-semibold">Father's Name: </span><?php echo !empty($fathers_name) ? $fathers_name : ''; ?></li>
                            <li><span class="text-semibold">Father's Occupation: </span><?php echo !empty($fathers_occupation) ? $fathers_occupation : ''; ?></li>
                            <li><span class="text-semibold">Mother's Name: </span><?php echo !empty($mothers_name) ? $mothers_name : ''; ?></li>
                            <li><span class="text-semibold">Mother's Occupation: </span><?php echo !empty($mothers_occupation) ? $mothers_occupation : ''; ?></li>
                            <li><span class="text-semibold">Guardian's Name: </span><?php echo !empty($guardians_name) ? $guardians_name : ''; ?></li>
                            <li><span class="text-semibold">Guardian's Occupation: </span><?php echo !empty($guardians_occupation) ? $guardians_occupation : ''; ?></li>
                            <li><span class="text-semibold">Relation with Guardian's: </span><?php echo !empty($guardians_relation) ? $guardians_relation : ''; ?></li>
                            <li><span class="text-semibold">&nbsp;</span></li>
                            <li><span class="text-semibold">&nbsp;</span></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-lg">
                    <thead>
                    <tr>
                        <th colspan="5" class="text-center">Admission Information</th>
                    </tr><tr>
                        <th class="col-sm-1">Session</th>
                        <th class="col-sm-1">Class</th>
                        <th class="col-sm-1">Section</th>
                        <th class="col-sm-1">Subject Type</th>
                        <th class="col-sm-2">Subject Combination Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(!empty($student_extra_info))
                    {
                        foreach ($student_extra_info as $key => $info)
                        {
                            echo '<tr>';
                            echo '<td>'.$info['ses_start_yr'] . ' - ' .$info['ses_end_yr'] .'</td>';
                            echo '<td>'.$info['class_name'] .'</td>';
                            echo '<td>'.$info['section_name'] .'</td>';
                            echo '<td>'.$info['subject_type'] .'</td>';
                            echo '<td>'.$info['subject_combination_name'] .'</td>';
                            echo '</tr>';
                        }
                    }
                    else
                    {
                        echo '<tr>';
                        echo '<td colspan="5" class="text-center">No record found!</td>';
                        echo '</tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>

            <div class="panel-body">
<!--                <h6>Other information</h6>-->
<!--                <p class="text-muted">-->
<!--                    Thank you for using Limitless. This invoice can be paid via PayPal, Bank transfer, Skrill or Payoneer. Payment is due within 30 days from-->
<!--                    the date of delivery. Late payment is possible, but with with a fee of 10% per month. Company registered in England and Wales #6893003, registered office:-->
<!--                    3 Goodman Street, London E1 8BF, United Kingdom. Phone number: 888-555-2311-->
<!--                </p>-->
            </div>
        </div>
    </div>
</div>
<!-- /invoice template -->