<!-- Basic layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }

        if ( ! empty($validation_err))
        {
            ?>
            <div class="alert alert-danger alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
            <?php
        }
        ?>

        <!--jquery validation error container-->
        <div id="errorContainer" class="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p class="text-bold">Please correct the following errors and try again:</p>
            <ul></ul>
        </div>

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title text-bold"><?php echo ! empty($form_action) ? $form_action : ''; ?></h5>
                <div class="heading-elements">
                    <a href="<?php echo base_url('admin/student/list'); ?>" class="btn btn-primary">
                        <i class="fa fa-list"></i>&nbsp;
                        List Student
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <form id="save_form" method="post" action="<?php echo ! empty($form_url) ? $form_url : '#'; ?>">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="text-bold">Student Registration Number: <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="std_reg_no" id="std_reg_no"
                                       autocomplete="off" placeholder="Student Registration Number" required data-msg-required="Student Registration Number Required."/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" style="margin-top: 26px"><i class="fa fa-search"></i> Search Student</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /basic layout -->