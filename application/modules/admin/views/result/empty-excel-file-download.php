<!-- Basic layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }

        if ( ! empty($validation_err))
        {
            ?>
            <div class="alert alert-danger alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
            <?php
        }
        ?>

        <!--jquery validation error container-->
        <div id="errorContainer" class="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p class="text-bold">Please correct the following errors and try again:</p>
            <ul></ul>
        </div>

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title text-bold"><?php echo ! empty($form_action) ? $form_action : ''; ?></h5>
                <div class="heading-elements">

                </div>
            </div>

            <div class="panel-body">
                <form id="save_form" action="<?php echo current_url(); ?>" method="post" class="form-horizontal">

                    <div class="form-group">
                        <label class="col-lg-3 control-label text-bold">Class: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <select class="form-control" name="class_id" id="class_id"
                                    required data-msg-required="Class Required">
                                <option value="">Select Class</option>
                                <?php
                                if(!empty($allClass)){
                                    foreach($allClass as $k => $class){
                                        ?>
                                        <option value="<?php echo $class['id']; ?>" data-class-type="<?php echo $class['class_type']; ?>"><?php echo $class['class_name'] ?></option>
                                <?php
                                    }
                                }

                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label text-bold">Section: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <select class="form-control" name="section_id" id="section_id"
                                    required data-msg-required="Class Required">
                                <option value="">Select Section</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label text-bold">Subject Type: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <select class="form-control" name="subject_type" id="subject_type" required data-msg-required="Subject Type Required">
                                <option value="">Select Subject Type</option>
                                <option value="All">All</option>
                                <option value="Arts">Arts</option>
                                <option value="Commerce">Commerce</option>
                                <option value="Science">Science</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3 control-label text-bold">Subject Combination Name: <span class="text-danger">*</span></label>
                        <div class="col-lg-4">
                            <select class="form-control" name="subject_combination_name" id="subject_combination_id"
                                    required data-msg-required="Class Required">
                                <option value="">Select Subject Combination Name</option>
                            </select>
                        </div>
                    </div>

                    <div class="text-right col-lg-7">
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<!-- /basic layout -->