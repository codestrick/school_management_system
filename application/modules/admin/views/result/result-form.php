<!-- Basic layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }

        if ( ! empty($validation_err))
        {
            ?>
            <div class="alert alert-danger alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
            <?php
        }
        ?>

        <!--jquery validation error container-->
        <div id="errorContainer" class="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p class="text-bold">Please correct the following errors and try again:</p>
            <ul></ul>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-body">
                    <h5 class="panel-title text-bold pull-left">
                        <?php echo ! empty($form_action) ? $form_action : ''; ?>
                    </h5>
                    <a href="<?php echo base_url('admin/student/list'); ?>" class="btn btn-primary pull-right">
                        <i class="fa fa-list"></i>&nbsp;
                        List Student
                    </a>
                </div>
            </div>
        </div>

        <?php
        if ( ! empty($all_details))
        {
            foreach ($all_details as $k => $details)
            {
                $unit = 'Unit ' . $details['exam_number'];
                $unit_table_class = 'unit-' . $details['exam_number'] . '-table';
                $unit_button_class = 'unit-' . $details['exam_number'] . '-button';
                ?>

                <div class="row">
                    <div class="col-md-12">

                        <div class="panel panel-flat">
                            <div class="panel-heading">
                                <h5 class="panel-title text-bold"><?php echo ! empty($unit) ? $unit : ''; ?>
                                    <span class="pull-right">REG NO.: 7845789654</span>
                                </h5>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered <?php echo $unit_table_class; ?>">
                                    <thead>
                                    <tr>
                                        <th>Subject</th>
                                        <th>Full Marks</th>
                                        <th>Present Status</th>
                                        <th style="width: 25%">Marks</th>
                                        <th style="width: 30px">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($details['result_trans'] as $key => $val)
                                    {
                                        ?>
                                        <tr class="subject-row" data-subject-id="<?php echo $val['subject_id']; ?>">
                                            <td><?php echo $val['subject_name']; ?></td>
                                            <td>20</td>
                                            <td data-field="present_status"><?php echo ($val['present_status'] == 1) ? 'Present' : 'Absent'; ?></td>
                                            <td data-field="number" class="ind-number" data-id="<?php echo $val['id']; ?>"><?php echo ($val['number'] != 0) ? $val['number'] : '' ; ?></td>
                                            <td>
                                                <a class="button button-small edit" title="Edit">
                                                    <i class="fa fa-save" title="Save"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>

    </div>
</div>
<!-- /basic layout -->