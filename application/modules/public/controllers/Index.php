<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends My_Controller
{
    public $autoload = array(
        'model' => array('App_model'),
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function Index()
    {
        redirect(base_url('home'));
    }

    public function home()
    {
        $dataArray['page_title']  = 'Home';
        $dataArray['header_type'] = 'header_1';

        $dataArray['css_local'] = array('flexslider', 'owl.carousel');
        $dataArray['js_local']  = array('jquery.flexslider', 'owl.carousel', 'slider');

        $this->load->view('home', $dataArray);
    }

    public function about()
    {
        $dataArray['faculties'] = $this->App_model->get_details_by('faculty', [], 'result_array');

        $dataArray['page_title']  = 'About Us';
        $dataArray['header_type'] = 'header_2';

        $dataArray['css_local'] = array();
        $dataArray['js_local']  = array();

        $this->load->view('about', $dataArray);
    }

    public function gallery()
    {
        $dataArray['page_title']  = 'Gallery';
        $dataArray['header_type'] = 'header_2';

        $dataArray['css_local'] = array('lsb');
        $dataArray['js_local']  = array('lsb.min', 'gallery');

        $this->load->view('gallery', $dataArray);
    }

    public function contact()
    {
        $dataArray['page_title']  = 'Contact Us';
        $dataArray['header_type'] = 'header_2';

        $dataArray['css_local'] = array();
        $dataArray['js_local']  = array();

        $this->load->view('contact', $dataArray);
    }
}