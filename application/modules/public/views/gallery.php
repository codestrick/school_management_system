<div class="welcome">
    <div class="container">
        <h3 class="tittle-agileits-w3layouts">Our <span class="green-w3">Gallery</span></h3>
        <div class="w3layouts_gallery_grids">
            <div class="col-md-4 w3layouts_gallery_grid">
                <a href="<?php echo base_url(); ?>assets/public/images/g1.jpg" class="lsb-preview" data-lsb-group="header">
                    <div class="w3layouts_news_grid">
                        <img src="<?php echo base_url(); ?>assets/public/images/g1.jpg" alt=" " class="img-responsive">
                        <div class="w3layouts_news_grid_pos">
                            <div class="wthree_text"><h3>Scholar Vision</h3></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 w3layouts_gallery_grid">
                <a href="<?php echo base_url(); ?>assets/public/images/g2.jpg" class="lsb-preview" data-lsb-group="header">
                    <div class="w3layouts_news_grid">
                        <img src="<?php echo base_url(); ?>assets/public/images/g2.jpg" alt=" " class="img-responsive">
                        <div class="w3layouts_news_grid_pos">
                            <div class="wthree_text"><h3>Scholar Vision</h3></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 w3layouts_gallery_grid">
                <a href="<?php echo base_url(); ?>assets/public/images/g3.jpg" class="lsb-preview" data-lsb-group="header">
                    <div class="w3layouts_news_grid">
                        <img src="<?php echo base_url(); ?>assets/public/images/g3.jpg" alt=" " class="img-responsive">
                        <div class="w3layouts_news_grid_pos">
                            <div class="wthree_text"><h3>Scholar Vision</h3></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 w3layouts_gallery_grid">
                <a href="<?php echo base_url(); ?>assets/public/images/g4.jpg" class="lsb-preview" data-lsb-group="header">
                    <div class="w3layouts_news_grid">
                        <img src="<?php echo base_url(); ?>assets/public/images/g4.jpg" alt=" " class="img-responsive">
                        <div class="w3layouts_news_grid_pos">
                            <div class="wthree_text"><h3>Scholar Vision</h3></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 w3layouts_gallery_grid">
                <a href="<?php echo base_url(); ?>assets/public/images/g5.jpg" class="lsb-preview" data-lsb-group="header">
                    <div class="w3layouts_news_grid">
                        <img src="<?php echo base_url(); ?>assets/public/images/g5.jpg" alt=" " class="img-responsive">
                        <div class="w3layouts_news_grid_pos">
                            <div class="wthree_text"><h3>Scholar Vision</h3></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 w3layouts_gallery_grid">
                <a href="<?php echo base_url(); ?>assets/public/images/g6.jpg" class="lsb-preview" data-lsb-group="header">
                    <div class="w3layouts_news_grid">
                        <img src="<?php echo base_url(); ?>assets/public/images/g6.jpg" alt=" " class="img-responsive">
                        <div class="w3layouts_news_grid_pos">
                            <div class="wthree_text"><h3>Scholar Vision</h3></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 w3layouts_gallery_grid">
                <a href="<?php echo base_url(); ?>assets/public/images/g7.jpg" class="lsb-preview" data-lsb-group="header">
                    <div class="w3layouts_news_grid">
                        <img src="<?php echo base_url(); ?>assets/public/images/g7.jpg" alt=" " class="img-responsive">
                        <div class="w3layouts_news_grid_pos">
                            <div class="wthree_text"><h3>Scholar Vision</h3></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 w3layouts_gallery_grid">
                <a href="<?php echo base_url(); ?>assets/public/images/g8.jpg" class="lsb-preview" data-lsb-group="header">
                    <div class="w3layouts_news_grid">
                        <img src="<?php echo base_url(); ?>assets/public/images/g8.jpg" alt=" " class="img-responsive">
                        <div class="w3layouts_news_grid_pos">
                            <div class="wthree_text"><h3>Scholar Vision</h3></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-4 w3layouts_gallery_grid">
                <a href="<?php echo base_url(); ?>assets/public/images/g9.jpg" class="lsb-preview" data-lsb-group="header">
                    <div class="w3layouts_news_grid">
                        <img src="<?php echo base_url(); ?>assets/public/images/g9.jpg" alt=" " class="img-responsive">
                        <div class="w3layouts_news_grid_pos">
                            <div class="wthree_text"><h3>Scholar Vision</h3></div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>