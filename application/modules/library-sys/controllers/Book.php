<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Book extends My_Controller
{

    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('Book_model','Author_model','Publisher_model'),
    );
    /**
     * Book constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $book_id
     */
    public function add($book_id = NULL)
    {
        $this->form_validation->set_rules('book_name', 'Book Name', "trim|required|unique[book.book_name.id.{$book_id}]");
        $this->form_validation->set_rules('author_id', 'Author Name', "trim|required");
        $this->form_validation->set_rules('publisher_id', 'Publisher Name', "trim|required");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($book_id))
            {
                $book_details = $this->Book_model->get_book_detail_by(['id' => $book_id]);

                if ( ! empty($book_details))
                {
                    $dataArray = array(
                        'book_name' => $book_details['book_name'],
                        'author_id' => $book_details['author_id'],
                        'publisher_id' => $book_details['publisher_id'],
                    );

                    $dataArray['form_action'] = 'Edit Book';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('lib/book/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Book';
            }

            $dataArray['css_local'] = array();
            $dataArray['js_local']  = array();

            $authors = $this->Author_model->get_author_detail_by([],'result_array');
            $publishers = $this->Publisher_model->get_publisher_detail_by([],'result_array');

            $dataArray['authors'] = !empty($authors) ? $authors : [];
            $dataArray['publishers'] = !empty($publishers) ? $publishers : [];


            $dataArray['page_title'] = 'Book';
            $dataArray['breadcrumb'] = 'Initial Information / Book';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('book/add-form', $dataArray);
        }
        else
        {
            $params = array(
                'book_name' => $this->input->post('book_name'),
                'author_id' => $this->input->post('author_id'),
                'publisher_id' => $this->input->post('publisher_id'),
            );

            if ( ! empty($book_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");


                $new_book_id = $this->Book_model->save_book($params, $book_id);
            }
            else
            {
                $params['created_at'] = date("Y-m-d H:i:s");
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_book_id = $this->Book_model->save_book($params);
            }

            if ( ! empty($new_book_id))
            {
                $this->session->set_flashdata('flash_message', (empty($book_id)) ? 'Book created successfully' : 'Book updated successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('lib/book/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('lib/book/list');
            }
        }
    }

    /**
     *
     */
    public function index()
    {
        $listing_headers = 'book_listing_headers';

        $data['source']          = site_url('library-sys/book/listBook_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Book';
        $data['page_title']      = 'Book';
        $data['breadcrumb']      = 'Initial Information / Book';

        $dataArray = $this->_table_listing($data);

        $this->load->view('book/index', $dataArray);
    }

    /**
     *
     */
    public function listBook_Json()
    {
        $listing_headers = 'book_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Book_model->tbl_name = 'book';

        $this->Book_model->join_tbl_name1 = 'author';
        $this->Book_model->join_tbl_name2 = 'publisher';

        $this->Book_model->join_cond1 = "{$this->Book_model->tbl_name}.author_id = {$this->Book_model->join_tbl_name1}.id";
        $this->Book_model->join_cond2 = "{$this->Book_model->tbl_name}.publisher_id = {$this->Book_model->join_tbl_name2}.id";

		
        $this->Book_model->select_db_cols = "book.id, book.book_name, book.author_id, book.publisher_id, book.created_at, book.updated_at,
        author.id as author_id, author.author_name,
        publisher.id as publisher_id, publisher.publisher_name";


        $this->Book_model->list_search_key = 'book_name';


        $resultdata = $this->Book_model->get_all_book_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }
	

    /**publisher
     * Delete Session
     *
     * @param $session_id
     */
//    public function delete($session_id)
//    {
//        $res = $this->Religion_model->delete_session(['id' => $session_id]);
//
//        $this->session->set_flashdata('flash_message', $res['msg']);
//        $this->session->set_flashdata('flash_message_status', $res['status']);
//
//        redirect('admin/religion/list');
//    }

		
		


	
}