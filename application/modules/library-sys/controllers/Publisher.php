<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Publisher extends My_Controller
{

    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('Publisher_model'),
    );
    /**
     * Religion constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $publisher_id
     */
    public function add($publisher_id = NULL)
    {
        $this->form_validation->set_rules('publisher_name', 'Publisher Name', "trim|required|unique[publisher.publisher_name.id.{$publisher_id}]");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($publisher_id))
            {
                $publisher_details = $this->Publisher_model->get_publisher_detail_by(['id' => $publisher_id]);

                if ( ! empty($publisher_details))
                {
                    $dataArray = array(
                        'publisher_name' => $publisher_details['publisher_name'],
                    );

                    $dataArray['form_action'] = 'Edit Publisher';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('lib/publisher/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Publisher';
            }

            $dataArray['css_local'] = array();
            $dataArray['js_local']  = array();

            $dataArray['page_title'] = 'Publisher';
            $dataArray['breadcrumb'] = 'Initial Information / Ppublisher';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('publisher/add-form', $dataArray);
        }
        else
        {
            $params = array(
                'publisher_name' => $this->input->post('publisher_name'),
            );

            if ( ! empty($publisher_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_publisher_id = $this->Publisher_model->save_publisher($params, $publisher_id);
            }
            else
            {
                $params['created_at'] = date("Y-m-d H:i:s");
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_publisher_id = $this->Publisher_model->save_publisher($params);
            }

            if ( ! empty($new_publisher_id))
            {
                $this->session->set_flashdata('flash_message', (empty($publisher_id)) ? 'Publisher created successfully' : 'Publisher updated successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('lib/publisher/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('lib/publisher/list');
            }
        }
    }

    /**
     *
     */
    public function index()
    {
        $listing_headers = 'publisher_listing_headers';

        $data['source']          = site_url('library-sys/publisher/listPublisher_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Publisher';
        $data['page_title']      = 'Publisher';
        $data['breadcrumb']      = 'Initial Information / Publisher';

        $dataArray = $this->_table_listing($data);

        $this->load->view('publisher/index', $dataArray);
    }

    /**
     *
     */
    public function listPublisher_Json()
    {
        $listing_headers = 'publisher_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Publisher_model->tbl_name = 'publisher';

        $this->Publisher_model->select_db_cols = "id, publisher_name, created_at, updated_at";

        $this->Publisher_model->list_search_key = 'publisher_name';


        $resultdata = $this->Publisher_model->get_all_publisher_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Session
     *
     * @param $session_id
     */
//    public function delete($session_id)
//    {
//        $res = $this->Religion_model->delete_session(['id' => $session_id]);
//
//        $this->session->set_flashdata('flash_message', $res['msg']);
//        $this->session->set_flashdata('flash_message_status', $res['status']);
//
//        redirect('admin/religion/list');
//    }
}