<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Author extends My_Controller
{

    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model'     => array('Author_model'),
    );
    /**
     * Religion constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $author_id
     */
    public function add($author_id = NULL)
    {
        $this->form_validation->set_rules('author_name', 'Author Name', "trim|required|unique[author.author_name.id.{$author_id}]");

        if ( ! $this->form_validation->run())
        {
            if ( ! empty($author_id))
            {
                $author_details = $this->Author_model->get_author_detail_by(['id' => $author_id]);

                if ( ! empty($author_details))
                {
                    $dataArray = array(
                        'author_name' => $author_details['author_name'],
                    );

                    $dataArray['form_action'] = 'Edit Author';
                }
                else
                {
                    $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                    $this->session->set_flashdata('flash_message_status', FALSE);

                    redirect('lib/author/list');
                }
            }
            else
            {
                $dataArray['form_action'] = 'Add Author';
            }

            $dataArray['css_local'] = array();
            $dataArray['js_local']  = array();

            $dataArray['page_title'] = 'Author';
            $dataArray['breadcrumb'] = 'Initial Information / Author';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message']        = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('author/add-form', $dataArray);
        }
        else
        {
            $params = array(
                'author_name' => $this->input->post('author_name'),
            );

            if ( ! empty($author_id))
            {
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_author_id = $this->Author_model->save_author($params, $author_id);
            }
            else
            {
                $params['created_at'] = date("Y-m-d H:i:s");
                $params['updated_at'] = date("Y-m-d H:i:s");

                $new_author_id = $this->Author_model->save_author($params);
            }

            if ( ! empty($new_author_id))
            {
                $this->session->set_flashdata('flash_message', (empty($author_id)) ? 'Author created successfully' : 'Author updated successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('lib/author/list');
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('lib/author/list');
            }
        }
    }

    /**
     *
     */
    public function index()
    {
        $listing_headers = 'author_listing_headers';

        $data['source']          = site_url('library-sys/author/listAuthor_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action']     = 'List Author';
        $data['page_title']      = 'Author';
        $data['breadcrumb']      = 'Initial Information / Author';

        $dataArray = $this->_table_listing($data);

        $this->load->view('author/index', $dataArray);
    }

    /**
     *
     */
    public function listAuthor_Json()
    {
        $listing_headers = 'author_listing_headers';

        $cols         = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Author_model->tbl_name = 'author';

        $this->Author_model->select_db_cols = "id, author_name, created_at, updated_at";

        $this->Author_model->list_search_key = 'author_name';


        $resultdata = $this->Author_model->get_all_author_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Session
     *
     * @param $session_id
     */
//    public function delete($session_id)
//    {
//        $res = $this->Religion_model->delete_session(['id' => $session_id]);
//
//        $this->session->set_flashdata('flash_message', $res['msg']);
//        $this->session->set_flashdata('flash_message_status', $res['status']);
//
//        redirect('admin/religion/list');
//    }
}