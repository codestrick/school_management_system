<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends My_Controller
{
    public $autoload = array(
        'model' => array('Session_model'),
    );

    public function __construct()
    {
        parent::__construct();
    }

    public function Index()
    {
        $to_day_date = Date("Y-m-d");
        $to_day_date = strtotime($to_day_date);
        $to_day_year = date("Y", $to_day_date);

        //get current year session
        $session_detail = $this->Session_model->get_session_detail_by(['ses_start_yr' => $to_day_year]);

        if (empty($session_detail)) {
            $params['ses_start_yr'] = $to_day_year;
            $params['ses_end_yr'] = ($to_day_year + 1);
            $params['created_at'] = date("Y-m-d H:i:s");
            $params['updated_at'] = date("Y-m-d H:i:s");

            $new_session_id = $this->Session_model->save_session($params);
        }

        $dataArray['page_title'] = 'Dashboard';
        $dataArray['breadcrumb'] = 'Dashboard';
        $dataArray['form_action'] = 'Dashboard';

        $this->load->view('dashboard', $dataArray);
    }

}