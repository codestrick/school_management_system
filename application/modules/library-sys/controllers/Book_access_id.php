<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Book_access_id extends My_Controller
{

    public $autoload = array(
        'libraries' => array('form_validation', 'report_library'),
        'model' => array('Book_access_id_model', 'Book_model'),
    );

    /**
     * Religion constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $book_access_id
     */
    public function add()
    {
        $this->form_validation->set_rules('book_id', 'Book Name', "trim|required");
        $this->form_validation->set_rules('quantity', 'Quantity', "trim|required");

        if (!$this->form_validation->run()) {


            $dataArray['form_action'] = 'Add Book_access_id';


            $books = $this->Book_model->get_book_detail_by([], 'result_array');

            $dataArray['css_local'] = array();
            $dataArray['js_local'] = array();

            $dataArray['books'] = $books;

            $dataArray['page_title'] = 'Book_access_id';
            $dataArray['breadcrumb'] = 'Initial Information / Book Access ID Create';

            $dataArray['validation_err'] = validation_errors();

            $dataArray['flash_message'] = $this->session->flashdata('flash_message');
            $dataArray['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->view('Book_access_id/add-form', $dataArray);
        } else {

            $book_id = $this->input->post('book_id');
            $quantitL̥y = $this->input->post('quantity');

            for($i = 1; $i <= $quantitL̥y; $i++){

                $params['book_id'] = $book_id;
                $params['created_at'] = date("Y-m-d H:i:s");
                $params['updated_at'] = date("Y-m-d H:i:s");


                $new_book_access_id = $this->Book_access_id_model->save_book_access_id($params);
            }

            $new_book_access_id = $this->Book_access_id_model->save_book_access_id($params);


            if (!empty($new_book_access_id)) {
                $this->session->set_flashdata('flash_message', 'Book-access-id created successfully');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect('lib/book-access-id/list');
            } else {
                $this->session->set_flashdata('flash_message', 'Invalid Operation Performed!');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect('lib/book-access-id/list');
            }
        }
    }

    /**
     *
     */
    public function index()
    {
        $listing_headers = 'book_access_id_listing_headers';

        $data['source'] = site_url('library-sys/book_access_id/listAuthor_Json');
        $data['listing_headers'] = $listing_headers;
        $data['form_action'] = 'List Book-access-id';
        $data['page_title'] = 'Book-access-id';
        $data['breadcrumb'] = 'Initial Information / Book-access-id';

        $dataArray = $this->_table_listing($data);

        $this->load->view('book_access_id/index', $dataArray);
    }

    /**
     *
     */
    public function listAuthor_Json()
    {
        $listing_headers = 'book_access_id_listing_headers';

        $cols = array_keys(lang($listing_headers));
        $pagingParams = $this->report_library->getPagingParams($cols);

        $this->Book_access_id_model->tbl_name = 'book_access_id';
        $this->Book_access_id_model->join_tbl_name1 = 'book';

        $this->Book_access_id_model->join_cond1 = "{$this->Book_access_id_model->tbl_name}.book_id = {$this->Book_access_id_model->join_tbl_name1}.id";

        $this->Book_access_id_model->select_db_cols = "book_access_id.id, book_access_id.book_id, book_access_id.created_at, book_access_id.updated_at,
        book.id as book_id, book.book_name";

        $this->Book_access_id_model->list_search_key = 'book_access_id.id';


        $resultdata = $this->Book_access_id_model->get_all_book_access_id_datatable($pagingParams);

        $tableResponse = $this->report_library->makeReportColumns($resultdata, $listing_headers);

        $this->load->setTemplate('json');
        $this->load->view('json', $tableResponse);
    }

    /**
     * Delete Session
     *
     * @param $session_id
     */
//    public function delete($session_id)
//    {
//        $res = $this->Religion_model->delete_session(['id' => $session_id]);
//
//        $this->session->set_flashdata('flash_message', $res['msg']);
//        $this->session->set_flashdata('flash_message_status', $res['status']);
//
//        redirect('admin/religion/list');
//    }
}