<!-- Basic layout-->
<div class="row">
    <div class="col-md-12">

        <?php
        if ( ! empty($flash_message))
        {
            ?>
            <div class="alert <?php echo $flash_message_status == 1 ? 'alert-success' : 'alert-danger' ?> alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $flash_message; ?>
            </div>
            <?php
        }

        if ( ! empty($validation_err))
        {
            ?>
            <div class="alert alert-danger alert-styled-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $validation_err; ?>
            </div>
            <?php
        }
        ?>

        <!--jquery validation error container-->
        <div id="errorContainer" class="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p class="text-bold">Please correct the following errors and try again:</p>
            <ul></ul>
        </div>

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title text-bold"><?php echo ! empty($form_action) ? $form_action : ''; ?></h5>
                <div class="heading-elements">
                    <a href="<?php echo base_url('lib/book-access-id/list'); ?>" class="btn btn-primary">
                        <i class="fa fa-list"></i>&nbsp;
                        List Book Access Id
                    </a>
                </div>
            </div>

            <div class="panel-body">
                <form id="save_form" action="" method="post" class="form-horizontal">

                    <div class="form-group">
                        <label class="col-lg-2 control-label text-bold">Book Name</label>
                        <div class="col-lg-4">
                            <select class="form-control" name="book_id" id="book_id"
                                    required data-msg-required="Book Name Required">
                                <option value="">Select Book</option>
                                <?php
                                if ( ! empty($books))
                                {
                                    foreach ($books as $k => $book)
                                    {

                                        if(!empty($book_id))
                                        {
                                            $selected = ($book['id'] == $book_id) ? 'selected' : '';
                                        }else{
                                            $selected = '';
                                        }

                                        echo '<option value="'. $book['id'].'"' . $selected . '>'.$book['book_name'].'</option>';

                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-lg-2 control-label text-bold">Quantity</label>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="quantity" id="quantity"
                                   value="<?php echo ! empty($quantity) ? $quantity : $this->input->post('quantity'); ?>"
                                   placeholder="Enter Quantity" required data-msg-required="Quantity is Required"/>
                        </div>
						
                    </div>
                    

                    <div class="text-right col-lg-6">
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<!-- /basic layout -->

