<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Session_model extends MY_Model
{
    public $tbl_name = 'session';

    /**
     * Session_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array $params
     * @param null  $id
     *
     * @return null
     */
    function save_session($params = [], $id = NULL)
    {
        $return = NULL;

        if ( ! empty($id))
        {
            $this->db->update($this->tbl_name, $params, ['id' => $id]);
            $return = $id;
        }
        else
        {
            $this->db->insert($this->tbl_name, $params);
            $return = $this->db->insert_id();
        }

        return $return;
    }

    /**
     * @param array  $params
     * @param string $return_type
     *
     * @return null
     */
    function get_session_detail_by($params = [], $return_type = 'row_array')
    {
        $result = NULL;

        if ($return_type == 'result_array')
        {
            $result = $this->db->get_where($this->tbl_name, $params)->result_array();
        }
        else
        {
            $result = $this->db->get_where($this->tbl_name, $params)->row_array();
        }

        return $result;
    }

    /**
     * @param array $pagingParams
     *
     * @return array|null
     */
    function get_all_session_datatable($pagingParams = array())
    {
        $this->db->select('SQL_CALC_FOUND_ROWS 1', FALSE);
        $this->db->select($this->select_db_cols);

        if ( ! empty($pagingParams['order_by']))
        {
            if (empty($pagingParams['order_direction']))
            {
                $pagingParams['order_direction'] = '';
            }

            switch ($pagingParams['order_by'])
            {
                default:
                    $this->db->order_by($pagingParams['order_by'],
                        $pagingParams['order_direction']);
                    break;
            }
        }

        $search = empty($pagingParams['search']) ? array() : $pagingParams['search'];
        if ( ! empty($search))
        {
            $this->db->like($this->list_search_key, $search);
            $this->db->or_like($this->list_search_key1, $search);
            $this->db->or_like($this->list_search_key2, $search);
            $this->db->or_like($this->list_search_key3, $search);
        }

        $return = $this->getWithCount($this->tbl_name, $pagingParams['records_per_page'], $pagingParams['offset']);

        return $return;
    }

    /**
     * @param $params
     *
     * @return array
     */
    function delete_session($params)
    {
        $status = $this->db->delete($this->tbl_name, $params);

        if ($status == TRUE)
        {
            $statusMessage = 'Session record successfully deleted.';
        }
        else
        {
            $statusMessage = 'Session record delete error.';
        }

        return [
            'status' => $status,
            'msg'    => $statusMessage,
        ];
    }
}
