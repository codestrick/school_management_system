<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends My_Controller
{
    public $autoload = array(
        'libraries' => array('form_validation', 'excel'),
        'model'     => array('Cron_request_model','Class_model'),
    );

    public function __construct()
    {
        parent::__construct();
    }

    function index(){
        $cron_request = $this->Cron_request_model->get_detail_by(['status' => 0,'request_type'=> 'result-request'],'result_array');

        if(!empty($cron_request)){

            foreach($cron_request as $val){

                if(!empty($val['body'])){

                    $body = json_decode($val['body'],1);
                    $this->get_all_data($body);
                }
            }
        }

        p($cron_request);
    }

    public function get_all_data($data){

        p($data);

        $class_id = $data['class_id'];

        if(!empty($class_id)){

            $class_details = $this->Class_model->get_detail_by(['id' => $class_id]);

        }

    }


}