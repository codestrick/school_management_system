<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Class_model extends MY_Model
{
    public $tbl_name = 'class';

    /**
     * Session_model constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array  $params
     * @param string $return_type
     *
     * @return null
     */
    function get_detail_by($params = [], $return_type = 'row_array')
    {
        $result = NULL;

        if ($return_type == 'result_array')
        {
            $result = $this->db->get_where($this->tbl_name, $params)->result_array();
        }
        else
        {
            $result = $this->db->get_where($this->tbl_name, $params)->row_array();
        }

        return $result;
    }

}
