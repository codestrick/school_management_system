<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends My_Controller
{

    public $autoload = array(
        'libraries' => array('form_validation'),
        'model'     => array('Auth_model'),
    );

    /**
     * Admin Login View & Validate
     */
    public function login()
    {
        $this->form_validation->set_rules('username', 'Username', 'required|max_length[255]');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run())
        {
            $data = array(
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password'),
            );

            $admin_credential = $this->Auth_model->get_details_by([
                'username' => $data['username'],
                'password' => md5($data['password'])
            ]);

            if ( ! empty($admin_credential))
            {

                $session_det = $this->db->get_where('session', ['ses_start_yr'=> date('Y')])->row_array();

                if(empty($session_det)){
                    $current_year = date('Y');
                    $params['ses_start_yr'] = $current_year;
                    $params['ses_end_yr'] = ($current_year + 1);
                    $params['created_at'] = date("Y-m-d H:i:s");
                    $params['updated_at'] = date("Y-m-d H:i:s");

                    $this->db->insert('session', $params);
                    $session_id = $this->db->insert_id();
                }else{
                    $session_id = $session_det['id'];
                }

                $this->session->set_userdata('user_auth', array(
                    'connected' => TRUE,
                    'id'        => $admin_credential['id'],
                    'name'      => $admin_credential['name'],
                    'email'     => $admin_credential['email'],
                    'mobile'    => $admin_credential['mobile'],
                    'session'   => $session_id,
                ));

                $this->session->set_flashdata('flash_message', 'You logged in successfully.');
                $this->session->set_flashdata('flash_message_status', TRUE);

                redirect(base_url('admin/dashboard'));
            }
            else
            {
                $this->session->set_flashdata('flash_message', 'Error, Please provide valid login credential.');
                $this->session->set_flashdata('flash_message_status', FALSE);

                redirect(base_url('admin/login'));
            }
        }
        else
        {
            $validation_errors = validation_errors();
            if ( ! empty($validation_errors))
            {
                $this->session->set_flashdata('flash_message', $validation_errors);
                $this->session->set_flashdata('flash_message_status', FALSE);
            }

            $data['flash_message']        = $this->session->flashdata('flash_message');
            $data['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->setTemplate('login');
            $this->load->view('login', $data);
        }
    }

    /**
     * Logout Admin
     */
    public function logout()
    {
        $this->session->sess_destroy();

        $this->session->set_flashdata('flash_message', 'You logged out successfully.');
        $this->session->set_flashdata('flash_message_status', TRUE);

        redirect(base_url('admin/login'));
    }

    /**
     * Forgot Password View & Validation
     */
    public function forgot_password()
    {
        if ($this->form_validation->run())
        {

        }
        else
        {
            $validation_errors = validation_errors();
            if ( ! empty($validation_errors))
            {
                $this->session->set_flashdata('flash_message', $validation_errors);
                $this->session->set_flashdata('flash_message_status', FALSE);
            }

            $data['flash_message']        = $this->session->flashdata('flash_message');
            $data['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->setTemplate('login');
            $this->load->view('forgot-password', $data);
        }
    }

    /**
     * Reset Password View & Validation
     */
    public function reset_password()
    {
        if ($this->form_validation->run())
        {

        }
        else
        {
            $validation_errors = validation_errors();
            if ( ! empty($validation_errors))
            {
                $this->session->set_flashdata('flash_message', $validation_errors);
                $this->session->set_flashdata('flash_message_status', FALSE);
            }

            $data['flash_message']        = $this->session->flashdata('flash_message');
            $data['flash_message_status'] = $this->session->flashdata('flash_message_status');

            $this->load->setTemplate('login');
            $this->load->view('reset-password', $data);
        }
    }
}