<?php

if ( ! defined('BASEPATH'))
{
    exit('No direct script access allowed');
}

class My_Controller extends MX_Controller
{
    /**
     * $ajaxRequest : this is the variable which contains the requested page is via ajax call or not. by default it is false and will be set as false and will be set as true in constructor after validating the request type.
     *
     */
    public $ajaxRequest = FALSE;
    public $template    = NULL;

    /**
     * My_Controller constructor.
     */
    public function __construct()
    {
        parent::__construct();

        /**
         * validating the request type is ajax or not and setting up the $ajaxRequest variable as true/false.
         *
         */
        $requestType       = $this->input->server('HTTP_X_REQUESTED_WITH');
        $this->ajaxRequest = strtolower($requestType) == 'xmlhttprequest';

        /**
         * set the default template as blank when the request type is ajax
         */
        if ($this->ajaxRequest === TRUE)
        {
            $this->load->setTemplate('blank');
        }

        $module = strtolower($this->router->fetch_module());

        $this->_load_default_config($module);
        $this->_load_default_language($module);

        switch ($module)
        {
            case 'admin':
                $this->load->setTemplate('admin');
                break;

            case 'library-sys':
                $this->load->setTemplate('lib');
                break;

            case 'public':
                $this->load->setTemplate('public');
                break;
        }
    }

    /**
     * Load Default Module Config file
     *
     * @param $module
     */
    private function _load_default_config($module)
    {
        $default_confgi_arr = array($module . '/assets/default.php', $module . '/menu/default.php', $module . '/report/default.php');

        foreach ($default_confgi_arr as $index => $item)
        {
            if (file_exists(APPPATH . 'config/' . $item))
            {
                $this->config->load($item);
            }
        }
    }

    /**
     * Load Default Module Language file
     *
     * @param $module
     */
    private function _load_default_language($module)
    {
        $item = 'reportcolumns/default_lang.php';

        if (file_exists(APPPATH . 'language/english/' . $item))
        {
            $this->lang->load($item);
        }
    }

    /**
     * Generate Jquery DataTable
     *
     * @param $data
     *
     * @return array
     */
    public function _table_listing($data)
    {
        $dataArray = array(
            'table'                => $this->report_library->makeTable($data['listing_headers'], $data),
            'flash_message'        => $this->session->flashdata('flash_message'),
            'flash_message_status' => $this->session->flashdata('flash_message_status'),
            'form_action'          => $data['form_action'],
            'page_title'           => $data['page_title'],
            'breadcrumb'           => $data['breadcrumb'],
        );

        $dataArray['js_local'] = array(
            'jquery.dataTables',
            'dataTables.bootstrap4',
            'dataTables.buttons',
            'jszip',
            'pdfmake',
            'vfs_fonts',
            'buttons.html5',
            'buttons.print',
            'dataTables.select',
            'dataTables-main'
        );

        $dataArray['css_local'] = array(
            'dataTables.bootstrap4',
            'select.bootstrap4',
            'buttons.dataTables'
        );

        return $dataArray;
    }

    /**
     * @param       $method
     * @param array $params
     */
    public function _remap($method, $params = array())
    {
        $module = $this->router->fetch_module();
        $class  = $this->router->fetch_class();

        $this->_check_user_auth($module, $class, $method, $params);

        if (method_exists($this, $method))
        {
            call_user_func_array(array($this, $method), $params);
        }
        else
        {
            show_404();
        }
    }

    /**
     * User Authentication check
     *
     * @param $module
     * @param $class
     * @param $method
     * @param $params
     *
     * @return bool
     */
    public function _check_user_auth($module, $class, $method, $params)
    {
        $sess_user_auth = $this->session->userdata('user_auth');
        $user_authd     = FALSE;
        $redirect_login = FALSE;

        if ( ! empty($sess_user_auth['connected']) && (isset($sess_user_auth['id']) && $sess_user_auth['id'] != NULL))
        {
            $user_authd = TRUE;
        }

        if ($module == 'auth')
        {
            if ( ! in_array($method, array('login', 'forgot_password', 'reset_password')))
            {
                if ( ! $user_authd)
                {
                    $redirect_login = TRUE;
                }
            }
            else
            {
                if ($user_authd)
                {
                    redirect('admin/dashboard');
                }
            }
        }
        else if ($module == 'admin')
        {
            if ( ! $user_authd)
            {
                $redirect_login = TRUE;
            }
        }
        else if ($module == 'public')
        {

        }
        else
        {
            if ( ! $user_authd)
            {
                $redirect_login = TRUE;
            }
        }

        if ($redirect_login == TRUE)
        {
            redirect(base_url('admin/login'));
        }
        else
        {
            return FALSE;
        }
    }
}
