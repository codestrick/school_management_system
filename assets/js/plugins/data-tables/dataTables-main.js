var dataTable_data;

$(document).ready(function () {

    var columns = [];
    var rowsGroup = null;
    var export_title = '';

    if (export_file_for == 'session')
    {
        columns = [1, 2];
        export_title = 'Session Details';
    }
    else if (export_file_for == 'class')
    {
        columns = [1, 2];
        export_title = 'Class Details';
    }
    else if (export_file_for == 'section')
    {
        columns = [1, 2];
        export_title = 'Section Details';
    }
    else if (export_file_for == 'subject')
    {
        columns = [1];
        export_title = 'Subject Details';
    }
    else if (export_file_for == 'religion')
    {
        columns = [1];
        export_title = 'Religion Details';
    }
    else if (export_file_for == 'caste')
    {
        columns = [1];
        export_title = 'Caste Details';
    }
    else if (export_file_for == 'sub-caste')
    {
        columns = [1, 2];
        export_title = 'Sub-Caste Details';
    }
    else if (export_file_for == 'occupation')
    {
        columns = [1];
        export_title = 'Occupation Details';
    }
    else if (export_file_for == 'faculty')
    {
        columns = [1, 2, 3, 4];
        export_title = 'Faculty Details';
    }

    initTables(export_title, columns, rowsGroup);
});

function initTables(export_title, columns, rowsGroup)
{
    $("table.dyntable:visible").each(function (i, ele) {
        var ele = $(ele);

        var source = ele.attr('source');

        var jsonStr = ele.attr('jsonInfo');

        var max_rows = ele.attr("max_rows");

        dataTable_data = ele.DataTable({
            bDestroy: true,
            bFilter: true,
            bLengthChange: true,
            iDisplayLength: 10,
            bSort: true,
            bServerSide: true,
            bProcessing: true,
            bJQueryUI: false,
            sPaginationType: "full_numbers",
            sAjaxSource: source,
            oLanguage: {
                sEmptyTable: 'No record found'
            },
            aoColumns: eval(jsonStr),
            columnDefs: [{
                orderable: false,
                className: 'select-checkbox text-center',
                title: "<input type='checkbox' id='select_all_row' class='select_all_row'>",
                targets: 0,
                width: "5%"
            }],
            select: {
                style: 'multi',
                selector: 'td:first-child'
            },
            order: [[1, 'asc']],
            dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'print',
                    text: 'Print All',
                    title: export_title,
                    exportOptions: {
                        modifier: {
                            selected: null
                        },
                        columns: columns
                    }
                },
                {
                    extend: 'print',
                    text: 'Print Selected',
                    title: export_title,
                    exportOptions: {
                        modifier: {
                            selected: true
                        },
                        columns: columns
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: 'Excel All',
                    title: export_title,
                    exportOptions: {
                        modifier: {
                            selected: null
                        },
                        columns: columns
                    }
                },
                {
                    extend: 'excelHtml5',
                    text: 'Excel Selected',
                    title: export_title,
                    exportOptions: {
                        modifier: {
                            selected: true
                        },
                        columns: columns
                    }
                }
            ],
            fnInfoCallback: function (oSettings, iStart, iEnd, iMax, iTotal, sPre) {
                return sPre;
            },
            fnDrawCallback: function (oSettings) {

            },
            initComplete: function (settings, json) {

            }
        });

        $('#select_all_row').change(function () {
            var $self = $(this);

            if ($self.prop("checked"))
            {
                dataTable_data.rows().select();
            }
            else
            {
                dataTable_data.rows().deselect();
            }
        });
    });
}
