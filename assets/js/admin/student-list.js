var student_list_table;

$(document).ready(function () {
    dataTableInIt();
});

function dataTableInIt()
{
    var export_title = 'Student Details';
    var columns = [1, 2, 3];
    var rowsGroup = [];

    if (typeof student_list_table != 'undefined')
    {
        student_list_table.destroy();
    }

    student_list_table = $('#student_list_table').DataTable({
        processing: true,
        serverSide: true,
        paging: true,
        lengthMenu: [[10, 25, 50, 100, 200], [10, 25, 50, 100, 200]],
        pageLength: 10,
        ordering: true,
        sPaginationType: "full_numbers",
        ajax: {
            "url": base_url + 'admin/student/get-student-list-ajax',
            "type": "POST"
        },
        oLanguage: {
            sEmptyTable: 'No record found'
        },
        dom: 'Blfrtip',
        buttons: [
            {
                extend: 'print',
                text: 'Print All',
                title: export_title,
                exportOptions: {
                    modifier: {
                        selected: null
                    },
                    columns: columns
                }
            },
            {
                extend: 'print',
                text: 'Print Selected',
                title: export_title,
                exportOptions: {
                    modifier: {
                        selected: true
                    },
                    columns: columns
                }
            },
            {
                extend: 'excelHtml5',
                text: 'Excel All',
                title: export_title,
                exportOptions: {
                    modifier: {
                        selected: null
                    },
                    columns: columns
                }
            },
            {
                extend: 'excelHtml5',
                text: 'Excel Selected',
                title: export_title,
                exportOptions: {
                    modifier: {
                        selected: true
                    },
                    columns: columns
                }
            },
        ],
        select: {
            style: 'multi',
            selector: 'td:first-child'
        },
        aaSorting: [[1, 'asc']],
        aoColumnDefs: [
            {orderable: false, className: 'select-checkbox', aTargets: [0]}
        ],
        columns: [
            {
                title: "<input type='checkbox' id='student_list_table_row_all_check' class='check_all_row'>",
                data: null,
                width: "5%",
                mRender: function (data, type, row) {
                    return '';
                }
            },
            {data: "reg_id", title: "Registration ID"},
            {data: "std_name", title: "Student Name"},
            {
                title: "Date of Birth",
                data: null,
                mRender: function (data, type, row) {
                    return moment(row.dob).format('DD-MM-YYYY');
                }
            },
            {
                title: "Action",
                data: null,
                width: "15%",
                className: "text-center",
                sorting: false,
                mRender: function (data, type, row) {

                    return '<i class="fa fa-eye view_button" data-id="' + row.id + '" style="font-size: 20px;color: darkblue !important;cursor: pointer" data-toggle="tooltip" title="View"></i>' +
                        '&nbsp;&nbsp;' +
                        '<i class="fa fa-edit edit_button" data-id="' + row.id + '" style="font-size: 20px;color: #2196F3 !important;cursor: pointer" data-toggle="tooltip" title="Edit"></i>' +
                        '&nbsp;&nbsp;' +
                        '<i class="fa fa-trash delete_button" data-id="' + row.id + '" style="font-size: 20px;color: #FF0000 !important;cursor: pointer" data-toggle="tooltip" title="Delete"></i>';

                }
            }
        ],
        fnInfoCallback: function (oSettings, iStart, iEnd, iMax, iTotal, sPre) {
            return sPre;
        },
        fnDrawCallback: function (oSettings) {

        },
        initComplete: function (settings, json) {
            $('#student_list_table thead th').removeClass('text-right');
        }
    });

    $('#student_list_table_row_all_check').change(function () {
        var $self = $(this);

        if ($self.prop("checked"))
        {
            student_list_table.rows().select();
        }
        else
        {
            student_list_table.rows().deselect();
        }
    });

    $('#student_list_table tbody').on('click', 'tr td .view_button', function (e) {

        var row_ID = $(this).data('id');

        window.location.href = base_url + 'admin/student/' + row_ID + '/details';
    });

    $('#student_list_table tbody').on('click', 'tr td .delete_button', function (e) {

        var row_ID = $(this).data('id');

        if (row_ID != '')
        {
            bootbox.confirm({
                title: 'Delete Student',
                size: 'small',
                message: "<h5 style='font-family: Arial, Helvetica Neue, Helvetica, sans-serif'>Are you sure you want to delete the student?</h5>",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result == true)
                    {
                        window.location.replace(base_url + 'admin/student/delete/' + row_ID);
                    }
                }
            }).find('.modal-header').addClass('bg-warning');
        }
    });

    $('#student_list_table tbody').on('click', 'tr td .edit_button', function (e) {

        var row_ID = $(this).data('id');
        var student_ID = 0;

        if (row_ID != '')
        {
            student_ID = row_ID;
        }

        window.location.href = base_url + 'admin/student/edit/' + student_ID;
    });
}