
$(document).ready(function () {

    $("#no_of_exam").multiselect({
        enableFiltering: true,
        nonSelectedText: 'Select Number of Exam',
        enableCaseInsensitiveFiltering: true,
        buttonWidth: '100%',
        includeSelectAllOption: true
    });

    $("#subjects").multiselect({
        enableFiltering: true,
        nonSelectedText: 'Select Subject',
        enableCaseInsensitiveFiltering: true,
        buttonWidth: '100%',
        includeSelectAllOption: true
    });

});