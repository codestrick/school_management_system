$(document).ready(function () {

    get_sub_caste_by_caste($('#caste_id').val());

    $('#std_name').attr('disabled', true);
    $('#dob').attr('disabled', true);
    $('#age').attr('disabled', true);
    $('#phone_no').attr('disabled', true);
    $('#gender').attr('disabled', true);
    $('#religion_id').attr('disabled', true);
    $('#caste_id').attr('disabled', true);
    $('#sub_caste_id').attr('disabled', true);

    $('#fathers_name').attr('disabled', true);
    $('#fathers_occupation_id').attr('disabled', true);
    $('#mothers_name').attr('disabled', true);
    $('#mothers_occupation_id').attr('disabled', true);
    $('#guardians_name').attr('disabled', true);
    $('#guardians_occupation_id').attr('disabled', true);
    $('#guardians_relation').attr('disabled', true);

    $('#permanent_state').attr('disabled', true);
    $('#permanent_district').attr('disabled', true);
    $('#permanent_location').attr('disabled', true);
    $('#permanent_pincode').attr('disabled', true);
    $('#correspondence_state').attr('disabled', true);
    $('#correspondence_district').attr('disabled', true);
    $('#correspondence_location').attr('disabled', true);
    $('#correspondence_pincode').attr('disabled', true);
    $('#useAddress').attr('disabled', true);

    // Wizard Form with validation

    var form = $(".steps-validation").show();

    // Initialize wizard
    $(".steps-validation").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        titleTemplate: '<span class="number">#index#</span> #title#',
        autoFocus: true,
        onStepChanging: function (event, currentIndex, newIndex) {

            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex)
            {
                return true;
            }

            // Forbid next action on "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age-2").val()) < 18)
            {
                return false;
            }

            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex)
            {

                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }

            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {

            // Used to skip the "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age-2").val()) >= 18)
            {
                form.steps("next");
            }

            // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3)
            {
                form.steps("previous");
            }
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex) {

            $.blockUI();

            $.ajax({
                url: base_url + 'admin/student/ajax-readmission-student',
                data: {
                    student_id: typeof student_id != 'undefined' ? student_id : '',
                    admission_class_id: $('#admission_class_id').val(),
                    section_id: $('#section_id').val(),
                    subject_type: $('#subject_type').val(),
                    subject_combination_id: $('#subject_combination_name').val(),
                },
                type: "POST",
                dataType: "json"
            }).done(function (res) {

                $.unblockUI();

                bootbox.alert({
                    title: '<h2>Upgrade Student</h2>',
                    message: '<h3>' + res.message + '</h3>',
                    callback: function () {
                        window.location.href = base_url + 'admin/student/list';
                    }
                });
            });
        }
    });

    // Initialize validation
    $(".steps-validation").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function (error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container'))
            {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline'))
                {
                    error.appendTo(element.parent().parent().parent().parent());
                }
                else
                {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio'))
            {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible'))
            {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline'))
            {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group'))
            {
                error.appendTo(element.parent().parent());
            }

            else
            {
                error.insertAfter(element);
            }
        },
        rules: {
            email: {
                email: true
            }
        }
    });

    $("#dob").datepicker({
        format: 'dd/mm/yyyy',
        endDate: '-d'
    });

    $("#dob").mask('00/00/0000');
    $("#phone_no").mask('0000000000');

    if ($('select').hasClass('select-2'))
    {
        $('select.select-2').select2();
    }

    $("#subject_type").change(function () {
        get_subject_combinations($("#admission_class_id").val(), $("#subject_type").val());
    });

    $("#admission_class_id").change(function () {

        var subject_type = $("#subject_type").val();
        var admission_class_id = $("#admission_class_id").val();

        get_section(admission_class_id);

        if (subject_type)
        {
            get_subject_combinations(admission_class_id, subject_type);
        }
    });

    $("#useAddress").click(function () {
        use_permanent_address();
    });

    $("#caste_id").change(function () {
        get_sub_caste_by_caste($(this).val());
    });
});

/**
 * Get Subject Combinations by Class and Subject Type jQuery AJAX
 * @param admission_class_id
 * @param subject_type
 */
function get_subject_combinations(admission_class_id, subject_type)
{
    $("#subject_combination_name").empty();

    var subject_combination_id = $("#subject_combination_name").data('subject-combination');

    if (admission_class_id && subject_type)
    {
        $.ajax({
            url: base_url + 'admin/student/ajax-get-subject-combination',
            data: {
                class_id: admission_class_id,
                subject_type: subject_type,
            },
            type: "POST",
            dataType: "json"
        }).done(function (res) {

            if (res.status == 'true')
            {
                var append_option = '<option value="">Select Subject Combination</option>';

                $.each(res.data, function (i, v) {

                    var selected = subject_combination_id == v.id ? 'selected' : '';

                    append_option += '<option value="' + v.id + '"' + selected + ' >' + v.subject_combination_name + '</option>';
                });

                $("#subject_combination_name").append(append_option);
            }
            else
            {
                var append_option = '<option value="">No Subject Combination Found!</option>';

                $("#subject_combination_name").append(append_option);
            }
        });
    }
    else
    {
        var append_option = '<option value="">No Subject Combination Found!</option>';

        $("#subject_combination_name").append(append_option);
    }
}

/**
 * On Checkbox check permanent and correspondence address will be same
 */
function use_permanent_address()
{
    var checkBox = document.getElementById("useAddress");

    if (checkBox.checked == true)
    {
        var state = $("#permanent_state").val();
        var district = $("#permanent_district").val();
        var location = $("#permanent_location").val();
        var pincode = $("#permanent_pincode").val();

        $("#correspondence_state").val(state);
        $("#correspondence_district").val(district);
        $("#correspondence_location").val(location);
        $("#correspondence_pincode").val(pincode);
    }
    else
    {
        $("#correspondence_state").val('');
        $("#correspondence_district").val('');
        $("#correspondence_location").val('');
        $("#correspondence_pincode").val('');
    }
}

/**
 * Get Sub Caste by Cast jQuery AJAX
 * @param cast_id
 */
function get_sub_caste_by_caste(caste_id)
{
    $("#sub_caste_id").empty();

    var sub_caste_id = $("#sub_caste_id").data('sub-caste-id');

    if (caste_id)
    {
        $.ajax({
            url: base_url + 'admin/student/ajax-get-sub-caste-by-caste',
            data: {
                caste_id: caste_id
            },
            type: "POST",
            dataType: "json"
        }).done(function (res) {

            if (res.status == 'true')
            {
                var append_option = '<option value="">Select Sub Caste</option>';

                $.each(res.data, function (i, v) {

                    var selected = sub_caste_id == v.id ? 'selected' : '';

                    append_option += '<option value="' + v.id + '"' + selected + '>' + v.sub_caste_name + '</option>';
                });

                $("#sub_caste_id").append(append_option);
            }
            else
            {
                var append_option = '<option value="">No Sub Caste Found!</option>';

                $("#sub_caste_id").append(append_option);
            }
        });
    }
    else
    {
        var append_option = '<option value="">No Sub Caste Found!</option>';

        $("#sub_caste_id").append(append_option);
    }
}

/**
 * Get Section by Class jQuery AJAX
 * @param class_id
 */
function get_section(class_id)
{
    $("#section_id").empty();

    var section_id = $("#section_id").data('section-id');

    if (class_id)
    {
        $.ajax({
            url: base_url + 'admin/student/ajax-get-section-by-class',
            data: {
                class_id: class_id
            },
            type: "POST",
            dataType: "json"
        }).done(function (res) {

            if (res.status == 'true')
            {
                var append_option = '<option value="">Select Section</option>';

                $.each(res.data, function (i, v) {

                    var selected = section_id == v.id ? 'selected' : '';

                    append_option += '<option value="' + v.id + '" ' + selected + '>' + v.section_name + '</option>';
                });

                $("#section_id").append(append_option);
            }
            else
            {
                var append_option = '<option value="">No Section Found!</option>';

                $("#section_id").append(append_option);
            }
        });
    }
    else
    {
        var append_option = '<option value="">No Section Found!</option>';

        $("#section_id").append(append_option);
    }
}