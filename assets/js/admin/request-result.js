$(document).ready(function () {


    $("#class_id").change(function () {
        $.blockUI({ css: { backgroundColor: '#26A69A', color: '#fff'} });
        var selected_class = $(this).find('option:selected');
        var class_type = selected_class.data('class-type');

        if(class_type == 'lower'){

            $('#subject_type option[value="All"]').attr("disabled", false);
            $('#subject_type option[value="Arts"]').attr("disabled", true);
            $('#subject_type option[value="Commerce"]').attr("disabled", true);
            $('#subject_type option[value="Science"]').attr("disabled", true);
            $('#subject_type option[value="Other"]').attr("disabled", true);
            $("#subject_type").val("All");


        }else{
            $("#subject_type").val("");

            $('#subject_type option[value="All"]').attr("disabled", true);
            $('#subject_type option[value="Arts"]').attr("disabled", false);
            $('#subject_type option[value="Commerce"]').attr("disabled", false);
            $('#subject_type option[value="Science"]').attr("disabled", false);
            $('#subject_type option[value="Other"]').attr("disabled", false);
        }
        get_section();
    });

    $("#class_id,#subject_type").change(function () {

        var class_id = $('#class_id').val();
        var subject_type = $('#subject_type').val();
        var section_id = $('#section_id').val();

        get_subject_combinations()
    });

});

function get_subject_combinations(){
    $.blockUI({ css: { backgroundColor: '#26A69A', color: '#fff'} });
    var class_id = $('#class_id').val();
    var subject_type = $('#subject_type').val();
    var section_id = $('#section_id').val();
    $.ajax({
        url: base_url + 'admin/ajax/ajax_controller/get_exam_combination_name',
        data: {
            class_id: $('#class_id').val(),
            subject_type: $('#subject_type').val(),
        },
        type: "POST",
        dataType: "json"
    }).done(function (res) {
        $.unblockUI();
        if (res.status == true)
        {
            var append_option = '<option value="">Select Subject Combination</option>';

            $.each(res.data, function (i, v) {
                append_option += '<option value="' + v.id + '">' + v.subject_combination_name + '</option>';
            });

            $("#subject_combination_id").html(append_option);
        }
        else
        {
            var append_option = '<option value="">No Subject Combination Found!</option>';

            $("#subject_combination_id").html(append_option);
        }
    });
}
function get_section(){
    $.ajax({
        url: base_url + 'admin/ajax/ajax_controller/get_section',
        data: {
            class_id: $('#class_id').val(),
        },
        type: "POST",
        dataType: "json"
    }).done(function (res) {
        $.unblockUI();
        if (res.status == true)
        {
            var append_option = '<option value="">Select Section</option>';

            $.each(res.data, function (i, v) {
                append_option += '<option value="' + v.id + '">' + v.section_name + '</option>';
            });

            $("#section_id").html(append_option);
        }
        else
        {
            var append_option = '<option value="">No Subject Combination Found!</option>';

            $("#section_id").html(append_option);
        }
    });
}