$(document).ready(function () {

    $("table tr").editable({
        keyboard: true,
        // double click to start editing
        dblclick: true,
        // enable edit buttons
        button: true,
        // CSS selector for edit buttons
        buttonSelector: ".edit",
        // uses select dropdown instead of input field
        dropdowns: {
            present_status: ['Present', 'Absent']
    },
        // maintains column width when editing
        maintainWidth: true,
        // callbacks for edit, save and cancel action
        edit: function (values) {
        },
        save: function (values) {
            $.blockUI();
            var result_trans_id = $(this).find('.ind-number').data('id');
            var number = values.number;
            var present_status = values.present_status;

            if(present_status == 'Present'){
                present_status = 1;
            } else{
                present_status = 0;
            }
            $.ajax({
                url: base_url + 'admin/result/ajax-result-save',
                data: {
                    result_trans_id: result_trans_id,
                    number: number,
                    present_status: present_status,
                },
                type: "POST",
                dataType: "json"
            }).done(function (res) {
                $.unblockUI();
            });

        },
        cancel: function (values) {
        }
    });

});